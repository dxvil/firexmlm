<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 */
class m_default extends CI_Model
{

  public function __construct(){
    $this->load->database();
  }

  public function data_insert($table_name, $data){
    return $this->db->insert($table_name, $data);
  
  }

  public function get_data($where, $table_name){
    $this->db->where($where);
    $row = $this->db->get($table_name);
    return $row->result();
  }

  public function get_user_list($query){
    $query = $this->db->query($query);
    return $query->result_array();
  }

  public function update($where, $data, $table_name){
    $this->db->where($where);
    return $this->db->update($table_name, $data);
  }

  public function get_single_row($query)
  {
    $query = $this->db->query($query);
    return $query->result_array();
  }

  public function execute_query($query)
  {
  return $this->db->query($query);
  }

  public function data_insertwithid($table_name, $data){
     $this->db->insert($table_name, $data);
     return $this->db->insert_id();
  }

}
