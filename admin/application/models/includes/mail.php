<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/*
		* 	Author: Dipanwita Chanda
		* 	Description: Mail model class
		* 	To send mail
	*/

	class Mail extends CI_Model {

   function sendEmail($emailid,$sub,$msg, $emailids = "")
    {
    $company_id=$this->session->userdata('company_id');
    $sql="SELECT * FROM tbl_emailconfiguration  WHERE id ='4'";
		$emailconfdata=$this->db->query($sql)->result_array();

		 foreach ($emailconfdata as $row) {
				$protocol=$row['protocol'];
			  $smtphost=$row['smtp_host'];
				$smtpport=$row['smtp_port'];
				$smtpuser=$row['smtp_user'];
				$smtppassword=$row['smtp_pass'];
				$mailtype=$row['mail_type'];
	}

  $config = Array(
  'protocol' => $protocol,
  'smtp_host' => $smtphost,
  'smtp_port' => $smtpport,
  //  'smtp_timeout' =>'7',
  'smtp_user' => $smtpuser,
  'smtp_pass' => $smtppassword,
  'mailtype' =>$mailtype,
  'charset'   => 'iso-8859-1');

  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");
  $this->email->from($smtpuser);
  $this->email->to($emailid);
  $this->email->bcc($emailids);
  $this->email->subject($sub);
  $this->email->message($msg);
  if (!$this->email->send()) {
    show_error($this->email->print_debugger()); }
  else {
    //echo 'Please check your mail inbox or spam!!';
  }

}

 function sendEmailattach($emailid,$sub,$msg,$file)
    {
$company_id=$this->session->userdata('company_id');
 $sql="SELECT * FROM tbl_emailconfiguration  WHERE id ='4'";
				$emailconfdata=$this->db->query($sql)->result_array();

				foreach ($emailconfdata as $row) {
				$protocol=$row['protocol'];
    $smtphost=$row['smtp_host'];
	$smtpport=$row['smtp_port'];
	$smtpuser=$row['smtp_user'];
	$smtppassword=$row['smtp_pass'];
	$mailtype=$row['mail_type'];

	}

  $config = Array(
  'protocol' => $protocol,
  'smtp_host' => $smtphost,
  'smtp_port' => $smtpport,
  //  'smtp_timeout' =>'7',
  'smtp_user' => $smtpuser,
  'smtp_pass' => $smtppassword,
  'mailtype' =>$mailtype);

  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");
  $this->email->from($smtpuser);
  $this->email->to($emailid);
 // $this->email->bcc($emailids);
  $this->email->subject($sub);
  $this->email->message($msg);
  $this->email->attach($file);
  if (!$this->email->send()) {
    show_error($this->email->print_debugger()); }
  else {
    //echo 'Please check your mail inbox or spam!!';
  }


}

function sendbccEmailattach($emailid,$sub,$msg, $emailids = "",$file)
    {
  $company_id=$this->session->userdata('company_id');
  $sql="SELECT * FROM tbl_emailconfiguration  WHERE id ='4'";
	$emailconfdata=$this->db->query($sql)->result_array();

	foreach ($emailconfdata as $row) {
	$protocol=$row['protocol'];
  $smtphost=$row['smtp_host'];
	$smtpport=$row['smtp_port'];
	$smtpuser=$row['smtp_user'];
	$smtppassword=$row['smtp_pass'];
	$mailtype=$row['mail_type'];
	}

  $config = Array(
  'protocol' => $protocol,
  'smtp_host' => $smtphost,
  'smtp_port' => $smtpport,
  //  'smtp_timeout' =>'7',
  'smtp_user' => $smtpuser,
  'smtp_pass' => $smtppassword,
  'mailtype' =>$mailtype);

  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");
  $this->email->from($smtpuser);
  $this->email->to($emailid);
  $this->email->bcc($emailids);
  $this->email->subject($sub);
  $this->email->message($msg);
  $this->email->attach($file);
  if (!$this->email->send()) {
  show_error($this->email->print_debugger()); }
  else {
    //echo 'Please check your mail inbox or spam!!';
  }
}

	 function clear(){
		 $this->email->clear();
	 }

	}


?>
