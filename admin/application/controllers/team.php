<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team extends CI_Controller {

	/**
	 * Package Controller
	 * Author: Dipanwita Chanda
	**/

  public function __construct()
  {
    parent::__construct();
    $username= $this->session->userdata("username");
    if (empty($username)) {
    $this->session->set_flashdata("error_login", "Invalid Request");
    redirect("login", "refresh");
    }
    $this->load->model('m_default');
    $sql="select coin_price from tbl_coinprice where id=1";
    $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
    foreach($this->input->post() as $items){
      if ($items != ''){
          if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
            $this->session->set_flashdata('error_login','Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
            break;
          }
      }
    }
  }

  public function index()
  {
    $this->data['content']='buy_package';
    $this->data['title']='Home | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function total_members_down($userid,$count)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $count+=count($records);
    if($count>0)
    {
    foreach($records as $row)
      {
        $count= $this->total_members_down($row['id'],$count);
      }
    }
      return $count;
  }
  
  public function total_members_down_matrix($userid,$count)
  {
    $sql="SELECT * FROM tbl_matrix where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();
    $count+=count($records);
    if($count>0)
    {
    foreach($records as $row)
      {
        $count= $this->total_members_down_matrix($row['member_id'],$count);
      }
    }
      return $count;
  }


  public function total_members_down_bv($userid,$count)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    $count+=count($records);
    if($count>0)
    {
    foreach($records as $row)
      {
         if($row['tempo']==1){
             $count=$count-1;
         }

        $count= $this->total_members_down_bv($row['id'],$count);
      }
    }
      return $count;
  }


  public function last_leg_id($userid)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid and leg=0";
    $records=$this->db->query($sql)->result_array();

    if($records>0)
    {
    foreach($records as $row)
      {$userid=$row['id'];
      $userid=  $this->last_leg_id($row['id']);
      }
    }
        return $userid;
  }

  public function last_leg_id_right($userid)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid and leg=1";
    $records=$this->db->query($sql)->result_array();

    if($records>0)
    {
    foreach($records as $row)
      {$userid=$row['id'];
      $userid=  $this->last_leg_id($row['id']);
      }
    }
        return $userid;
  }

public function treeview()
{
  $member_id=$this->session->userdata('member_id');
//   $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$member_id;
//   $leftid= $this->db->query($query)->result_array();
//   if(count($leftid)>0)
//   {
//   $countpairs=1;
//   $this->data['totalleftleg']=$this->total_members_down($leftid[0]['id'],$countpairs);
//   }
//   else{
//     $this->data['totalleftleg']=0;
//   }

//   $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$member_id;
//   $rightid= $this->db->query($query)->result_array();

//   if(count($rightid)>0)
//   {
//   $countpairs=1;
//   $this->data['totalrightleg']= $this->total_members_down($rightid[0]['id'],$countpairs);
//   }
//   else{
//     $this->data['totalrightleg']=0;
//   }

//   //////
//   if(count($leftid)>0)
//   {
//   if($leftid[0]['tempo']!=1){
//             $countpairs=0;
//         }
//         else{
//             $countpairs=1;
//         }
//   $this->data['totalleftleg_bv']=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
//   }
//   else{
//     $this->data['totalleftleg_bv']=0;
//   }


//   if(count($rightid)>0)
//   {
//   if($rightid[0]['tempo']!=1){
//             $countpairs=0;
//         }
//         else{
//             $countpairs=1;
//         }

//   $this->data['totalrightleg_bv']= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
//   }
//   else{
//     $this->data['totalrightleg_bv']=0;
//   }
//////

$sql="select leftcount,rightcount,totalleftpv,totalrightpv from tbl_member where id=$member_id ";
    $valo = $this->db->query($sql)->result_array();
    
    
    $this->data['totalleftleg']=$valo[0]['leftcount'];
    $this->data['totalrightleg']=$valo[0]['rightcount'];

    $this->data['totalleftpv']=$valo[0]['totalleftpv'];
    $this->data['totalrightpv']=$valo[0]['totalrightpv'];


$row['rank']=$this->db->query("select rank from tbl_member where id=$member_id")->row()->rank;



if($row['rank']==0){
      $rank="Distributor";
    }
    if($row['rank']==1){
      $rank="Bronze";
    }
    else if($row['rank']==2){
      $rank="Silver";
    }
    else if($row['rank']==3){

      $rank="Platinum";
    }
    else if($row['rank']==4){

      $rank="Gold";
    }
    else if($row['rank']==5){

      $rank="Pearl";
    }
    else if($row['rank']==6){

      $rank="Diamond";
    }
    else if($row['rank']==7){
      $rank="Ruby";
    }
    else if($row['rank']==8){
      $rank="Red Diamond";
    }
    else if($row['rank']==9){

      $rank="Kohinoor";
    }
    else if($row['rank']==10){

      $rank="Ever Green Wallet";
    }

  $this->data['rank']=$rank;

  $str='';
  $str1=$this->m_treeview($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


public function rank_data()
{

   $member_id=$this->session->userdata('member_id');
  $query = "SELECT id,name,rank FROM  tbl_member where leg =0 and parent_id=".$member_id;
  $leftid= $this->db->query($query)->result_array();


  $query = "SELECT id,name,rank FROM  tbl_member where leg =1 and parent_id=".$member_id;
  $rightid= $this->db->query($query)->result_array();


    if(count($leftid)>0)
        {
          if($leftid[0]['rank']==0){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
        $this->data['distributorleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,0);
         if($leftid[0]['rank']==1){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
        $this->data['bronzeleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,1);
         if($leftid[0]['rank']==2){
           $countrank=1;
          }
          else{
            $countrank=0;
          }

         $this->data['silverleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,2);

         if($leftid[0]['rank']==3){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['platinumleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,3);

          if($leftid[0]['rank']==4){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['goldleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,4);


          if($leftid[0]['rank']==5){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['pearlleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,5);

          if($leftid[0]['rank']==6){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['diamondleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,6);

          if($leftid[0]['rank']==7){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['rubyleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,7);

          if($leftid[0]['rank']==8){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['reddiamondleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,8);


          if($leftid[0]['rank']==9){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['kohinoorleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,9);

          if($leftid[0]['rank']==10){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['shineleft']=$this->total_members_down_rank($leftid[0]['id'],$countrank,10);

        }

       if(count($rightid)>0)
        {
          if($rightid[0]['rank']==0){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['distributorright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,0);
         if($rightid[0]['rank']==1){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['bronzeright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,1);
         if($rightid[0]['rank']==2){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['silverright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,2);

         if($rightid[0]['rank']==3){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['platinumright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,3);

          if($rightid[0]['rank']==4){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['goldright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,4);


          if($rightid[0]['rank']==5){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['pearlright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,5);

          if($rightid[0]['rank']==6){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['diamondright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,6);

          if($rightid[0]['rank']==7){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['rubyright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,7);

          if($rightid[0]['rank']==8){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['reddiamondright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,8);


          if($rightid[0]['rank']==9){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['kohinoorright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,9);

          if($rightid[0]['rank']==10){
            $countrank=1;
          }
          else{
            $countrank=0;
          }
         $this->data['shineright']=$this->total_members_down_rank($rightid[0]['id'],$countrank,10);

        }

  $this->data['content']='rank_data';
  $this->data['title']='Rank Data | Ever Green Wallet';
  $this->load->view('common/template',$this->data);

}

public function total_members_down_rank($userid,$count,$rank)
  {
    $sql="SELECT id,tempo,rank FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();


    foreach($records as $row)
      {
         if($row['rank']==$rank){
              $count++;
         }

        $count= $this->total_members_down_rank($row['id'],$count,$rank);
      }

      return $count;
  }
public function matrix_treeview()
{
  $member_id=$this->session->userdata('member_id');
  $query = $this->db->query("SELECT member_id FROM  tbl_matrix where parent_id=$member_id order by id desc")->result_array();
  
  if(count($query)==1){
      $this->data['leftcount']=  $this->total_members_down_matrix($query[0]['member_id'],1);
  }
  else if(count($query)==2){
      $this->data['leftcount']=  $this->total_members_down_matrix($query[0]['member_id'],1);
       $this->data['rightcount']=  $this->total_members_down_matrix($query[1]['member_id'],1);
  }
 
  
  
  $str='';
  $str1=$this->m_treeview_matrix($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_matrix';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function treeview_details_matrix()
{
  $member_id=$this->input->post('id');
  
   $query = $this->db->query("SELECT member_id FROM  tbl_matrix where parent_id=$member_id order by id desc")->result_array();
  
  if(count($query)==1){
      $this->data['leftcount']=  $this->total_members_down_matrix($query[0]['member_id'],1);
  }
  else if(count($query)==2){
      $this->data['leftcount']=  $this->total_members_down_matrix($query[0]['member_id'],1);
       $this->data['rightcount']=  $this->total_members_down_matrix($query[1]['member_id'],1);
  }
  $this->data['member']=$this->db->query("Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id")->result_array();

  $str='';
  $str1=$this->m_treeview_matrix($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_new_matrix';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


public function m_treeview_matrix($userid,$str,$level)
{
  $sql="SELECT a.member_id as id,a.parent_id , b.username,b.email FROM tbl_matrix a,  tbl_login b where a.parent_id=$userid and a.member_id=b.member_id order by id DESC";
  $records=$this->db->query($sql)->result_array();
  if($level==2)
  {return $str;}

  if( count($records)==0 ){

      $records[0]['id']=0;
      $records[0]['leg']=1;
      $records[0]['image']='images/vacant.png';
      $records[0]['username']='vacant';
      $records[0]['email']='vacant';

      $records[1]['id']=0;
      $records[1]['leg']=0;
      $records[1]['image']='images/vacant.png';
      $records[1]['username']='vacant';
      $records[1]['email']='vacant';

  }

  if( count($records)==1 ){

      $records[1]['id']=0;
      $records[1]['leg']=1;
      $records[1]['image']='images/vacant.png';
      $records[1]['username']='vacant';
      $records[1]['email']='vacant';
  }

 
  if(count($records)>0)
  {
/*if( count($records)==1 ){
  if($records[0]['leg']==0)
  {
    $records[1]['id']=0;
    $records[1]['leg']=1;
    $records[1]['image']='images/vacant.png';
    $records[1]['username']='vacant';
    $records[1]['email']='vacant';
  }
  else if($records[0]['leg']==1)
  {
    $records[1]['id']=0;
    $records[1]['leg']=0;
    $records[1]['image']='images/vacant.png';
    $records[1]['username']='vacant';
    $records[1]['email']='vacant';
  }

}*/

    $level++;
    $records=$this->array_msort($records,array('leg'=>SORT_ASC));
   $str.='<ul>';
   $view_url=base_url().'index.php/team/treeview_details_matrix';
  foreach($records as $row)
    {
      if($row['username']!='vacant'){

        $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$row['id'];
        $leftid= $this->db->query($query)->result_array();
        if(count($leftid)>0)
        {
         $countpairs=1;
         $totalleftleg=$this->total_members_down($leftid[0]['id'],$countpairs);
        }
        else{
          $totalleftleg=0;
        }

        $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$row['id'];
        $rightid= $this->db->query($query)->result_array();

        if(count($rightid)>0)
        {
         $countpairs=1;
         $totalrightleg= $this->total_members_down($rightid[0]['id'],$countpairs);
        }
        else{
          $totalrightleg=0;
        }

        $image='images/user.png';
        $str.='<li><a onclick="action_without_msg(\''.$view_url.'\', '.$row['id'].')" class="mytooltip" style=" text-transform:capitalize;font-size:11px; font-weight:600;">
  <img src="'.base_url().'images/user.png" style="width:40px;">

        '.$row['username'].'<span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">'.$row['name'].'<br> '.$row['email'].'
<br><br>
        </span></span></span></a>';
        $str= $this->m_treeview_matrix($row['id'],$str,$level);
        $str.='</li>';
      }
      else{
        $image=$row['image'];
        $str.='<li><a class="mytooltip" style=" text-transform:capitalize;font-size:11px; font-weight:600;">
  <img src="'.base_url().$image.'" style="width:40px;">

        '.$row['username'].'<span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">'.$row['name'].'<br> '.$row['email'].'</span></span></span></a>';

        $str.='</li>';
      }
    }
  $str.='</ul>';
  }
    return $str;
}


public function right()
{
  $member_id=$this->input->post('id');

  $member_id=$this->last_leg_id_right($member_id);


  $this->data['member']=$this->db->query("Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id")->result_array();

  $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$member_id;
  $leftid= $this->db->query($query)->result_array();
  if(count($leftid)>0)
  {
   $countpairs=1;
   $this->data['totalleftleg']=$this->total_members_down($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg']=0;
  }

  $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$member_id;
  $rightid= $this->db->query($query)->result_array();

  if(count($rightid)>0)
  {
   $countpairs=1;
   $this->data['totalrightleg']= $this->total_members_down($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg']=0;
  }

  //////
  if(count($leftid)>0)
  {
   if($leftid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }
   $this->data['totalleftleg_bv']=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg_bv']=0;
  }


  if(count($rightid)>0)
  {
   if($rightid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }

   $this->data['totalrightleg_bv']= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg_bv']=0;
  }
//////

  $str='';
  $str1=$this->m_treeview($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_new';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function left()
{
  $member_id=$this->input->post('id');

$member_id=$this->last_leg_id($member_id);


  $this->data['member']=$this->db->query("Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id")->result_array();

  $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$member_id;
  $leftid= $this->db->query($query)->result_array();
  if(count($leftid)>0)
  {
   $countpairs=1;
   $this->data['totalleftleg']=$this->total_members_down($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg']=0;
  }

  $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$member_id;
  $rightid= $this->db->query($query)->result_array();

  if(count($rightid)>0)
  {
   $countpairs=1;
   $this->data['totalrightleg']= $this->total_members_down($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg']=0;
  }

//////
  if(count($leftid)>0)
  {
   if($leftid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }
   $this->data['totalleftleg_bv']=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg_bv']=0;
  }


  if(count($rightid)>0)
  {
   if($rightid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }

   $this->data['totalrightleg_bv']= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg_bv']=0;
  }
//////
  $str='';
  $str1=$this->m_treeview($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_new';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function treeview_details()
{
  $member_id=$this->input->post('id');
  $row['rank']=$this->db->query("select rank from tbl_member where id=$member_id")->row()->rank;



if($row['rank']==0){
      $rank="Distributor";
    }
    if($row['rank']==1){
      $rank="Bronze";
    }
    else if($row['rank']==2){
      $rank="Silver";
    }
    else if($row['rank']==3){

      $rank="Platinum";
    }
    else if($row['rank']==4){

      $rank="Gold";
    }
    else if($row['rank']==5){

      $rank="Pearl";
    }
    else if($row['rank']==6){

      $rank="Diamond";
    }
    else if($row['rank']==7){
      $rank="Ruby";
    }
    else if($row['rank']==8){
      $rank="Red Diamond";
    }
    else if($row['rank']==9){

      $rank="Kohinoor";
    }
    else if($row['rank']==10){

      $rank="Ever Green Wallet";
    }

  $this->data['rank']=$rank;

  $this->data['member']=$this->db->query("Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id")->result_array();

  $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$member_id;
  $leftid= $this->db->query($query)->result_array();
  if(count($leftid)>0)
  {
   $countpairs=1;
   $this->data['totalleftleg']=$this->total_members_down($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg']=0;
  }

  $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$member_id;
  $rightid= $this->db->query($query)->result_array();

  if(count($rightid)>0)
  {
   $countpairs=1;
   $this->data['totalrightleg']= $this->total_members_down($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg']=0;
  }

//////
  if(count($leftid)>0)
  {
   if($leftid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }
   $this->data['totalleftleg_bv']=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg_bv']=0;
  }


  if(count($rightid)>0)
  {
   if($rightid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }

   $this->data['totalrightleg_bv']= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg_bv']=0;
  }
//////


$sql="select leftcount,rightcount,totalleftpv,totalrightpv from tbl_member where id=$member_id ";
    $valo = $this->db->query($sql)->result_array();
    


    $this->data['totalleftpv']=$valo[0]['totalleftpv'];
    $this->data['totalrightpv']=$valo[0]['totalrightpv'];

  $str='';
  $str1=$this->m_treeview($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_new';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function level_up(){
  $member_id=$this->input->post('id');
  $user_name= $this->session->userdata('member_id');

  if($user_name!=$member_id)
  {
   $member_id=$this->db->query("Select parent_id FROM tbl_member where id=$member_id ")->row()->parent_id;
  }

  $this->data['member']=$this->db->query("Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id")->result_array();

  $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$member_id;
  $leftid= $this->db->query($query)->result_array();
  if(count($leftid)>0)
  {
   $countpairs=1;
   $this->data['totalleftleg']=$this->total_members_down($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg']=0;
  }

  $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$member_id;
  $rightid= $this->db->query($query)->result_array();

  if(count($rightid)>0)
  {
   $countpairs=1;
   $this->data['totalrightleg']= $this->total_members_down($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg']=0;
  }

//////
  if(count($leftid)>0)
  {
   if($leftid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }
   $this->data['totalleftleg_bv']=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalleftleg_bv']=0;
  }


  if(count($rightid)>0)
  {
   if($rightid[0]['tempo']!=1){
            $countpairs=0;
        }
        else{
            $countpairs=1;
        }

   $this->data['totalrightleg_bv']= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
  }
  else{
    $this->data['totalrightleg_bv']=0;
  }
//////
  $str='';
  $str1=$this->m_treeview($member_id,$str,0);
  $this->data['str']=$str1;
  $this->data['content']='tree_view_new';
  $this->data['title']='Tree View | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function m_treeview($userid,$str,$level)
{
  $sql="SELECT a.* , b.username,b.email FROM tbl_member a,  tbl_login b where a.parent_id=$userid and a.id=b.member_id";
  $records=$this->db->query($sql)->result_array();
  if($level==2)
  {return $str;}

  if( count($records)==0 ){

      $records[0]['id']=0;
      $records[0]['leg']=1;
      $records[0]['image']='images/vacant.png';
      $records[0]['username']='vacant';
      $records[0]['email']='vacant';

      $records[1]['id']=0;
      $records[1]['leg']=0;
      $records[1]['image']='images/vacant.png';
      $records[1]['username']='vacant';
      $records[1]['email']='vacant';


  }


  if(count($records)>0)
  {
if( count($records)==1 ){
  if($records[0]['leg']==0)
  {
    $records[1]['id']=0;
    $records[1]['leg']=1;
    $records[1]['image']='images/vacant.png';
    $records[1]['username']='vacant';
    $records[1]['email']='vacant';
  }
  else if($records[0]['leg']==1)
  {
    $records[1]['id']=0;
    $records[1]['leg']=0;
    $records[1]['image']='images/vacant.png';
    $records[1]['username']='vacant';
    $records[1]['email']='vacant';
  }

}

    $level++;
    $records=$this->array_msort($records,array('leg'=>SORT_ASC));
   $str.='<ul>';
   $view_url=base_url().'index.php/team/treeview_details';
   $add_url=base_url().'index.php/team/add_member';
  foreach($records as $row)
    {

    if($row['rank']==0){
      $rank="Distributor";
    }
    if($row['rank']==1){
      $rank="Bronze";
    }
    else if($row['rank']==2){
      $rank="Silver";
    }
    else if($row['rank']==3){

      $rank="Platinum";
    }
    else if($row['rank']==4){

      $rank="Gold";
    }
    else if($row['rank']==5){

      $rank="Pearl";
    }
    else if($row['rank']==6){

      $rank="Diamond";
    }
    else if($row['rank']==7){
      $rank="Ruby";
    }
    else if($row['rank']==8){
      $rank="Red Diamond";
    }
    else if($row['rank']==9){

      $rank="Kohinoor";
    }
    else if($row['rank']==10){

      $rank="Ever Green Wallet";
    }


      if($row['username']!='vacant'){

//         $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$row['id'];
//         $leftid= $this->db->query($query)->result_array();
//         if(count($leftid)>0)
//         {
//          $countpairs=1;
//          $totalleftleg=$this->total_members_down($leftid[0]['id'],$countpairs);
//         }
//         else{
//           $totalleftleg=0;
//         }

//         $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$row['id'];
//         $rightid= $this->db->query($query)->result_array();

//         if(count($rightid)>0)
//         {
//          $countpairs=1;
//          $totalrightleg= $this->total_members_down($rightid[0]['id'],$countpairs);
//         }
//         else{
//           $totalrightleg=0;
//         }

//         ////
//          if(count($leftid)>0)
//   {
//   if($leftid[0]['tempo']!=1){
//             $countpairs=0;
//         }
//         else{
//             $countpairs=1;
//         }
//   $totalleftleg_bv=$this->total_members_down_bv($leftid[0]['id'],$countpairs);
//   }
//   else{
//     $totalleftleg_bv=0;
//   }


//   if(count($rightid)>0)
//   {
//   if($rightid[0]['tempo']!=1){
//             $countpairs=0;
//         }
//         else{
//             $countpairs=1;
//         }

//   $totalrightleg_bv= $this->total_members_down_bv($rightid[0]['id'],$countpairs);
//   }
//   else{
//     $totalrightleg_bv=0;
//   }
/////


$sql="select leftcount,rightcount from tbl_member where id=".$row['id'];
    $valo = $this->db->query($sql)->result_array();
    
    
    $totalleftleg=$valo[0]['leftcount'];
    $totalrightleg=$valo[0]['rightcount'];

// <tr><td>BV:</td><td>'.($totalleftleg_bv*2000).'</td><td>'.($totalrightleg_bv*2000).'</td><td>'.(($totalleftleg_bv*2000)+($totalrightleg_bv*2000)).'</td><tr>

        $image='images/user.png';
        $str.='<li><a onclick="action_without_msg(\''.$view_url.'\', '.$row['id'].')" class="mytooltip" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-content="'.$this->session->userdata('name').'" data-original-title="<br>Name: '.$row['name'].'<br> Contact: '.$row['contact_number'].' <br> Package Price: '.$row['package_price'].'
<br><table >
<tr ><td></td><td>Left </td><td> Right</td><td> Total</td><tr>
<tr><td>Member:</td><td>'.$totalleftleg.'</td><td>'.$totalrightleg.'</td><td>'.($totalrightleg+$totalleftleg).'</td><tr>

</table>"  

 style=" text-transform:capitalize;font-size:11px; font-weight:600;">
  <img src="'.base_url().'assets/images/user/avatar-2.jpeg" style="width:40px;">'.$row['username'].'
        </a>';
        $str= $this->m_treeview($row['id'],$str,$level);
        $str.='</li>';
      }
      else{
        $image=$row['image'];
        //onclick="action_without_msg_member(\''.$add_url.'\', '.$userid.','.$row['leg'].')"
        $str.='<li><a   class="mytooltip" style=" text-transform:capitalize;font-size:11px; font-weight:600;">
  <img src="'.base_url().$image.'" style="width:40px;">

        '.$row['username'].'</a>';

        $str.='</li>';
      }
    }
  $str.='</ul>';
  }
    return $str;
}

 public function direct_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Direct' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
  $this->data['spillincome']=$this->m_default->get_user_list($sql);
  $this->data['content']='listdirectincome';
  $this->data['title']='Direct Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function directmember(){
  $member_id=$this->session->userdata('member_id');
  // $sql="SELECT a.* , b.username,b.email FROM tbl_member a,  tbl_login b where a.sponser_id=$member_id and a.id=b.member_id";
 
 
  $query="SELECT a.id,a.name ,a.sponser_id,a.contact_number,a.created_on,a.member_status, b.username,b.email FROM tbl_member a,  tbl_login b where a.leg =0 and a.parent_id=$member_id and a.id=b.member_id";    
  $leftid= $this->db->query($query)->result_array();  
  
 
  $query="SELECT a.id,a.name ,a.sponser_id,a.contact_number ,a.created_on, a.member_status, b.username,b.email FROM tbl_member a,  tbl_login b where a.leg =1 and a.parent_id=$member_id and a.id=b.member_id";
  $rightid= $this->db->query($query)->result_array();
  

  $this->data['leftred']=0;
  $this->data['rightred']=0;
  
  $this->data['leftgreen']=0;
  $this->data['rightgreen']=0;
  
  
  //$this->data['directmembers']=$this->m_default->get_user_list($sql);
  
 $this->data['directmembersright']=array();
   
   $arr=array();
   
   if($leftid[0]['id']!=""){
       $this->data['directmembers']=$this->downlinerecursiondirect($leftid[0]['id'],$arr,$member_id);
   }
   
   if($rightid[0]['id']!=""){
       $this->data['directmembersright']=$this->downlinerecursiondirect($rightid[0]['id'],$arr,$member_id);
   }
   
  
  
  
  if($leftid[0]['sponser_id']==$member_id){
      $this->data['directmembers'] = array_merge($leftid,$this->data['directmembers']);
  }
 
 
 
 if($rightid[0]['sponser_id']==$member_id){
      $this->data['directmembersright'] = array_merge($rightid,$this->data['directmembersright']);
  }

 foreach($this->data['directmembers'] as $key=>$value){
     
     if($this->data['directmembers'][$key]['member_status']==0)
     {
         $this->data['leftred']++;
     }
     
     if($this->data['directmembers'][$key]['member_status']==1)
     {
         $this->data['leftgreen']++;
     }
      
     
      //$this->data['directmembers'][$key]['sponsername']=$this->db->query('select username from tbl_login where member_id='.$this->data['directmembers'][$key]['sponser_id'])->row()->username;
  }
  
  
  foreach($this->data['directmembersright'] as $key=>$value){
      //$this->data['directmembersright'][$key]['sponsername']=$this->db->query('select username from tbl_login where member_id='.$this->data['directmembersright'][$key]['sponser_id'])->row()->username;
  
      
      if($this->data['directmembersright'][$key]['member_status']==0)
     {
         $this->data['rightred']++;
     }
     
     if($this->data['directmembersright'][$key]['member_status']==1)
     {
         $this->data['rightgreen']++;
     }
  }
   
 
  $this->data['content']='list_directmembers';
  $this->data['title']='Direct Member | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function downline(){
  $member_id=$this->session->userdata('member_id');
  
  $query="SELECT a.sponser_id,a.id,a.name , a.contact_number,a.member_status,a.created_on , b.username,b.email FROM tbl_member a,  tbl_login b where a.leg =0 and a.parent_id=$member_id and a.id=b.member_id";    
  $leftid= $this->db->query($query)->result_array();  
  
 
  $query="SELECT a.sponser_id,a.id,a.name , a.contact_number,a.member_status,a.created_on , b.username,b.email FROM tbl_member a,  tbl_login b where a.leg =1 and a.parent_id=$member_id and a.id=b.member_id";
  $rightid= $this->db->query($query)->result_array();
  

  $this->data['leftred']=0;
  $this->data['rightred']=0;
  
  $this->data['leftgreen']=0;
  $this->data['rightgreen']=0;
  
  
  
  if($leftid[0]['id']!="" && $leftid[0]['id']!=null){
  $this->data['downline']=$this->downlinerecursion($leftid[0]['id'],$leftid);}
  
  if($rightid[0]['id']!="" && $rightid[0]['id']!=null){
  $this->data['downlineright']=$this->downlinerecursion($rightid[0]['id'],$rightid);
      
  }
  //  print_r($this->data['downline']);

 foreach($this->data['downline'] as $key=>$value){
     
     if($this->data['downline'][$key]['member_status']==0)
     {
         $this->data['leftred']++;
     }
     
     if($this->data['downline'][$key]['member_status']==1)
     {
         $this->data['leftgreen']++;
     }
      
     
      $this->data['downline'][$key]['sponsername']=$this->db->query('select username from tbl_login where member_id='.$this->data['downline'][$key]['sponser_id'])->row()->username;
  }
  
  
  foreach($this->data['downlineright'] as $key=>$value){
      $this->data['downlineright'][$key]['sponsername']=$this->db->query('select username from tbl_login where member_id='.$this->data['downlineright'][$key]['sponser_id'])->row()->username;
  
      
      if($this->data['downlineright'][$key]['member_status']==0)
     {
         $this->data['rightred']++;
     }
     
     if($this->data['downlineright'][$key]['member_status']==1)
     {
         $this->data['rightgreen']++;
     }
  }
  $this->data['content']='list_downline';
  $this->data['title']='List Downline | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function downlinerecursion($member_id,$arr)
{
  $sql="SELECT a.sponser_id,a.id,a.name , a.contact_number,a.member_status,a.created_on ,  b.username,b.email FROM tbl_member a,  tbl_login b where a.parent_id=$member_id and a.id=b.member_id";
  $records=$this->db->query($sql)->result_array();

  if(count($records)>0)
  {
  foreach($records as $row)
    {
      $arr = $this->downlinerecursion($row['id'],$arr);
    }
    $arr = array_merge($records,$arr);
  }
    return $arr;
}


public function downlinerecursiondirect($member_id,$arr,$sponser_id)
{
   
  $sql="SELECT a.created_on,a.id,a.name ,a.contact_number,a.sponser_id,a.member_status, b.username,b.email FROM tbl_member a,  tbl_login b where a.parent_id=$member_id and a.id=b.member_id";
  $records=$this->db->query($sql)->result_array();

 /* if(count($records)>0)
  {*/
  foreach($records as $key=>$row)
    {
       $recordsponser=$records;
        if($records[$key]['sponser_id']==$sponser_id){
            
            $arr = array_merge($recordsponser,$arr);
           
        }
        else{
            unset($recordsponser[$key]);
        }
        
        
      $arr = $this->downlinerecursiondirect($row['id'],$arr,$sponser_id);
    }
    
  //}
    return $arr;
}



public function matchingincome()
{
  $member_id=$this->session->userdata('member_id');
  /*$sql="SELECT c.*  FROM tbl_bonus c WHERE  c.bonus_type='MBV' AND c.member_id=$member_id";
  $this->data['directmembers']=$this->m_default->get_user_list($sql);*/
  $sql="SELECT c.*  FROM tbl_matching_amount c WHERE c.member_id=$member_id";
  $this->data['matchingincome']=$this->m_default->get_user_list($sql);
  $this->data['content']='list_matchingbonus';
  $this->data['title']='Matching Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function team_royalty()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Team Royalty' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
  $this->data['teamroyalty']=$this->m_default->get_user_list($sql);
  $this->data['content']='list_teamroyalty';
  $this->data['title']='Team Royalty Bonus | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function level_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_levelincome';
  $this->data['title']='Level Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function matrix_level_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Matrix Level' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_matrixlevelincome';
  $this->data['title']='Level Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function rank_match_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Rank Match' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_rank_match';
  $this->data['title']='Rank Match Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function roi()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='ROI' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_cashback';
  $this->data['title']='ROI Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function cashback_summary()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level,c.direct_match, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Cash Back' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_cashbacksummary';
  $this->data['title']='Cash Back Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


public function club_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Club' AND c.member_id=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_clubincome';
  $this->data['title']='Club Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function sponser_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Sponser' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_sponserincome';
  $this->data['title']='Sponser Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


public function bv()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level Bonus' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
  $this->data['listbv']=$this->db->query($sql)->result_array();
  $this->data['content']='list_bv';
  $this->data['title']='BV | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}
public function team_development()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  * FROM tbl_development_bonus c WHERE  c.member_id=$member_id ";
  $this->data['teamdevelopment']=$this->db->query($sql)->result_array();
  $this->data['content']='list_teamdevelopment';
  $this->data['title']='Team Development Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function matrix_count()
{
  $member_id=$this->session->userdata('member_id');
  $this->data['level1']= $this->total_members_down_level($member_id,0,0,1);
  $this->data['level2']= $this->total_members_down_level($member_id,0,0,2);
  $this->data['level3']= $this->total_members_down_level($member_id,0,0,3);
  $this->data['level4']= $this->total_members_down_level($member_id,0,0,4);
  $this->data['level5']= $this->total_members_down_level($member_id,0,0,5);
  $this->data['level6']= $this->total_members_down_level($member_id,0,0,6);
  $this->data['level7']= $this->total_members_down_level($member_id,0,0,7);
  $this->data['level8']= $this->total_members_down_level($member_id,0,0,8);
  $this->data['level9']= $this->total_members_down_level($member_id,0,0,9);
  $this->data['level10']= $this->total_members_down_level($member_id,0,0,10);

  $this->data['content']='list_matrixcount';
  $this->data['title']='Matrix Count | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function total_members_down_level($userid,$count,$level,$levelconstant)
  {
     $sql="SELECT * FROM tbl_matrix where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    //$count+=count($records);
    $level++;

    if($level==$levelconstant)
    {
        $count+=count($records);
        return $count;

    }


    /*if($count>0)
    {*/
    foreach($records as $row)
      {
        $count= $this->total_members_down_level($row['member_id'],$count,$level,$levelconstant);
      }
    //}
      return $count;
  }





function array_msort($array, $cols)
{
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
    }
    $eval = substr($eval,0,-1).');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k,1);
            if (!isset($ret[$k])) $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;

}

	public function add_member()
	{
	 $id=$this->input->post('id');

	 if(!empty($id)){
	 $this->data['sponser_id']= $query=$this->db->query("select username from tbl_login where member_id=$id")->row()->username;
	 $this->data['spname']= $query=$this->db->query("select name from tbl_member where id=$id")->row()->name;
	 }


	 $this->session->set_userdata('addmemberid',$id);
	 $direction=$this->input->post('direction');
	 $this->session->set_userdata('direction',$direction);

	 if($direction==0){
	      $this->data['direction']='Left';
	 }
	 if($direction==1){
	      $this->data['direction']='Right';
	 }

     $this->data['title']='Add Member | Ever Green Wallet';
     $this->data['content']='innersignup';
	 $this->load->view('common/template',$this->data);
	}

public function save_member()
	{
        $this->form_validation->set_rules('email', 'Email', 'is_unique[tbl_login.email]');

		$this->form_validation->set_rules('sponser_id', 'Sponser ID', 'required');
		$this->form_validation->set_rules('direction', 'Direction', 'required');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('contact_no', 'Contact No', 'required');
	  
		$this->form_validation->set_rules('plan', 'Plan', 'required');
		$plan=$this->input->post('plan');

		if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error_login',validation_errors());
				redirect('team/add_member');
			}
			else{
       $sponser_id=$this->input->post('sponser_id');

			 $query= $query=$this->db->query("select a.member_id from tbl_login a,tbl_member b where a.username='$sponser_id'  and b.id=a.member_id 
   and b.member_status=1")->result_array();

       if(count($query)>0)
			 {

                $direction=$this->session->userdata('direction');

                $parent_id=$this->session->userdata('addmemberid');
                
                
                $this->recursive_leg_sponser($parent_id,$direction);


              $legcount=  $this->db->query("select count(id) as count from tbl_member where parent_id=$parent_id and leg=$direction")->row()->count;

if($legcount>0)
{
 $this->session->set_flashdata('error_login','Maximum Legs Exceed');
				redirect('team/add_member');
}

 				$name=$this->input->post('name');

 				$contact_no=$this->input->post('contact_no');
 				$email=$this->input->post('email');
 				//$user_id=$this->input->post('user_id');

 				$password=$this->rand_string(8);
 				//$password=$this->input->post('password');
 				
 				$country=$this->input->post('country');
				$bitcoin_address=$this->input->post('bitcoin_address');
                $bank_name=$this->input->post('bank_name');
 				$ifsc_code=$this->input->post('ifsc_code');
 				$account_holder_name=$this->input->post('account_holder_name');
 				$branch=$this->input->post('branch');
 				$account_number=$this->input->post('account_number');
 				$address=$this->input->post('address');
				$date=$this->input->post('date');
                $city=$this->input->post('city');
                $pan_no=$this->input->post('pan_no');
                $nominee_name=$this->input->post('nominee_name');
                $relationship=$this->input->post('relationship');
                $birth_date=$this->input->post('birth_date');
                $nominee_name=$this->input->post('nominee_name');
                $dob=$this->input->post('dob');
                $gender=$this->input->post('gender');
                $pin_code=$this->input->post('pin_code');

  		     if($plan==7)
                {
                    $pv=10;
                }
                else if($plan==8)
                {
                    $pv=20;
                }
                else{
                    $pv=1;
                }
                
                if($plan==1){
                    $package_price=1000;
                }
                else if($plan==2){
                    $package_price=2000;
                }
                 else if($plan==3){
                    $package_price=5000;
                }
                else if($plan==4){
                    $package_price=8000;
                }
                else if($plan==5){
                    $package_price=12000;
                }
                else if($plan==6){
                    $package_price=15000;
                }
                else if($plan==7){
                    $package_price=9999;
                }
                else if($plan==8){
                    $package_price=19999;
                }
                
                
                
  		    	$data = array('package_price'=>$package_price,'pv'=>$pv,'relationship'=>$relationship,'birth_date'=>$birth_date,'nominee_name'=>$nominee_name,'dob'=>$dob,'gender'=>$gender,'pin_code'=>$pin_code,'pan_no'=>$pan_no,'city'=>$city,'package_id'=>$plan,'name' => $name,'sponser_id'=>$query[0]['member_id'], 'leg'=> $direction,'contact_number'=>$contact_no,'country'=>$country,'parent_id'=>$parent_id,'user_image'=>'user.png','bitcoin_address'=>$bitcoin_address,'bank_name'=>$bank_name,'ifsc_code'=>$ifsc_code,'account_holder_name'=>$account_holder_name,
				'branch'=>$branch,'account_number'=>$account_number,'address'=>$address,'date'=>date('Y-m-d',strtotime($date)));
				$member_id=$this->m_default->data_insertwithid('tbl_member',$data);

                $shinerandy=$this->rand_string(5);
				$userid='IW000'.$member_id;

				$logindata = array('member_id' => $member_id,'email'=>$email, 'username'=>$userid,'password'=>md5($password),'usertype'=>2);
				$this->m_default->data_insert('tbl_login',$logindata);

				$records=array();
				// $sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$query[0]['member_id'];
				// $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
				// $records=$this->db->query($sql)->result_array();

				// if(count($records)<2)
				// {
				//  $matrixparent_id= 2;
				// }
				// else{

				// 	$matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
				// }


				// $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
				// $this->m_default->data_insert('tbl_matrix',$matrixdata);


				$walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
				$this->m_default->data_insert('tbl_wallet',$walletdata);
				
					$walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
				$this->m_default->data_insert('tbl_fundwallet',$walletdata);
				
			
				$this->m_default->data_insert('tbl_repurchase',$walletdata);
				
				$array=array('member_id'=>$member_id,'left_used'=>0,'right_used'=>0,'total_pairs'=>0,'rank'=>500);
                $this->m_default->data_insert('tbl_pairsdata',$array);
				

				$teamdevelopdata = array('member_id' => $member_id,'left_ids'=>'','right_ids'=>'');
				$this->m_default->data_insert('tbl_team_development',$teamdevelopdata);
				
				// for($rank=0;$rank<=9;$rank++){
				// $pairsdata = array('member_id' => $member_id,'left_used'=>0,'right_used'=>0,'rank'=>$rank);
				// $this->m_default->data_insert('tbl_pairsdata',$pairsdata);
				// }
				
 $array=array('member_id'=>$member_id,'coins'=>0);
        $this->m_default->data_insert('tbl_coin',$array);
        
        $array=array('member_id'=>$member_id,'coins'=>0);
        $this->m_default->data_insert('tbl_release_wallet',$array);
        

				$this->session->set_flashdata('success','Registered Successfully..! <br>Your Username is '.$userid. ' and Password is '.$password );
				redirect('team/add_member');

			 }
			 else {
          $this->session->set_flashdata('error_login','Sponser Id Does not exist..!');
 					redirect('team/add_member');
 			}
			redirect('team/add_member');
			}
			redirect('team/add_member');
	}
	
	
		public function recursive_leg_sponser($userid,$leg)
	{
	   $sql="SELECT parent_id,leg FROM tbl_member  where id=$userid";
	  $records=$this->db->query($sql)->result_array();
	  
	  	if($leg==0){
	      $this->db->query("update tbl_member set leftcount=leftcount+1 where id=".$userid);
	  }
	  
	   if($leg==1){
	      $this->db->query("update tbl_member set rightcount=rightcount+1 where id=".$userid);
	  }
	  
	  
	  $leg=$records[0]['leg'];
	
			foreach($records as $row)
		    {
					return $this->recursive_leg_sponser($row['parent_id'],$leg);
			}

	}

	function rand_string( $length ) {

    $chars = "0123456789";
    return substr(str_shuffle($chars),0,$length);

}

public function recursive_leg_matrix($arraystr)
		{

	$str='';
	$records=explode(',',$arraystr);

				foreach($records as $row){
					if($row!=""){
					$sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$row;
				  $recordsnew1=$this->db->query($sql)->result_array();

					if(count($recordsnew1)<2){

							return $row;
					}
					else{


						$str.=$recordsnew1[0]['member_id'].','.$recordsnew1[1]['member_id'].',';
					}
	}
				  }

				return	 $this->recursive_leg_matrix($str);

				}

}
