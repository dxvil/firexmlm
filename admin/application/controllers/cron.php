<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Cron extends CI_Controller {

	/**
	 * Cron Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct(){
    parent::__construct();
    $this->load->model('m_default');
  }

  public function cron_matrix(){
    $query = "SELECT id, rank,package_id FROM  tbl_member where id!=1 ";
    $num_rows = $this->db->query($query)->result_array();
    $pairs='';
    $teamroyalty = array();
    $royaltycount=0;
    $this->db->trans_begin();
    foreach ($num_rows as $key => $value) {
      $member_id=$value['id'];
      $matrixcount=  $this->db->query("select count(id) as count from tbl_matrix where member_id=".$value['id'])->row()->count;
      if($matrixcount==0 || $matrixcount==""){
        $wallamount=$this->db->query("select amount from tbl_wallet where member_id=".$num_rows[$key]['id'])->row()->amount;
        if($wallamount>=400){
          $query="update tbl_wallet set amount=amount-400 where member_id=".$num_rows[$key]['id'];
          $this->m_default->execute_query($query);
          $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;
          $array=array('member_id'=>$member_id,'desc'=>"Level Upgrade",'debited'=>400,'balance'=>$debitedamount,'status'=>1);
          $this->m_default->data_insert('tbl_wallet_report',$array);
          $records=array();
	        $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
	        $records=$this->db->query($sql)->result_array();
	        if(count($records)<2){
	          $matrixparent_id= 2;
	        }
	        else{
		        $matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
	        }
	        $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
	        $this->m_default->data_insert('tbl_matrix',$matrixdata);
        }
      }
    }
    $this->db->trans_commit();
  }

  public function fix_coin_issues(){


  }

  public function recursive_leg_matrix($arraystr){
	  $str='';
	  $records=explode(',',$arraystr);
	  foreach($records as $row){
		if($row!=""){
			$sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$row;
			$recordsnew1=$this->db->query($sql)->result_array();
			if(count($recordsnew1)<2){
				return $row;
		  }
			else{
				$str.=$recordsnew1[0]['member_id'].','.$recordsnew1[1]['member_id'].',';
			}
    }
	}
	return	 $this->recursive_leg_matrix($str);
	}
	
	
	
	public function get_max(){
  $listmember= $this->db->query('select a.id,a.package_price,a.package_id,a.package_add_date,b.coins from tbl_member a, tbl_coin b where a.id=b.member_id and a.id!=1 and a.package_id!=0')->result_array();
   
  foreach($listmember as $val){
        $member_id = $val['id'];
     
        $package_id = $val['package_id'];
      
        $date1=date_create(date("Y-m-d",strtotime($val['package_add_date'])));
        $date2=date_create(date("Y-m-d"));
        $diff=date_diff($date1,$date2);
        $diffdate= $diff->format("%a");
      
        if($diffdate==30 ||  $diffdate==60 || $diffdate==90){
          
           if($package_id==2  || $package_id==4 || $package_id==6 || $package_id==8 || $package_id==9 || $package_id==10 || $package_id==11 || $package_id==12 || $package_id==13){ 
             
            $rWallet= ($val['coins']*30)/100;
            
            $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
            
             $array=array('percent'=>30,'member_id'=>$member_id,'coins'=>$rWallet);
             $this->m_default->data_insert('tbl_get_rcoin',$array);
            
            } 
        
        }
         
      
    //   $array=array('member_id'=>$member_id,'coins'=>$rWallet);
    //   $this->m_default->data_insert('tbl_release_wallet',$array);
      
    //   $array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
    //     $this->m_default->data_insert('tbl_member_package',$array);
      
  }
}


public function block_users(){

  $users = $this->db->query("SELECT c.username from tbl_coin as a left join tbl_member as b on a.member_id = b.id left join tbl_login as c on c.member_id = b.id WHERE a.coins >= 500000")->result_array();
  foreach($users as $val){
    $username = $val['username'];
    //print_r($val['username']);
    $this->db->query("UPDATE tbl_login SET status = 1 WHERE username = '$username'");
  }

}


public function xu(){
  $date = $this->input->get('date');
  $lists = $this->db->query("SELECT coins, id, member_id, COUNT(member_id) as total_rows FROM tbl_get_rcoin WHERE DATE(created_on) = '$date' GROUP BY member_id HAVING COUNT(member_id) = 2")->result_array();
  foreach($lists as $val){
    $rWallet = $val['coins'];
    $member_id = $val['member_id'];
    $id = $val['id'];
    $this->db->query("update tbl_release_wallet set coins=coins-$rWallet where member_id=$member_id");
    $this->db->query("DELETE FROM tbl_get_rcoin WHERE id = $id");
  }

}


public function poi(){
  
  
  foreach ($mn as $dd){

  $listmember= $this->db->query('select a.id,a.package_price,a.package_id,a.package_add_date,b.coins from tbl_member a, tbl_coin b where a.id=b.member_id and a.id!=1 and a.package_id!=0')->result_array();
   
  foreach($listmember as $val){
              $member_id = $val['id'];
          
              $package_id = $val['package_id'];
            
              $date1=date_create(date("Y-m-d",strtotime($val['package_add_date'])));
              $date2=date_create(date("Y-m-d"));
              $diff=date_diff($date1,$date2);
              $diffdate= $diff->format("%a");
            
              if($diffdate==30 ||  $diffdate==60 || $diffdate==90){
                
                if($package_id==2  || $package_id==4 || $package_id==6 || $package_id==8 || $package_id==9 || $package_id==10 || $package_id==11 || $package_id==12 || $package_id==13){ 
                  
                  $rWallet= ($val['coins']*30)/100;
                  
                  $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
                  
                  $array=array('percent'=>30,'member_id'=>$member_id,'coins'=>$rWallet);
                  $this->m_default->data_insert('tbl_get_rcoin',$array);
                  
                  } 
              
              }
            
        }
    }

  }

}
