<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Home Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct()
  {
       parent::__construct();
       $username= $this->session->userdata("username");
       $userType=$this->session->userdata('usertype');
       if (empty($username)) {
          $this->session->set_flashdata("error_login", "Invalid Request");
          redirect("login", "refresh");
       }
       $this->load->model('m_default');
       foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
  }

  public function index()
  {
    $this->data['tax']=$this->m_default->get_user_list('Select id,tax_name from tbl_tax');
    $this->data['content']='product/add_product';
    $this->data['title']='Add Product | FIREXCOIN';
    $this->load->view('common/template',$this->data);
    }
    public function sell_product()
    {
      $member_id=$this->session->userdata('member_id');
      $sql="SELECT  member_id,username FROM tbl_login WHERE member_id!=$member_id ";
      $this->data['members']=$this->db->query($sql)->result_array();

      $sql="SELECT  `product_name`, `product_id`, `id` FROM tbl_product";
      $this->data['products']=$this->db->query($sql)->result_array();

      $this->data['content']='product/sell_product';
      $this->data['title']='Sell Product | Ever Green Wallet';
      $this->load->view('common/template',$this->data);
      }

        public function sold_product(){
        $member_id=  $this->input->post('member_id');
        $product_id=  $this->input->post('product_id');
        $member_name=  $this->input->post('member_name');
        $product_price=  $this->input->post('product_price');
        $payment=  $this->input->post('payment');
          $billing_date=  $this->input->post('billing_date');

        // $quantity=  $this->input->post('quantity');
        $tax=0;$sub_total=0;
        foreach ($product_id as $value) {
          $quantity= $this->input->post('quantity'.$value);
          $sql="select a.`product_mrp`,a.product_bv,b.`percentage` from `tbl_tax` b, `tbl_product` a where a.id=$value and a.tax_id=b.id";

          $productdetails=$this->db->query($sql)->result_array();
          $quantity= $this->input->post('quantity'.$value);
          $tax+=($productdetails[0]['product_mrp']*$quantity*$productdetails[0]['percentage'])/100;
          $sub_total+=$productdetails[0]['product_mrp']*$quantity;

            $totalbv=$productdetails[0]['product_bv']*$quantity;
          $this->savedetails($member_id,$value,0,$totalbv,'Repurchase','N/A');
        }

  $data = array('payment_mode'=>$payment, 'billing_date'=>date('Y-m-d',strtotime($billing_date)), 'member_id' => $member_id,'tax_amount'=>$tax, 'sub_total'=>$sub_total, 'grand_total'=>($sub_total+$tax));
  $invoice_id=  $this->m_default->data_insertwithid('tbl_invoice', $data);

foreach ($product_id as $value) {
  $sql="select a.`product_mrp`,a.product_bv,b.`percentage` from `tbl_tax` b, `tbl_product` a where a.id=  $value and a.tax_id=b.id";

  $productdetails=$this->db->query($sql)->result_array();
  $quantity= $this->input->post('quantity'.$value);
  $tax=($productdetails[0]['product_mrp']*$quantity*$productdetails[0]['percentage'])/100;

  $data = array('invoice_id'=>$invoice_id,'member_id' => $member_id,'product_id' =>$value,'tax'=>$productdetails[0]['percentage'],'tax_amount'=>$tax,'bv_earned'=>$productdetails[0]['product_bv'],'price'=>$productdetails[0]['product_mrp'],'quantity'=>$quantity);
  $this->m_default->data_insertwithid('tbl_invoice_details', $data);
}

         $sql="Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id=$member_id and a.id=b.member_id";
         $this->data['member_details']=$this->db->query($sql)->result_array();

         $this->data['org_logo'] = base_url().'uploads/company_logo-sample.jpg';
         $this->data['org_name'] = 'Plotus';
         $this->data['email'] = 'plutodemo1@gmail.com';
         $this->data['contact'] = '+656526565363';
         $this->data['address'] = '45/5 Bihar bh,<br>Pin 789546';
         $this->data['org_code'] = 'PL';
         $this->data['invoice_color']='#000';

         $sql="select *  from tbl_invoice where id=$invoice_id";

         $this->data['invoice']=$this->db->query($sql)->result_array();

         $sql="select a.*, b.product_name  from tbl_invoice_details a, tbl_product b where a.invoice_id=$invoice_id and b.id=a.product_id";

         $this->data['invoice_details']=$this->db->query($sql)->result_array();

        $this->data['content']='invoice';
        $this->data['title']='Invoice | Ever Green Wallet';
        $this->load->view('common/template',$this->data);
      }

      public function savedetails($parent_id,$member_id,$amount,$bv_points,$bonustype,$level)
      {
        $array=array('member_id'=>$parent_id,'child_id'=>$member_id,'amount'=>$amount,'bv_amount'=>$bv_points,'bonus_type'=>$bonustype,'level'=>$level);
        $this->m_default->data_insert('tbl_bonus',$array);

        $query="update tbl_wallet set amount=amount+$amount where member_id=$parent_id";
        $this->m_default->execute_query($query);
      }

      public function getpackage()
        {
        $package=$this->input->post('package');
        if(empty($package)){
          echo '';
        }
        else{
        $str='';
        foreach ($package as $value) {
            $package_mrp=  $this->db->query("Select product_mrp from tbl_product where id=$value")->row()->product_mrp;
            $product_dp=  $this->db->query("Select product_dp from tbl_product where id=$value")->row()->product_dp;
            $product_bv=  $this->db->query("Select product_bv from tbl_product where id=$value")->row()->product_bv;

            $str.= '

              <div class="col-md-3" ><div class="form-group">
            <label for="exampleInputuname">Product Price</label>
                <input type="number" class="form-control"  placeholder="Quantity" value="'.$package_mrp.'" required>
            </div></div>
            <div class="col-md-3" >
            <div class="form-group">
            <label for="exampleInputuname">Product DP</label>
                <input type="number" class="form-control"  placeholder="Quantity" value="'.$product_dp.'" required>
            </div></div>
            <div class="col-md-3" >
            <div class="form-group">
            <label for="exampleInputuname">Product BV</label>
                <input type="number" class="form-control"  placeholder="Quantity" value="'.$product_bv.'" required>
                </div>
            </div>
  <div class="col-md-3" >
            <div class="form-group">
                <label for="exampleInputuname">Quantity</label>
                    <input type="number" class="form-control" id="quantity'.$value.'"  name="quantity'.$value.'" placeholder="Quantity" value="" required>

            </div>
              </div>

            ';
        }
        echo $str;
      }

        }
        public function getmembername()
        {
        $member_id=  $this->input->post('member_id');
        echo  $this->db->query("Select name from tbl_member where id=$member_id")->row()->name;
        }

  public function save_product()
  {
    $product_id=$this->input->post('product_id');
    $product_name=$this->input->post('product_name');
    $product_mrp=$this->input->post('product_mrp');
    $product_dp=$this->input->post('product_dp');
    $tax_id=$this->input->post('tax_id');
    $quantity=$this->input->post('quantity');
    $product_bv=$this->input->post('product_bv');

    $config['upload_path'] = './uploads/product/';
    $config['allowed_types'] = '*';
    $config['file_name'] = 'product' . date('YmdHis');
    $this->load->library('upload', $config);
    $upload = $this->upload->do_upload('userfile');
    if (!$upload) {
      echo $error = $this->upload->display_errors();
      $this->session->set_flashdata('upload_user_image', $error);
      $this->data['member_img_error'] = 'There is some problems to upload image';die;

    }else {
    $upload_data = $this->upload->data();
    $image_path =  $config['file_name'] . $upload_data['file_ext'];

    $data = array('product_id' => $product_id,'product_name' =>$product_name,'product_mrp'=>$product_mrp,'product_dp'=>$product_dp,'tax_id'=>$tax_id,'quantity'=>$quantity,'product_bv'=>$product_bv,'image'=>$image_path);
    $this->m_default->data_insert('tbl_product', $data);
    $this->session->set_flashdata('success','Product Added Successfully..!');
    }
    redirect('product/list_product');
  }

  public function update_product()
  {
    $id=$this->input->post('id');
    $product_id=$this->input->post('product_id');
    $product_name=$this->input->post('product_name');
    $product_mrp=$this->input->post('product_mrp');
    $product_dp=$this->input->post('product_dp');
    $tax_id=$this->input->post('tax_id');
    $quantity=$this->input->post('quantity');
    $product_bv=$this->input->post('product_bv');
    $where=array('id'=>$id);
    $data = array('product_id' => $product_id,'product_name' =>$product_name,'product_mrp'=>$product_mrp,'product_dp'=>$product_dp,'tax_id'=>$tax_id,'quantity'=>$quantity,'product_bv'=>$product_bv);
    $this->m_default->update($where, $data, 'tbl_product');
    $this->session->set_flashdata('success','Product Edited Successfully..!');
    redirect('product/list_product');
  }

  public function list_product()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,b.tax_name FROM  tbl_product a,tbl_tax b WHERE a.tax_id=b.id and a.status=0";
    $this->data['listmember']=$this->db->query($sql)->result_array();
    $this->data['content']='product/list_product';
    $this->data['title']='List Product | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }


public function edit_product()
{$this->data['tax']=$this->m_default->get_user_list('Select id,tax_name from tbl_tax');
$id = $this->input->post('id');
$sql="SELECT  a.*  FROM  tbl_product a WHERE  a.id=$id";
$this->data['productdetails']=$this->db->query($sql)->result_array();
$this->data['id']=$id;
$this->data['content']='product/edit_product';
$this->data['title']='Edit Product | Ever Green Wallet';
$this->load->view('common/template',$this->data);
}

  public function activate() {
    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_product');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Product Activated Successfully..!');
      redirect('member');
    }
  }

  public function deactivate() {
    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_product');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Product Deactivated Successfully..!');
      redirect('product/list_product');
    }
  }

  public function list_invoice()
  {

    $sql="SELECT  a.* ,b.username FROM  tbl_invoice a,tbl_login b where b.member_id=a.member_id";
    $this->data['listinvoice']=$this->db->query($sql)->result_array();
    $this->data['content']='list_invoice';
    $this->data['title']='List Invoice | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function view_invoice()
  {
    $invoice_id=$this->input->post('id');
    $sql="select *  from tbl_invoice where id=$invoice_id";

    $this->data['invoice']=$this->db->query($sql)->result_array();


    $sql="Select a.* , b.username,b.email FROM tbl_member a,  tbl_login b  where a.id={$this->data['invoice'][0]['member_id']} and a.id=b.member_id";
    $this->data['member_details']=$this->db->query($sql)->result_array();

    $this->data['org_logo'] = base_url().'uploads/company_logo-sample.jpg';
    $this->data['org_name'] = 'Plotus';
    $this->data['email'] = 'plutodemo1@gmail.com';
    $this->data['contact'] = '+656526565363';
    $this->data['address'] = '45/5 Bihar bh,<br>Pin 789546';
    $this->data['org_code'] = 'PL';
    $this->data['invoice_color']='#000';

    $sql="select *  from tbl_invoice where id=$invoice_id";

    $this->data['invoice']=$this->db->query($sql)->result_array();


    $sql="select a.*, b.product_name  from tbl_invoice_details a, tbl_product b where a.invoice_id=$invoice_id and b.id=a.product_id";

    $this->data['invoice_details']=$this->db->query($sql)->result_array();

   $this->data['content']='invoice';
   $this->data['title']='Invoice | Ever Green Wallet';
   $this->load->view('common/template',$this->data);
  }
}
