<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payout extends CI_Controller {

  /**
   * Package Controller
   * Author: Dipanwita Chanda
   **/

  public function __construct()
  {
       parent::__construct();
       $username= $this->session->userdata("username");
       date_default_timezone_set("Asia/Kolkata");
       if (empty($username)) {
       $this->session->set_flashdata("error_login", "Invalid Request");
       redirect("login", "refresh");
       }
       $usertype= $this->session->userdata("usertype");
       if($usertype!=1)
       { $this->session->set_flashdata("error_login", "Invalid Request");
        redirect("login", "refresh");}
       $this->load->model('m_default');
       // ini_set('max_execution_time', 1200);
       // ini_set('memory_limit','4096M');
       foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
     }

public function roi_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='ROI' AND  a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_cashback';
  $this->data['title']='ROI Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

 public function direct_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Direct'  AND a.id=c.`child_id` AND  b.member_id=c.child_id";
  $this->data['spillincome']=$this->m_default->get_user_list($sql);
  $this->data['content']='listdirectincome';
  $this->data['title']='Direct Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

  public function index()
  {
    $this->data['content']='pay_out';
    $this->data['title']='Daily Closing | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function club()
  {
    $this->data['content']='club';
    $this->data['title']='Daily Closing | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

   public function level()
  {
    $this->data['content']='level_payout';
    $this->data['title']='Level Payout Closing | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }



public function generate_payout()
  {
    $this->data['content']='pay_out';
    $this->data['title']='Daily Closing | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }


public function cashback_summary()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level,c.direct_match, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Cash Back' AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_cashbacksummary';
  $this->data['title']='Cash Back Income | FIREXCOIN';
  $this->load->view('common/template',$this->data);
}

  public function instantbonus()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT a.* , b.username,b.email, c.amount , c.created_on AS bonusdate FROM tbl_member a, tbl_login b, tbl_bonus c WHERE a.parent_id=$member_id AND a.id=b.member_id AND c.bonus_type='Direct Bonus' ";
    $this->data['directmembers']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_instantbonus';
    $this->data['title']='Sponser Bonus | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function rank_match_income()
{
//   $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Rank Match' AND  a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_rank_match';
  $this->data['title']='Rank Match Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

  public function level_income()
{

  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level' AND  a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_levelincomeadmin';
  $this->data['title']='Level Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function matrix_level_income()
{
//   $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Matrix Level' AND  a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_matrixlevelincome';
  $this->data['title']='Level Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

  public function matchingincome()
  {
    $member_id=$this->session->userdata('member_id');
    /*$sql="SELECT c.*  FROM tbl_bonus c WHERE  c.bonus_type='MBV' AND c.member_id=$member_id";
    $this->data['directmembers']=$this->m_default->get_user_list($sql);*/
    $sql="SELECT c.*,b.username,b.email,a.name FROM tbl_login b,tbl_matching_amount c, tbl_member a WHERE b.member_id=c.member_id and a.id = b.member_id order by created_on desc";
    $this->data['matchingincome']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_matchingbonus1';
    $this->data['title']='Matching Income | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function team_royalty()
  {
    $sql="SELECT  a.* ,c.bv_amount ,b.username,b.email, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Team Royalty' AND  a.id=c.`member_id` AND  b.member_id=c.member_id";
    $this->data['teamroyalty']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_teamroyalty';
    $this->data['title']='Team Royalty Bonus | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function cashback_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Cash Back' AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_cashback';
  $this->data['title']='Cash Back Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


public function club_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Club'  AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_clubincome';
  $this->data['title']='Club Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}

public function sponser_income()
{
  $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Sponser'  AND a.id=c.`member_id` AND  b.member_id=c.member_id";
  $this->data['spillincome']=$this->db->query($sql)->result_array();
  $this->data['content']='list_sponserincome';
  $this->data['title']='Sponser Income | Ever Green Wallet';
  $this->load->view('common/template',$this->data);
}


  public function spill_income()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Spill' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
    $this->data['spillincome']=$this->db->query($sql)->result_array();
    $this->data['content']='list_spillincome';
    $this->data['title']='Spill Income | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function bv()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level Bonus' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
    $this->data['listbv']=$this->db->query($sql)->result_array();
    $this->data['content']='list_bv';
    $this->data['title']='BV | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }
  public function team_development()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.*, b.username,b.email  FROM tbl_development_bonus a,tbl_login b WHERE  a.member_id=b.member_id ";
    $this->data['teamdevelopment']=$this->db->query($sql)->result_array();
    $this->data['content']='list_teamdevelopment1';
    $this->data['title']='Team Development Income | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

  public function wallet()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,sum(amount) amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level Bonus'  AND a.id=c.`member_id` AND  b.member_id=c.member_id";
    $this->data['levelbonus']=$this->db->query($sql)->result_array();
    $this->data['content']='list_levelbonus';
    $this->data['title']='Level Bonus | Ever Green Wallet';
    $this->load->view('common/template',$this->data);
  }

public function total_members_down_level($userid,$count,$level,$levelconstant)
  {
     $sql="SELECT * FROM tbl_matrix where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    //$count+=count($records);
    $level++;

    if($level==$levelconstant)
    {
        $count+=count($records);
        return $count;

    }


    foreach($records as $row)
      {
        $count= $this->total_members_down_level($row['member_id'],$count,$level,$levelconstant);
      }
    //}
      return $count;
  }



  public function matrix()
  {
    $query = "SELECT id,matrix_rank FROM  tbl_member where id!=1 and member_status=1";
   $num_rows = $this->db->query($query)->result_array();
   $child_id=0;
   foreach ($num_rows as $key => $value) {
   $parent_id=$value['id'];
    $rank=$value['matrix_rank'];

   if($rank==0){
     $totalmember= $this->total_members_down_level($parent_id,0,0,1);
     if($totalmember==5){
       $query = "update tbl_member  set matrix_rank=1 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,50,0,'Matrix Level','Rank 1','');
     }
   }

   if($rank==1){
     $totalmember= $this->total_members_down_level($parent_id,0,0,2);
     if($totalmember==25){
       $query = "update tbl_member  set matrix_rank=2 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,250,0,'Matrix Level','Rank 2','');
     }
   }

   if($rank==2){
     $totalmember= $this->total_members_down_level($parent_id,0,0,3);
     if($totalmember==125){
       $query = "update tbl_member  set matrix_rank=3 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,1250,0,'Matrix Level','Rank 3','');
     }
   }

   if($rank==3){
     $totalmember= $this->total_members_down_level($parent_id,0,0,4);
     if($totalmember==625){
       $query = "update tbl_member  set matrix_rank=4 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,6250,0,'Matrix Level','Rank 4','');
     }
   }

   if($rank==4){
     $totalmember= $this->total_members_down_level($parent_id,0,0,5);
     if($totalmember==3125){
       $query = "update tbl_member  set matrix_rank=5 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,31250,0,'Matrix Level','Rank 5','');
     }
   }

   if($rank==5){
     $totalmember= $this->total_members_down_level($parent_id,0,0,6);
     if($totalmember==15625){
       $query = "update tbl_member  set matrix_rank=6 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,156250,0,'Matrix Level','Rank 6','');
     }
   }

   if($rank==6){
     $totalmember= $this->total_members_down_level($parent_id,0,0,7);
     if($totalmember==78125){
       $query = "update tbl_member  set matrix_rank=7 where id=".$value['id'];
       $this->db->query($query);
       $this->savedetails($parent_id,$child_id,781250,0,'Matrix Level','Rank 7','');
     }
   }

//   if($rank==7){
//      $totalmember= $this->total_members_down_level($parent_id,0,0,8);
//      if($totalmember==390625){
//       $query = "update tbl_member  set matrix_rank=8 where id=".$value['id'];
//       $this->db->query($query);
//       $this->savedetails($parent_id,$child_id,3906250,0,'Matrix Level','Rank 8');
//      }
//   }

//   if($rank==8){
//      $totalmember= $this->total_members_down_level($parent_id,0,0,9);
//      if($totalmember==1953125){
//       $query = "update tbl_member  set matrix_rank=9 where id=".$value['id'];
//       $this->db->query($query);
//       $this->savedetails($parent_id,$child_id,19531250,0,'Matrix Level','Rank 9');
//      }
//   }

//   if($rank==9){
//      $totalmember= $this->total_members_down_level($parent_id,0,0,10);
//      if($totalmember==9765625){
//       $query = "update tbl_member  set matrix_rank=10 where id=".$value['id'];
//       $this->db->query($query);
//       $this->savedetails($parent_id,$child_id,97656250,0,'Matrix Level','Rank 10');
//      }
//   }

   }
   $payout_date=$this->input->post('payoutdate');
   $payoutdate = array('payout_date' => date('Y-m-d',strtotime($payout_date)));
   $this->m_default->data_insert('tbl_payout',$payoutdate);

   $this->session->set_flashdata('success','Payout Generated Successfully..!');
   redirect('payout/generate_payout');
  }

  public function level_calculation()
  {

    $payout_date=$this->input->post('payoutdate');
    $query = "SELECT member_id as id, rank FROM  tbl_matrix";
    $num_rows = $this->db->query($query)->result_array();

    foreach ($num_rows as $key => $value) {
        $rank=$value['rank'];
        $upgradefee=0;
        if($value['rank']==0){

         $rank0total=$this->total_members_down_matrix($num_rows[$key]['id'],0);


         if($rank0total>=2 ){


        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=".$num_rows[$key]['id'])->row()->amount;
        $array=array('member_id'=>$num_rows[$key]['id'],'desc'=>'Auto Level Fee','debited'=>$upgradefee,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_wallet_report',$array);

            $query = "update tbl_matrix  set rank=1 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,600,0,'Level','Auto Income Level 1 Achieved','');
            $upgradefee=300;
         }

        }
        else if($value['rank']==1){

         $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,1);

         if($ranktotal>=5){
            $query = "update tbl_matrix  set rank=2 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,1600,0,'Level','Auto Income Level 2 Achieved','');
            $upgradefee=1000;
         }
        }


        else if($value['rank']==2){
         $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,2);

         if($ranktotal>=10){
            $query = "update tbl_matrix  set rank=3 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,5600,0,'Level','Auto Income Level 3 Achieved','');
             $upgradefee=2000;
         }
        }
        else if($value['rank']==3){
         $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,3);

         if($ranktotal>=20){
            $query = "update tbl_matrix  set rank=4 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,35200,0,'Level','Auto Income Level 4 Achieved','');
            $upgradefee=10000;
         }
        }
        else if($value['rank']==4){
         $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,4);

         if($ranktotal>=40){
            $query = "update tbl_matrix  set rank=5 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,416000,0,'Level','Auto Income Level 5 Achieved','');
            $upgradefee=160000;
         }
        }
        else if($value['rank']==5){
         $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,5);

         if($ranktotal>=80){
            $query = "update tbl_matrix  set rank=6 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,1152000,0,'Level','Auto Income Level 6 Achieved','');
            $upgradefee=6000000;
         }
        }
        else if($value['rank']==6){
          $ranktotal=$this->total_members_down_rank_matrix($num_rows[$key]['id'],0,6);

         if($ranktotal>=80){
            $query = "update tbl_matrix  set rank=7 where member_id=".$num_rows[$key]['id'];
            $this->db->query($query);
            $this->savedetails($num_rows[$key]['id'],0,384000000,0,'Level','Auto Income Level 7 Achieved','');
         }
        }

        if($upgradefee!=0){

        $parent_id= $num_rows[$key]['id'];
        $query="update tbl_wallet set amount=amount-$upgradefee where member_id=$parent_id";
        $this->m_default->execute_query($query);
        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$parent_id ")->row()->amount;
        $array=array('member_id'=>$parent_id,'desc'=>'Upgrade Fee','debited'=>$upgradefee,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_wallet_report',$array);
        }
}

   $payout_date=$this->input->post('payoutdate');
   $payoutdate = array('payout_date' => date('Y-m-d',strtotime($payout_date)));
   $this->m_default->data_insert('tbl_payout',$payoutdate);

   $this->session->set_flashdata('success','Payout Generated Successfully..!');
   redirect('payout/level');
   //$this->matrix();

  }


  public function club_income_payout(){
      $teamroyalty = array();
    $royaltycount=0;

       $query = "SELECT id, rank,package_id FROM  tbl_member where id!=1 and member_status=1";
    $num_rows = $this->db->query($query)->result_array();
    $this->db->trans_begin();
     foreach ($num_rows as $key => $value) {
      $parent_id=$num_rows[$key]['id'];
      /**********Club Member*********/
  $pcreated_on='';
  $pcreated_on=$this->db->query("select created_on from tbl_member where id=$parent_id")->row()->created_on;
  $pcreated_on=date('Y-m-d',strtotime($pcreated_on));
  $clubdirect=$this->db->query("select count(id) as count from tbl_member where  sponser_id=".$num_rows[$key]['id']." and member_status=1 and DATEDIFF('".$pcreated_on."',date_format(created_on, '%Y-%m-%d'))<=30")->row()->count;
  if($clubdirect>=60)
  {
     $teamroyalty[$royaltycount]['member_id']=$num_rows[$key]['id'];
     $royaltycount++;
  }
/***********Club Member************/

}


/***********************************************Royalty Bonus**********************************************/

$today=date('Y-m-d');


  if(count($teamroyalty)>0){

    $query = 'SELECT  sum(package_price) as clubprice FROM tbl_member where date_format(package_add_date, "%Y-%m-%d")="'.$today.'" and member_status=1';
    $incomecount=$this->db->query($query)->row()->clubprice;

   $incometoday=((($incomecount)*5)/100)/count($teamroyalty);

       if($incometoday<2000){
           $incometoday=2000;
       }

     foreach ($teamroyalty as $value) {

       $this->savedetails($value['member_id'],0,$incometoday,0,'Club','Club Income','');

   }
}

/***********************************************Royalty Bonus**********************************************/

$this->db->trans_commit();

redirect('payout/club');

  }

 public function roi()
  {
    $this->data['content']='pay_out_roi';
    $this->data['title']='Daily Closing | Earth Life Innovation';
    $this->load->view('common/template',$this->data);
  }
public function payout_roi(){
    
     $payout_date=$this->input->post('payoutdate');
     
    $count= $this->db->query('select count(id) as count from tbl_payout where date(created_on)="'.date('Y-m-d').'"')->row()->count;
  
  if($count>0){
      $this->session->set_flashdata('error','Already Paid..!');
  redirect('payout/roi');
  }
  else{
       
date_default_timezone_set('Asia/Kolkata');
    
$date = date("Y-m-d");
$timestamp = strtotime($date);
$weekday= date("l", $timestamp );
$normalized_weekday = strtolower($weekday);

if (($normalized_weekday == "saturday") || ($normalized_weekday == "sunday")) {
    echo "true";
} else {
    $query = "SELECT id,package_price FROM  tbl_member where id!=1 ";
    $num_rows = $this->db->query($query)->result_array();
    $pairs='';

    $this->db->trans_begin();
    foreach ($num_rows as $key => $value) {
      
      // $sponser=$this->db->query('select count(id) as count from tbl_member where sponser_id='.$value['id'])->row()->count;
      
       if($value['package_price']==1000 || $value['package_price']==2000 || $value['package_price']==3000 || $value['package_price']==4000){
          
          // if($sponser>=3){
              $roiincome=($value['package_price']*1)/100;
          // }
          // else
          // {
          //     $roiincome=33;
          // }
          $this->savedetails($value['id'],0,$roiincome,0,'ROI','ROI Income','');
      }

      else if($value['package_price']==5000 || $value['package_price']==6000 || $value['package_price']==7000 || $value['package_price']==8000 || $value['package_price']==9000 || $value['package_price']==10000){
          
          // if($sponser>=3){
              $roiincome=($value['package_price']*2)/100;
          // }
          // else
          // {
          //     $roiincome=33;
          // }

              $this->savedetails($value['id'],0,$roiincome,0,'ROI','ROI Income','');
          
      }
      
     
      
       
    }
    
   $this->db->trans_commit();
  
}

   }
   
$payoutdate = array('payout_date' => date('Y-m-d',strtotime($payout_date)));
   $this->m_default->data_insert('tbl_payout',$payoutdate);
   $this->session->set_flashdata('success','Payout Generated Successfully..!');
   redirect('payout/roi');
}

  public function generatematching()
  {
    $trids='';
    $trcount=0;
    $matching_price=150;
    $query = "SELECT id, rank,package_id,package_price FROM  tbl_member where id!=1 and member_status=1 and id != 793";


    $num_rows = $this->db->query($query)->result_array();
    $pairs='';
    $teamroyalty = array();
    $royaltycount=0;
    $this->db->trans_begin();
    

    foreach ($num_rows as $key => $value) {

     //  if($value['package_id']==1){
     //      $capping=1500;
     //  }
     // else if($value['package_id']==2){
     //      $capping=2500;
     //  }
     //  else if($value['package_id']==3){
     //      $capping=5000;
     //  }
     //  else if($value['package_id']==4){
     //      $capping=8000;
     //  }
     //  else if($value['package_id']==5){
     //      $capping=12000;
     //  }
     //  else if($value['package_id']==6){
     //      $capping=15000;
     //  }
     //  else if($value['package_id']==7){
     //      $capping=9999;
     //  }
     //  else if($value['package_id']==8){
     //      $capping=1000;
     //  }

      $capping=$value['package_price']*5;

      $totalleftpvmatching=0;
      $totalrightpvmatching=0;
      $totalrightpv=0;
      $totalleftpv=0;
      $newpairpv=0;
      $pairs='';
      $totalleftleg=0;
      $leftmemberids='';
      $totalrightleg=0;
      $rightmemberids='';
      $parent_id=$num_rows[$key]['id'];
      $count= $this->db->query("SELECT id,name FROM  tbl_member where tempo!=1 and member_status=1 AND parent_id=".$num_rows[$key]['id']." order by leg asc")->result_array();
      $imbv= $this->db->query("SELECT id FROM  tbl_bonus where bonus_type='MBV' and member_id=".$num_rows[$key]['id'])->result_array();
      $this->total_members_down($parent_id,0);
      $query = "SELECT id,name,pv,sponser_id,member_status FROM  tbl_member where leg =0 and parent_id=".$num_rows[$key]['id'];
      $leftid= $this->db->query($query)->result_array();
      if(count($leftid)>0)
      {
          if($leftid[0]['member_status']==1){
              $countleftstatus=1;
          }
          else{
               $countleftstatus=0;
          }
     //  $totalleftleg=$this->total_members_down($leftid[0]['id'],$countleftstatus);

        if($leftid[0]['sponser_id']==$num_rows[$key]['id']){
        $leftpv=$leftid[0]['pv'];
    }
    else{
        $leftpv=0;
    }
       $totalleftpv=$this->total_members_down_pv($leftid[0]['id'],$leftpv,$num_rows[$key]['id']);

       $leftpvmatching=$leftid[0]['pv'];
       $totalleftpvmatching=$this->total_members_down_pv_matching($leftid[0]['id'],$leftpvmatching);

      // $leftmemberids=$this->total_members_down_ids($leftid[0]['id'],$leftid[0]['id']);
      }


   $rightid= $this->db->query("SELECT id,name,pv,sponser_id,member_status FROM  tbl_member where leg =1 and parent_id=".$num_rows[$key]['id'])->result_array();


   if(count($rightid)>0)
   {
       if($rightid[0]['member_status']==1){
              $countrightstatus=1;
          }
          else{
               $countrightstatus=0;
          }
    //$totalrightleg= $this->total_members_down($rightid[0]['id'],$countrightstatus);
    //$rightmemberids=$this->total_members_down_ids($rightid[0]['id'],$rightid[0]['id']);

    if($rightid[0]['sponser_id']==$num_rows[$key]['id']){
        $rightpv=$rightid[0]['pv'];
    }
    else{
        $rightpv=0;
    }
    $totalrightpv=$this->total_members_down_pv($rightid[0]['id'],$rightpv,$num_rows[$key]['id']);

    $rightpvmatching=$rightid[0]['pv'];
       $totalrightpvmatching=$this->total_members_down_pv_matching($rightid[0]['id'],$rightpvmatching);
   }
  //  echo 'user_id :- '.$num_rows[$key]['id'];
  //  echo '<br/>';
  //  echo 'right_match : - ';
  //  print_r($totalrightpvmatching);
  //  echo '<br/>';
  //  echo 'left_match : -' ;
  //  print_r($totalleftpvmatching);
  //  echo '<br/>';
   




   /*Cash Back Income Pv */
   // if($totalleftpv<$totalrightpv){
   //     $newpairpv=$totalleftpv;
   // }
   // else if($totalrightpv<$totalleftpv){
   //     $newpairpv=$totalrightpv;
   // }
   // else {
   //     $newpairpv=$totalrightpv;
   // }

   // if(count($imbv)>0){

   // if($newpairpv==1)
   // {
   //     $this->savedetails($parent_id,0,40,$totalbv,'Cash Back','Cash Back Income',$newpairpv);

   // }
   // else if($newpairpv==2)
   // {
   //     $this->savedetails($parent_id,0,80,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   // else if($newpairpv==3)
   // {
   //     $this->savedetails($parent_id,0,120,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==4)
   // {
   //     $this->savedetails($parent_id,0,160,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==5)
   // {
   //     $this->savedetails($parent_id,0,200,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==6)
   // {
   //     $this->savedetails($parent_id,0,240,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==7)
   // {
   //     $this->savedetails($parent_id,0,280,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==8)
   // {
   //     $this->savedetails($parent_id,0,320,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv==9)
   // {
   //     $this->savedetails($parent_id,0,360,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv>=10 && $newpairpv<20 )
   // {
   //     $this->savedetails($parent_id,0,400,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv>=20 && $newpairpv<30)
   // {
   //     $this->savedetails($parent_id,0,800,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //  else if($newpairpv>=30  && $newpairpv<40)
   // {
   //     $this->savedetails($parent_id,0,1200,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //   else if($newpairpv>=40 && $newpairpv<50)
   // {
   //     $this->savedetails($parent_id,0,1600,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //   else if($newpairpv>=50 && $newpairpv<60)
   // {
   //     $this->savedetails($parent_id,0,2000,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //   else if($newpairpv>=60 && $newpairpv<70)
   // {
   //     $this->savedetails($parent_id,0,2400,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //   else if($newpairpv>=70 && $newpairpv<80)
   // {
   //     $this->savedetails($parent_id,0,2800,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   // else if($newpairpv>=80 && $newpairpv<90)
   // {
   //     $this->savedetails($parent_id,0,3200,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   //   else if($newpairpv>=90 && $newpairpv<100)
   // {
   //     $this->savedetails($parent_id,0,3600,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }
   // else if($newpairpv>=100)
   // {
   //     $this->savedetails($parent_id,0,4000,$totalbv,'Cash Back','Cash Back Income',$newpairpv);
   // }

   // }

    /*Cash Back Income Pv */




     if(1)
     {
      $leftdata= $this->db->query("SELECT left_used,right_used FROM  tbl_pairsdata where rank=500 and member_id= ".$num_rows[$key]['id'])->result_array();
      //print_r($leftdata);
      $newleft=$totalleftpvmatching-$leftdata[0]['left_used'];
      $newright=$totalrightpvmatching-$leftdata[0]['right_used'];
      // echo 'new left :- '. $newleft;
      // echo '<br/>';
      // echo 'new right :- '. $newright;
      // echo '<br/>';

      

      if($newleft!=0 && $newright!=0 && $newleft>0 && $newright>0)
       {
        if($newleft==$newright)
         {

      $newpairs=$newleft;
      $carryright=$newright-$newpairs;
      $carryleft=$newleft-$newpairs;
      $pairs=$spillleft_ids.$spillright_ids;
      // $totalamount=($matching_price*$newpairs);


      $totalamount=($newpairs*10)/100;

      if($totalamount>$capping){
         $totalamount= $capping;
      }

      $array=array('rank'=>$matching_rank,'matching_price'=>$matching_price,'member_id'=>$parent_id,'amount'=>$totalamount,'right_carry'=>$carryright,'left_carry'=>$carryleft,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1','pair_ids'=>$pairs,'payout_date'=>date('Y-m-d',strtotime($payout_date)));

      // echo '<br/>';
      // echo '----------------';
      // print_r($array);
      // echo '----------------';

      
      $this->m_default->data_insert('tbl_matching_amount',$array);
      
      $query = "update tbl_pairsdata set left_used = left_used+$newleft,right_used =right_used+$newleft,total_pairs=total_pairs+$newpairs  where rank=500 and member_id =".$num_rows[$key]['id'];
      $this->db->query($query);
    }
    else if($newright>$newleft)
    {
      $newpairs=$newleft;

      $pairs=$spillleft_ids.$spillright_ids;
      $totalamount=($newpairs*10)/100;
      $carryright=$newright-$newpairs;
      $carryleft=$newleft-$newpairs;
      if($totalamount>$capping){
         $totalamount= $capping;
      }
      $array=array('rank'=>$matching_rank,'matching_price'=>$matching_price,'member_id'=>$parent_id,'amount'=>$totalamount,'right_carry'=>$carryright,'left_carry'=>$carryleft,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1','pair_ids'=>$pairs,'payout_date'=>date('Y-m-d',strtotime($payout_date)));

      // echo '<br/>';
      // echo '----------------';
      // print_r($array);
      // echo '----------------';
      
      $this->m_default->data_insert('tbl_matching_amount',$array);
      
      $query = "update tbl_pairsdata set left_used = left_used+$newleft,right_used =right_used+$newpairs,total_pairs=total_pairs+$newpairs  where rank=500 and member_id =".$num_rows[$key]['id'];
      
      $this->db->query($query);
    }
    else if($newleft>$newright)
    {

      $newpairs=$newright;

      $carryleft=$newleft-$newpairs;
      $carryright=$newright-$newpairs;
      $pairs=$spillleft_ids.$spillright_ids;
      $totalamount=($newpairs*10)/100;
      if($totalamount>$capping){
         $totalamount= $capping;
      }
      $array=array('rank'=>$matching_rank,'matching_price'=>$matching_price,'member_id'=>$parent_id,'amount'=>$totalamount,'right_carry'=>$carryright,'left_carry'=>$carryleft,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1','pair_ids'=>$pairs,'payout_date'=>date('Y-m-d',strtotime($payout_date)));

      // echo '<br/>';
      // echo '----------------';
      // print_r($array);
      // echo '----------------';
      
      $this->m_default->data_insert('tbl_matching_amount',$array);

      $query = "update tbl_pairsdata set left_used = left_used+$newpairs,right_used =right_used+$newright,total_pairs=total_pairs+$newpairs  where rank=500 and member_id =".$num_rows[$key]['id'];
      
      $this->db->query($query);
    }

    $totalamount=($newpairs*10)/100;
    if($totalamount>$capping){
         $totalamount = $capping;
      }



    $this->savedetails($parent_id,0,$totalamount,$totalbv,'MBV','Matching Income','');

    /**Sponser Income***/
    // $sponseridmbv=$this->db->query("select sponser_id from tbl_member where id=$parent_id")->row()->sponser_id;

    // $legleftcheck=$this->db->query("select count(id) as count from tbl_member where sponser_id=$sponseridmbv and leg=0")->row()->count;
    // $legrightcheck=$this->db->query("select count(id) as count from tbl_member where sponser_id=$sponseridmbv and leg=1")->row()->count;

    // if($legleftcheck>0 && $legrightcheck>0){

    // $sponseramount=($totalamount*50)/100;
    // if($sponseramount>500)
    // {
    //     $sponseramount=500;
    // }
    // $this->savedetails($sponseridmbv,$parent_id,$sponseramount,$totalbv,'Sponser','Sponser Income','');
    // }

       }

       else {

        // echo 'Position Here';
        // echo '<br/>';

       }
     }

    //  echo '-----------------';
    //   echo '<br/>';
    //   echo '<hr>';
    //   echo '<br/>';

  }


      $this->db->trans_commit();

      $this->session->set_flashdata('success','Payout Generated Successfully..!');
   redirect('payout/generate_payout');
     // $this->level_calculation();
  }

  public function ins($userid,$count)
  {
      for($i=1;$i<=2000;$i++){
          $array=array('member_id'=>$i,'left_used'=>0,'right_used'=>0,'total_pairs'=>0);
          $this->m_default->data_insert('tbl_pairsdata',$array);
      }
  }

public function total_members_down_rank($userid,$count,$rank)
  {
    $sql="SELECT id,tempo,rank FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();


    foreach($records as $row)
      {
         if($row['rank']>=$rank){
              $count++;
         }

        $count= $this->total_members_down_rank($row['id'],$count,$rank);
      }

      return $count;
  }

  public function total_members_down_rank_matrix($userid,$count,$rank)
  {
    $sql="SELECT member_id,rank FROM tbl_matrix where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();


    foreach($records as $row)
      {
         if($row['rank']==$rank){
              $count++;
         }

        $count= $this->total_members_down_rank_matrix($row['member_id'],$count,$rank);
      }

      return $count;
  }

  public function total_members_down_rank_time($userid,$count,$rank)
  {

    $sql="SELECT id,tempo,rank FROM tbl_member where parent_id=$userid and rank_updated=1";
    $records=$this->db->query($sql)->result_array();


    foreach($records as $row)
      {
         if($row['rank']==$rank){
              $count++;
         }

        $count= $this->total_members_down_rank_time($row['id'],$count,$rank);
      }

      return $count;
  }

 public function total_members_down($userid,$count)
  {
    $sql="SELECT id,tempo,member_status FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    $count+=count($records);



    foreach($records as $row)
      {
         if($row['tempo']==1 || $row['member_status']==0){
             $count=$count-1;
         }

        $count= $this->total_members_down($row['id'],$count);
      }

      return $count;
  }

  public function total_members_down_matrix($userid,$count)
  {
    $sql="SELECT member_id FROM tbl_matrix where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    $count+=count($records);


    foreach($records as $row)
      {

        $count= $this->total_members_down_matrix($row['member_id'],$count);
      }

      return $count;
  }

   public function total_members_down_pv($userid,$pv,$sponser_id)
  {
    $sql="SELECT id,pv,sponser_id,member_status FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    foreach($records as $row)
      {
          if($row['sponser_id']==$sponser_id && $row['member_status']==1){
               $pv+=$row['pv'];
          }

        $pv= $this->total_members_down_pv($row['id'],$pv,$sponser_id);
      }

      return $pv;
  }

  public function total_members_down_pv_matching($userid,$pv)
  {
    $sql="SELECT id,pv,member_status FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();

    foreach($records as $row)
      {
          if($row['member_status']==1){
               $pv+=$row['pv'];
          }

        $pv= $this->total_members_down_pv_matching($row['id'],$pv);
      }

      return $pv;
  }

  public function savedetails($parent_id,$member_id,$amount,$bv_points,$bonustype,$level,$pvmatch)

  {

    $array=array('member_id'=>$parent_id,'child_id'=>$member_id,'amount'=>$amount == NULL ? 0 : $amount,'bv_amount'=>$bv_points,'bonus_type'=>$bonustype,'level'=>$level,'direct_match'=>$pvmatch == '' ? 0 : $pvmatch);
    $this->m_default->data_insert('tbl_bonus',$array);

    $query="update tbl_wallet set amount=amount+$amount where member_id=$parent_id";
    $this->m_default->execute_query($query);

    $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$parent_id ")->row()->amount;
   $array=array('member_id'=>$parent_id,'desc'=>$level,'credited'=>$amount,'balance'=>$debitedamount,'status'=>1);
   $this->m_default->data_insert('tbl_wallet_report',$array);
  }
  public function total_members_down_ids($userid,$ids)
  {
    $sql="SELECT id,tempo FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $count=count($records);

    if($count>0)
    {
    foreach($records as $row)
      {
        if($row['tempo']!=1){
          $ids.=','.$row['id'];
        }

        $ids= $this->total_members_down_ids($row['id'],$ids);
      }
    }
      return $ids;
  }


  public function cron_matrix(){

    $query = "SELECT id, rank,package_id FROM  tbl_member where id!=1 ";
    $num_rows = $this->db->query($query)->result_array();
    $pairs='';
    $teamroyalty = array();
    $royaltycount=0;
    $this->db->trans_begin();
    foreach ($num_rows as $key => $value) {

    $matrixcount=  $this->db->query("select count(id) as count from tbl_matrix where member_id=".$value['id'])->row()->count;

    if($matrixcount==0 || $matrixcount=="")
    {

    $wallamount=$this->db->query("select amount from tbl_wallet where member_id=".$num_rows[$key]['id'])->row()->amount;
    if($wallamount>=400){
        $query="update tbl_wallet set amount=amount-400 where member_id=".$num_rows[$key]['id'];
        $this->m_default->execute_query($query);
    }

    $records=array();
  $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
  $records=$this->db->query($sql)->result_array();

  if(count($records)<2)
  {
   $matrixparent_id= 2;
  }
  else{
    $matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
  }

  $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
  $this->m_default->data_insert('tbl_matrix',$matrixdata);
    }

    }
     $this->db->trans_commit();
  }


  public function recursive_leg_matrix($arraystr)
    {
              $str='';
              $records=explode(',',$arraystr);
        foreach($records as $row){
          if($row!=""){
          $sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$row;
          $recordsnew1=$this->db->query($sql)->result_array();

          if(count($recordsnew1)<2){

              return $row;
          }
          else{
            $str.=$recordsnew1[0]['member_id'].','.$recordsnew1[1]['member_id'].',';
              }
                     }
          }

        return   $this->recursive_leg_matrix($str);
        }

}
