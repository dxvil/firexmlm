<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payout extends CI_Controller {

	/**
	 * Package Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct()
  {
       parent::__construct();
       $username= $this->session->userdata("username");
       if (empty($username)) {
       $this->session->set_flashdata("error_login", "Invalid Request");
       redirect("login", "refresh");
       }
       $usertype= $this->session->userdata("usertype");
       if($usertype!=1)
       { $this->session->set_flashdata("error_login", "Invalid Request");
        redirect("login", "refresh");}
       $this->load->model('m_default');
       foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
  }

  public function index()
  {
    $this->data['content']='pay_out';
    $this->data['title']='Daily Closing | Pluto';
    $this->load->view('common/template',$this->data);
  }


public function generate_payout()
  {
    $this->data['content']='pay_out';
    $this->data['title']='Daily Closing | Pluto';
    $this->load->view('common/template',$this->data);
  }


  public function instantbonus()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT a.* , b.username,b.email, c.amount , c.created_on AS bonusdate FROM tbl_member a, tbl_login b, tbl_bonus c WHERE a.parent_id=$member_id AND a.id=b.member_id AND c.bonus_type='Direct Bonus' ";
    $this->data['directmembers']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_instantbonus';
    $this->data['title']='Sponser Bonus | Pluto';
    $this->load->view('common/template',$this->data);
  }


  public function matchingincome()
  {
    $member_id=$this->session->userdata('member_id');
    /*$sql="SELECT c.*  FROM tbl_bonus c WHERE  c.bonus_type='MBV' AND c.member_id=$member_id";
    $this->data['directmembers']=$this->m_default->get_user_list($sql);*/
    $sql="SELECT c.*,b.username,b.email   FROM tbl_login b,tbl_matching_amount c WHERE b.member_id=c.member_id";
    $this->data['matchingincome']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_matchingbonus1';
    $this->data['title']='Matching Income | Pluto';
    $this->load->view('common/template',$this->data);
  }

  public function team_royalty()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,c.amount ,b.username,b.email, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Team Royalty' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
    $this->data['teamroyalty']=$this->m_default->get_user_list($sql);
    $this->data['content']='list_teamroyalty';
    $this->data['title']='Team Royalty Bonus | Pluto';
    $this->load->view('common/template',$this->data);
  }

  public function spill_income()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Spill' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
    $this->data['spillincome']=$this->db->query($sql)->result_array();
    $this->data['content']='list_spillincome';
    $this->data['title']='Spill Income | Pluto';
    $this->load->view('common/template',$this->data);
  }

  public function bv()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,c.amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level Bonus' AND c.member_id=$member_id AND a.id=c.`child_id` AND  b.member_id=c.child_id";
    $this->data['listbv']=$this->db->query($sql)->result_array();
    $this->data['content']='list_bv';
    $this->data['title']='BV | Pluto';
    $this->load->view('common/template',$this->data);
  }
  public function team_development()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.*, b.username,b.email  FROM tbl_development_bonus a,tbl_login b WHERE  a.member_id=b.member_id ";
    $this->data['teamdevelopment']=$this->db->query($sql)->result_array();
    $this->data['content']='list_teamdevelopment1';
    $this->data['title']='Team Development Income | Pluto';
    $this->load->view('common/template',$this->data);
  }

  public function wallet()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,sum(amount) amount ,b.username,b.email,c.level, c.created_on AS bonusdate FROM tbl_bonus c, tbl_member a,tbl_login b WHERE c.bonus_type='Level Bonus'  AND a.id=c.`member_id` AND  b.member_id=c.member_id";
    $this->data['levelbonus']=$this->db->query($sql)->result_array();
    $this->data['content']='list_levelbonus';
    $this->data['title']='Level Bonus | Pluto';
    $this->load->view('common/template',$this->data);
  }

  public function generatematching()
  {
    $payout_date=$this->input->post('payoutdate');
    $payoutdate = array('payout_date' => date('Y-m-d',strtotime($payout_date)));

    $this->m_default->data_insert('tbl_payout',$payoutdate);

    $query = "SELECT id FROM  tbl_member where id!=1";
    $num_rows = $this->db->query($query)->result_array();
    $pairs='';
    foreach ($num_rows as $key => $value) {
      $parent_id=$num_rows[$key]['id'];

      $query = "SELECT id,name FROM  tbl_member where parent_id=".$num_rows[$key]['id']." order by leg asc";
      $count= $this->db->query($query)->result_array();

      $query = "SELECT id FROM  tbl_bonus where bonus_type='MBV' and member_id=".$num_rows[$key]['id'];
      $imbv= $this->db->query($query)->result_array();

      $countpairs=0;
   //echo 'Total Down';
    $this->total_members_down($parent_id,$countpairs);
   //left
   $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$num_rows[$key]['id'];
   $leftid= $this->db->query($query)->result_array();
   if(count($leftid)>0)
   {
    $countpairs=1;
    //echo 'Left Leg';
     $totalleftleg=$this->total_members_down($leftid[0]['id'],$countpairs);
     $leftmemberids=$this->total_members_down_ids($leftid[0]['id'],$leftid[0]['id']);
   }


  /*team develop*/

  $leftmembersidsarray=explode(',',$leftmemberids);
  $teamleft_ids= $this->db->query("SELECT left_ids FROM  tbl_team_development where member_id=".$num_rows[$key]['id'])->row()->left_ids;

  $teamleft_idsarray=explode(',',$teamleft_ids);
  $leftcount=0;
  foreach($leftmembersidsarray as $row)
  {
      $flag=0;
      foreach ($teamleft_idsarray as $row1) {
          if($row==$row1)
          {
              $flag=1;
          }
      }
      if($flag==0)
      {
        $leftcount++;
        $teamleft_ids.=$row.',';
      }

  }


   //right
   $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$num_rows[$key]['id'];
   $rightid= $this->db->query($query)->result_array();

   if(count($rightid)>0)
   {
    $countpairs=1;
    //echo 'Right Leg';
    $totalrightleg= $this->total_members_down($rightid[0]['id'],$countpairs);
    $rightmemberids=$this->total_members_down_ids($rightid[0]['id'],$rightid[0]['id']);
   }



   /*team develop*/


   $rightmembersidsarray=explode(',',$rightmemberids);
   $teamright_ids= $this->db->query("SELECT right_ids FROM  tbl_team_development where member_id=".$num_rows[$key]['id'])->row()->right_ids;

   $teamright_idsarray=explode(',',$teamright_ids);
   $rightcount=0;
   foreach($rightmembersidsarray as $row)
   {
       $flag=0;
       foreach ($teamright_idsarray as $row1) {
           if($row==$row1)
           {
               $flag=1;
           }
       }
       if($flag==0)
       {
         $rightcount++;
         $teamright_ids.=$row.',';
       }

   }

   if(($rightcount+$leftcount)>=75)
   {
     if($rightcount>=5 && $leftcount>=5)
     {
      $right15per=  (($rightcount*2000)*15)/100;
      $left15per=  (($leftcount*2000)*15)/100;
      $totaltdb=$right15per+$left15per;
      $this->savedetails($num_rows[$key]['id'],0,0,$totaltdb,'Team Development','N/A');

      $this->db->query("update  tbl_team_development set left_ids='$teamleft_ids',right_ids='$teamright_ids'  where member_id=".$num_rows[$key]['id']);

      $array=array('member_id'=>$parent_id,'left_count'=>$leftcount,'right_count'=>$rightcount,'left_percentage'=>$left15per,'right_percentage'=>$right15per,'total_bv'=>$totaltdb);
      $this->m_default->data_insert('tbl_development_bonus',$array);

    /*echo $teamleft_ids;
      echo $leftcount;
      echo '<br>';
      echo $teamright_ids;
      echo $rightcount;*/

     }

  /*team royalty*/
     if($rightcount>=10 && $leftcount>=10)
     {
     /* $right15per=  (($rightcount*2000)*15)/100;
      $left15per=  (($leftcount*2000)*15)/100;
      $totaltdb=$right15per+$left15per;
      $this->savedetails($num_rows[$key]['id'],0,0,$totaltdb,'Team Development','N/A');

      $this->db->query("update  tbl_team_development set left_ids='$teamleft_ids',right_ids='$teamright_ids'  where member_id=".$num_rows[$key]['id']);

      $array=array('member_id'=>$parent_id,'left_count'=>$leftcount,'right_count'=>$rightcount,'left_percentage'=>$left15per,'right_percentage'=>$right15per,'total_bv'=>$totaltdb);
      //$this->m_default->data_insert('tbl_development_bonus',$array);*/

    /*echo $teamleft_ids;
      echo $leftcount;
      echo '<br>';
      echo $teamright_ids;
      echo $rightcount;*/

     }
   }

     if(count($imbv)>0)
     {
   $query = "SELECT left_used,right_used FROM  tbl_pairsdata where member_id= ".$num_rows[$key]['id'];
   $leftdata= $this->db->query($query)->result_array();
  $newleft=$totalleftleg-$leftdata[0]['left_used'];
  $newright=$totalrightleg-$leftdata[0]['right_used'];
  if($newleft!=0 && $newright!=0)
  {
    if($newleft==$newright)
    {
      $newpairs=$newleft;
      $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>0,'left_carry'=>0,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
      $this->m_default->data_insert('tbl_matching_amount',$array);
    }
    else if($newright>$newleft)
    {
      $newpairs=$newleft;
      $carryright=$newright-$newleft;
      $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>$carryright,'left_carry'=>0,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
      $this->m_default->data_insert('tbl_matching_amount',$array);
    }
    else if($newleft>$newright)
    {
      $newpairs=$newright;
      $carryleft=$newleft-$newright;
      $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>0,'left_carry'=>$carryleft,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
      $this->m_default->data_insert('tbl_matching_amount',$array);
    }
    $query = "update tbl_pairsdata set left_used = left_used+$newpairs,right_used =right_used+$newpairs,total_pairs=total_pairs+$newpairs  where member_id =".$num_rows[$key]['id'];
    $this->db->query($query);
  for($i=0;$i<$newpairs;$i++){
    $this->savedetails($parent_id,0,400,4000,'MBV','N/A');
  }
  }
     }
     else{
   if(count($count)==2)
      {
        $pairs=$count[0]['id'].','.$count[1]['id'];
        $query = "SELECT id,name FROM  tbl_member where parent_id=".$count[0]['id'];
        $child1= $this->db->query($query)->result_array();

        $query = "SELECT id,name FROM  tbl_member where parent_id=".$count[1]['id'];
        $child2= $this->db->query($query)->result_array();

        if(count($child1)>0)
        {
        $pairs.=','.$child1[0]['id'];
        $this->savedetails($parent_id,0,400,6000,'MBV','N/A');
        $array=array('member_id'=>$parent_id,'pair_ids'=>$pairs,'amount'=>400,'right_carry'=>($totalrightleg-1),'left_carry'=>($totalleftleg-2),'left'=>$totalleftleg,'right'=>$totalrightleg,'pairs'=>1,'mbv_type'=>'2:1');
        $this->m_default->data_insert('tbl_matching_amount',$array);

        $array=array('member_id'=>$parent_id,'left_used'=>2,'right_used'=>1,'total_pairs'=>1);
        $this->m_default->data_insert('tbl_pairsdata',$array);
        }
        else if(count($child2)>0)
        {
          $pairs.=','.$child2[0]['id'];
          $this->savedetails($parent_id,0,400,4000,'MBV','N/A');
          $array=array('member_id'=>$parent_id,'pair_ids'=>$pairs,'amount'=>400,'right_carry'=>($totalrightleg-2),'left_carry'=>($totalleftleg-1),'left'=>$totalleftleg,'right'=>$totalrightleg,'pairs'=>1,'mbv_type'=>'1:2');
          $this->m_default->data_insert('tbl_matching_amount',$array);

          $array=array('member_id'=>$parent_id,'left_used'=>1,'right_used'=>2,'total_pairs'=>1);
          $this->m_default->data_insert('tbl_pairsdata',$array);
        }
        else {
          continue;
        }
      }
      else {
      continue;
      }
  $pairs='';}
  }
     $this->session->set_flashdata('success','Payout Generated Successfully..!');
     redirect('payout/generate_payout');
  }



  public function total_members_down($userid,$count)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $count+=count($records);
    if($count>0)
    {
    foreach($records as $row)
      {
        $count= $this->total_members_down($row['id'],$count);
      }
    }
      return $count;
  }

  public function savedetails($parent_id,$member_id,$amount,$bv_points,$bonustype,$level)
  {
    $array=array('member_id'=>$parent_id,'child_id'=>$member_id,'amount'=>$amount,'bv_amount'=>$bv_points,'bonus_type'=>$bonustype,'level'=>$level);
    $this->m_default->data_insert('tbl_bonus',$array);

    $query="update tbl_wallet set amount=amount+$amount where member_id=$parent_id";
    $this->m_default->execute_query($query);
  }
  public function total_members_down_ids($userid,$ids)
  {
    $sql="SELECT * FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $count=count($records);

    if($count>0)
    {
    foreach($records as $row)
      {
        $ids.=','.$row['id'];
        $ids= $this->total_members_down_ids($row['id'],$ids);
      }
    }
      return $ids;
  }


}
