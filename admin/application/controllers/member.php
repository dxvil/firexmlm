<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

  
  public function __construct()
  {
       parent::__construct();
          $username= $this->session->userdata("username");
       if (empty($username)) {
    $this->session->set_flashdata("error_login", "Invalid Request");
    redirect("login", "refresh");
   }
       $this->load->model('m_default');
        $sql="select coin_price from tbl_coinprice where id=1";
      $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
      foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
  }

  public function index()
  {
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,b.username,b.email, b.status as loginstatus FROM  tbl_member a,tbl_login b WHERE a.id=b.member_id and b.usertype=2";
    $this->data['listmember']=$this->db->query($sql)->result_array();
      foreach($this->data['listmember'] as $key=>$value){
      $this->data['listmember'][$key]['Sponserusername']=$this->db->query('select username from tbl_login where member_id='.$this->data['listmember'][$key]['sponser_id'])->row()->username;
      if ($this->data['listmember'][$key]['recharge_id'] != NULL){
        $x = $this->data['listmember'][$key]['recharge_id'];
        $this->data['listmember'][$key]['recharge_id']=$this->db->query("select b.name from tbl_login as a left join tbl_member as b on a.member_id = b.id where a.username='$x'")->row()->name . ' &nbsp; ('. $this->data['listmember'][$key]['recharge_id'].')';
      }
    }
    $this->data['content']='member/list_member';
    $this->data['title']='List Member | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }


  public function activate() {
    $id = $this->input->post('id');
    $where = array('member_id' => $id);
    $data = array('status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_login');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Member Blocked Successfully..!');
      redirect('member');
    }
  }

  public function deactivate() {
    $id = $this->input->post('id');
    $where = array('member_id' => $id);
    $data = array('status' => 0);
    $flag = $this->m_default->update($where, $data, 'tbl_login');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Member Unblocked Successfully..!');
      redirect('member');
    }
  }


public function savedetails($parent_id,$member_id,$levelbonus,$bonustype,$level)
 {
   $array=array('member_id'=>$parent_id,'child_id'=>$member_id,'amount'=>$levelbonus,'bonus_type'=>$bonustype,'level'=>$level);
   $this->m_default->data_insert('tbl_bonus',$array);
   $query="update tbl_wallet set amount=amount+$levelbonus where member_id=$parent_id";
   $this->m_default->execute_query($query);
 }

public function greenmember() {
    $id = $this->input->post('id');

$bonustype='Level Bonus';
     $price=110;
     $member_id=$id;
     $child_id=$member_id;


     $query = "SELECT parent_id, presenter_id, member_status FROM  tbl_member  WHERE id=$member_id";
     $num_rows = $this->m_default->get_single_row($query);

     if($num_rows[0]['member_status']==1)
     {
     $this->session->set_flashdata('danger','Already a Green Member..!');
     redirect('member');

     }
     else
     {

   $query="update tbl_member set member_status=1 where id=$member_id";
   $this->m_default->execute_query($query);
     $parent_id=$num_rows[0]['parent_id'];
     $presenter_id=$num_rows[0]['presenter_id'];
     $directbonus=($price*25)/100;
     $this->savedetails($parent_id,$child_id,$directbonus,'Direct Bonus','N/A');
     $presenterbonus=($price*15)/100;
     $this->savedetails($presenter_id,$child_id,$presenterbonus,'Presenter Bonus','N/A');
     for($i=1;$i<=10;$i++)
      {
        $query = "SELECT parent_id, presenter_id FROM  tbl_member  WHERE id=$member_id";
        $num_rows = $this->m_default->get_single_row($query);
         $parent_id=$num_rows[0]['parent_id'];
        if(!empty($num_rows[0]['parent_id']))
          {
          if($i==1)
          {
            $levelbonus=($price*8)/100;
               $level='Level 1';
          }
          else if($i==2)
          {
            $levelbonus=($price*8)/100;
               $level='Level 2';
          }
          else if($i==3)
          {
              $levelbonus=($price*6)/100;
               $level='Level 3';
          }
          else if($i==4)
          {
              $levelbonus=($price*6)/100;
               $level='Level 4';
          }
          else if($i==5)
          {
              $levelbonus=($price*5)/100;
               $level='Level 5';
          }
          else if($i==6)
          {
                $levelbonus=($price*5)/100;
                 $level='Level 6';
          }
          else if($i==7)
          {
                 $levelbonus=($price*4)/100;
                    $level='Level 7';
          }
          else if($i==8)
          {
                 $levelbonus=($price*4)/100;
                    $level='Level 8';
          }
          else if($i==9)
          {
                 $levelbonus=($price*2)/100;
                    $level='Level 9';
          }
          else if($i==10)
          {
                 $levelbonus=($price*2)/100;
                    $level='Level 10';
          }

          $this->savedetails($parent_id,$child_id,$levelbonus,$bonustype,$level);
          $member_id=$parent_id;
        }
      }
      $this->session->set_flashdata('success','Activated Successfully');
      redirect('member');
      }
  }



  public function edit_member()
{
$id = $this->input->post('id');
$sql="SELECT  a.* ,b.username,b.email FROM  tbl_member a,tbl_login b WHERE a.id=b.member_id and a.id=$id";
$this->data['memberdetails']=$this->db->query($sql)->result_array();
$this->data['id']=$id;
$this->data['content']='member/edit_member';
$this->data['title']='Edit Member | FIREXCOIN';
$this->load->view('common/template',$this->data);
}


public function change_password()
{
$id = $this->input->post('id');
$sql="SELECT  a.* ,b.username,b.password FROM  tbl_member a,tbl_login b WHERE a.id=b.member_id and a.id=$id";
$this->data['memberdetails']=$this->db->query($sql)->result_array();
$this->data['id']=$id;
$this->data['content']='member/edit_password';
$this->data['title']='Change Password | FIREXCOIN';
$this->load->view('common/template',$this->data);
}

public function view_member()
{
$id = $this->input->post('id');
$sql="SELECT  a.* ,b.username,b.email FROM  tbl_member a,tbl_login b WHERE a.id=b.member_id and a.id=$id";
$this->data['memberdetails']=$this->db->query($sql)->result_array();
$this->data['content']='member/view_member';
$this->data['title']='List Member | FIREXCOIN';
$this->load->view('common/template',$this->data);
}


public function update()
{
  $id = $this->input->post('id');
  $where = array('id' => $id);

  $name=$this->input->post('name');
  $contact_no=$this->input->post('contact_no');
  $email=$this->input->post('email');
  $country=$this->input->post('country');
  $bitcoin_address=$this->input->post('bitcoin_address');

  $data = array('name' => $name,'contact_number'=>$contact_no,'country'=>$country,'bitcoin_address'=>$bitcoin_address);
  $flag=$this->m_default->update($where, $data, 'tbl_member');

  $where = array('member_id' => $id);
  $data = array('email' => $email);
  $flag=$this->m_default->update($where, $data, 'tbl_login');

  if($flag == true) {
    $this->session->set_flashdata('success', 'Member Deactivated Successfully..!');
    redirect('member');
  }
}

public function update_password()
{
  $id = $this->input->post('id');
  $new_password=$this->input->post('new_password');
  $where = array('member_id' => $id);
  $data = array('password' => md5($new_password));
  $flag=$this->m_default->update($where, $data, 'tbl_login');
  if($flag == true) {
    $this->session->set_flashdata('success', 'Password Updated Successfully..!');
    redirect('member');
  }
}

}
