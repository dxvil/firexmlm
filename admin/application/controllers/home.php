<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

  /**
   * Home Controller
   * Author: Dipanwita Chanda
   **/

  public function __construct(){
    parent::__construct();
    $username= $this->session->userdata("username");
    $userType=$this->session->userdata('usertype');
    if (empty($username)) {
      $this->session->set_flashdata("error_login", "Invalid Request");
      redirect("login", "refresh");
    }
    $this->load->model('m_default');
    $this->load->model('includes/mail');
    $sql="select coin_price from tbl_coinprice where id=1";
    $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;

    foreach($this->input->post() as $items){
      if ($items != ''){
          if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
            $this->session->set_flashdata('error_login','Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
            break;
          }
      }
    }
     
  }
  
  public function coin_price()
  {
    $sql="select coin_price from tbl_coinprice where id=1";
    $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
    $this->data['content']='coin_price';
    $this->data['title']='Announcement | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }

  public function dollar_price()
  {
    $sql="select price from tbl_dollarprice where id=1";
    $this->data['wave_dollar_price'] = $this->db->query($sql)->row()->price;
    $this->data['content']='dollar_price';
    $this->data['title']='Update dollar price | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }

  public function super_package()
  {
    $this->data['content']='super_package';
    $sql="SELECT  member_id,username FROM tbl_login WHERE usertype=2";
    $packages = array(array("package_id" => "20", "package_name" => "100"), array("package_id" => "21", "package_name" => "500"),
    array("package_id" => "22", "package_name" => "1000"), array("package_id" => "23", "package_name" => "2000"),
    array("package_id" => "24", "package_name" => "5000"), array("package_id" => "25", "package_name" => "10000")
    );
    $this->data['packages']=$packages;
    $this->data['members']=$this->db->query($sql)->result_array();
    $this->data['title']='Super Packages | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }
  
   public function update_coin()
  {
     $coin_price= $this->input->post('coin_price');
     $sql="update tbl_coinprice set coin_price=$coin_price  where id=1";
      $this->db->query($sql);
    
    redirect('home/coin_price');
  }

  public function update_dollar()
  {
     $coin_price= $this->input->post('coin_price');
     $sql="update tbl_dollarprice set price=$coin_price  where id=1";
      $this->db->query($sql);
    
    redirect('home/dollar_price');
  }
  

  public function jata1(){
    $totalleftpv=0;$totalrightpv=0;
    $sql="SELECT id FROM tbl_member where id between 2 and 1000";
    $records=$this->db->query($sql)->result_array();
    foreach($records as $row){
      $totalleftpv=0;$totalrightpv=0;
      $member_id=$row['id'];
      $query = "SELECT id,name,pv FROM  tbl_member where leg =0 and parent_id=".$member_id;
      $leftid= $this->db->query($query)->result_array();
      if(count($leftid)>0){
        $totalleftpv=$this->total_members_down($leftid[0]['id'],1);
      }
      $query = "SELECT id,name,pv FROM  tbl_member where leg =1 and parent_id=".$member_id;
      $rightid= $this->db->query($query)->result_array();
      if(count($rightid)>0){
        $totalrightpv=$this->total_members_down($rightid[0]['id'],1);
      }
      $this->db->query("update tbl_member set leftcount=$totalleftpv, rightcount=$totalrightpv where id=$member_id");
    }
  }

  public function jata(){
    $totalleftpv=0;$totalrightpv=0;
    $sql="SELECT id FROM tbl_member where id!=1 limit 0, 1000";
    $records=$this->db->query($sql)->result_array();
    foreach($records as $row){
      $member_id=$row['id'];
      $query = "SELECT id,name,pv FROM  tbl_member where leg =0 and parent_id=".$member_id;
      $leftid= $this->db->query($query)->result_array();
      if(count($leftid)>0){
        $totalleftpv=$this->total_members_down_pv($leftid[0]['id'],$leftid[0]['pv'],$member_id);
      }
    $query = "SELECT id,name,pv FROM  tbl_member where leg =1 and parent_id=".$member_id;
    $rightid= $this->db->query($query)->result_array();
    if(count($rightid)>0){
      $totalrightpv=$this->total_members_down_pv($rightid[0]['id'],$rightid[0]['pv'],$member_id);
    }
    $this->db->query("update tbl_member set totalleftpv=$totalleftpv, totalrightpv=$totalrightpv where id=$member_id");
    }
  }

  public function total_members_down($userid,$count){
    $sql="SELECT id FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $count+=count($records);

    foreach($records as $row){
      $count= $this->total_members_down($row['id'],$count);
    }
    return $count;
  }

  public function list_news(){

  $query = "SELECT * FROM tbl_news where status=0 order by id desc limit 10 ";
    $this->data['list_news']= $this->db->query($query)->result_array();
    $this->data['content']='list_news';
    $this->data['title']='Announcement | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }


  public function welcome_letter(){
    $member_id=$this->session->userdata('member_id');
    $query = "SELECT package_price FROM  tbl_member where id=".$member_id;
      $this->data['package_price']= $this->db->query($query)->row()->package_price;
    $this->data['list_member']=$this->db->query('select * from tbl_member where id='.$member_id)->result_array();
    $this->data['content']='welcome_letter';
    $this->data['title']='Home | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }

  public function total_members_down_bv($userid,$count){
    $sql="SELECT id FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();
    $count+=count($records);
    foreach($records as $row){
      $count= $this->total_members_down_bv($row['id'],$count);
    }
    return $count;
  }

  public function total_members_down_pv($userid,$pv,$sponser_id){
    $sql="SELECT id,pv,sponser_id FROM tbl_member where parent_id=$userid";
    $records=$this->db->query($sql)->result_array();
    foreach($records as $row){
      $pv+=$row['pv'];
      $pv= $this->total_members_down_pv($row['id'],$pv,$sponser_id);
    }
    return $pv;
  }

  public function index(){
    $userType=$this->session->userdata('usertype');
    if($userType==1){
      $graph_data = $this->admingraph_view();
      $strmonth='';
      $strtotal='';
      $graphrecord=array();
      for($i=1;$i<=12;$i++){
        $flag=0;
        foreach($graph_data as  $key=>$value){
          if($i==$value['month']){
            $graphrecord[$i]['month']=date("F", mktime(0, 0, 0, $value['month'], 10));
            $graphrecord[$i]['total']=$value['grand_total'];
            $flag=1;
          }
        }
        if($flag==0){
          $graphrecord[$i]['month']=date("F", mktime(0, 0, 0, $i, 10));
          $graphrecord[$i]['total']=0;
        }
      }
      $this->data['graphrecord']=$graphrecord;
      $sql="select sum(amount) as totalbonus from tbl_bonus ";
      $this->data['totalbonus'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as matrix from tbl_bonus where  bonus_type='Matrix Level'";
      $this->data['matrix'] = $this->m_default->get_single_row($sql);
      $sql="select sum(bv_amount) as repurchase from tbl_bonus where  bonus_type='Repurchase'";
      $this->data['repurchase'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as mbv from tbl_bonus where bonus_type='MBV'";
      $this->data['mbv'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as level from tbl_bonus where  bonus_type='Level'";
      $this->data['level'] = $this->m_default->get_single_row($sql);
      $sql="select sum(bv_amount) as royalty from tbl_bonus where  bonus_type='Royalty'";
      $this->data['royalty'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as direct from tbl_bonus where  bonus_type='Direct'";
      $this->data['direct'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as roi from tbl_bonus where bonus_type='ROI'";
      $this->data['roi'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as sponser from tbl_bonus where  bonus_type='Sponser'";
      $this->data['sponser'] = $this->m_default->get_single_row($sql);
      $sql="select sum(bv_amount) as bv from tbl_bonus ";
      $this->data['bv'] = $this->m_default->get_single_row($sql);

      $month=date('m', strtotime(date('Y-m')." -1 month"));
      $sql="select sum(amount) as pmonthly from tbl_bonus where month(created_on)=$month";
      $this->data['pmonthly'] = $this->m_default->get_single_row($sql);
      
      $sql="select sum(coins) as r_coins from tbl_release_wallet";
      $this->data['r_coins'] = $this->m_default->get_single_row($sql);
      
      $sql="select sum(coins) as coins from tbl_coin";
      $this->data['coins'] = $this->m_default->get_single_row($sql);

      $month=date('m');
      $sql="select sum(amount) as monthly from tbl_bonus where month(created_on)=$month";
      $this->data['monthly'] = $this->m_default->get_single_row($sql);

      $query = "SELECT * FROM tbl_member order by id desc limit 10 ";
      $this->data['recentmem']= $this->db->query($query)->result_array();

      $sql="select sum(amount) as totalfundwallet from tbl_fundwallet";
      $this->data['totalfundwallet'] = $this->m_default->get_single_row($sql);

      $sql="select count(id) as totalmember from tbl_member";
      $this->data['totalmember'] = $this->m_default->get_single_row($sql);
      
      $sql="select count(id) as atotalmember from tbl_member where member_status=1";
      $this->data['atotalmember'] = $this->m_default->get_single_row($sql);
      
      $sql="select count(id) as itotalmember from tbl_member where member_status=0";
      $this->data['itotalmember'] = $this->m_default->get_single_row($sql);

      $this->data['content']='dashboard';
      $this->data['title']='Home | FIREXCOIN';
      $this->load->view('common/template',$this->data);
    }
    else {
      $graph_data = $this->graph_view();
      $strmonth='';
      $strtotal='';
      $graphrecord=array();
      for($i=1;$i<=12;$i++){
        $flag=0;
        foreach($graph_data as  $key=>$value){
          if($i==$value['month']){
            $graphrecord[$i]['month']=date("F", mktime(0, 0, 0, $value['month'], 10));
            $graphrecord[$i]['total']=$value['grand_total'];
            $flag=1;
          }
        }
        if($flag==0){
          $graphrecord[$i]['month']=date("F", mktime(0, 0, 0, $i, 10));
          $graphrecord[$i]['total']=0;
        }
      }
      $this->data['graphrecord']=$graphrecord;
      $member_id=$this->session->userdata('member_id');
      $sql="select sum(amount) as totalbonus from tbl_wallet where member_id=$member_id";
      $this->data['totalbonus'] = $this->m_default->get_single_row($sql);
      
      $sql="select coins as rcoin from tbl_release_wallet where member_id=$member_id";
      $this->data['rcoin'] = $this->m_default->get_single_row($sql);
      
      
       $sql="select coins as mycoins from tbl_coin where member_id=$member_id";
      $this->data['mycoins'] = $this->m_default->get_single_row($sql);
      
      
       
      
      $sql="select sum(amount) as totalfundwallet from tbl_fundwallet where member_id=$member_id";
      $this->data['totalfundwallet'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as matrix from tbl_bonus where member_id=$member_id and bonus_type='Matrix Level'";
      $this->data['matrix'] = $this->m_default->get_single_row($sql);
      $sql="select sum(bv_amount) as repurchase from tbl_bonus where member_id=$member_id and bonus_type='Repurchase'";
      $this->data['repurchase'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as mbv from tbl_bonus where member_id=$member_id and bonus_type='MBV'";
      $this->data['mbv'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as level from tbl_bonus where member_id=$member_id and bonus_type='Level'";
      $this->data['level'] = $this->m_default->get_single_row($sql);
      $sql="select sum(bv_amount) as royalty from tbl_bonus where member_id=$member_id and bonus_type='Royalty'";
      $this->data['royalty'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as direct from tbl_bonus where member_id=$member_id and bonus_type='Direct'";
      $this->data['direct'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as roi from tbl_bonus where member_id=$member_id and bonus_type='ROI'";
      $this->data['roi'] = $this->m_default->get_single_row($sql);
      $sql="select sum(amount) as sponser from tbl_bonus where member_id=$member_id and bonus_type='Sponser'";
      $this->data['sponser'] = $this->m_default->get_single_row($sql);
      $sql="select rank from tbl_member where id=$member_id ";
      $this->data['rank'] = $this->db->query($sql)->row()->rank;
      $sql="select matrix_rank from tbl_member where id=$member_id ";
      $this->data['matrix_rank'] = $this->db->query($sql)->row()->matrix_rank;
      $sql="select amount from tbl_repurchase where member_id=$member_id";
      $this->data['repurchase'] = $this->m_default->get_single_row($sql);
      $sql="select totalleftpv,totalrightpv,leftcount,rightcount from tbl_member where id=$member_id ";
      $valo = $this->db->query($sql)->result_array();
      $this->data['totalleftleg']=$valo[0]['leftcount'];
      $this->data['totalrightleg']=$valo[0]['rightcount'];
      $totalleftpv=$valo['totalleftpv'];
      $totalrightpv=$valo['totalrightpv'];
      if($totalleftpv<$totalrightpv){
        $newpairpv=$totalleftpv;
      }
      else if($totalrightpv<$totalleftpv){
        $newpairpv=$totalrightpv;
      }
      else {
        $newpairpv=$totalrightpv;
      }
      if($newpairpv>=250000){
        $this->data['rewardsin']="1 Crore Cash";
      }
      else if($newpairpv>=100000){
        $this->data['rewardsin']="50,000,00/- Cash";
      }
      else if($newpairpv>=80000){
        $this->data['rewardsin']="28,000,00/- Cash";
      }
      else if($newpairpv>=70000){
        $this->data['rewardsin']="18,000,00/- Cash";
      }
      else if($newpairpv>=60000){
        $this->data['rewardsin']="14,000,00/- Cash";
      }
      else if($newpairpv>=50000){
        $this->data['rewardsin']="900,000/- Cash";
      }
      else if($newpairpv>=20000){
        $this->data['rewardsin']="450,000/- Cash";
      }
      else if($newpairpv>=10000){
        $this->data['rewardsin']="200,000/- Cash";
      }
      else if($newpairpv>=5000){
        $this->data['rewardsin']="120,000/- Cash";
      }
      else if($newpairpv>=2500){
        $this->data['rewardsin']="60,000/- Cash";
      }
      else if($newpairpv>=1000){
        $this->data['rewardsin']="30,000/- Cash";
      }
      else if($newpairpv>=500){
        $this->data['rewardsin']="15,000/- Cash";
      }
      $arraycount['activecount']=0;
      $arraycount['inactivecount']=0;
      $this->data['downlineactive']= $arraycount['activecount'];
      $this->data['downlineinactive']= $arraycount['inactivecount'];
      $this->data['downline']=$valo[0]['leftcount']+$valo[0]['rightcount'];
      $query = "SELECT count(id) as count FROM tbl_member where sponser_id=".$member_id;
      $this->data['directmem']= $this->db->query($query)->row()->count;
      $query = "SELECT count(id) as count FROM tbl_member where member_status=1 and sponser_id=".$member_id;
      $this->data['directmemactive']= $this->db->query($query)->row()->count;
      $query = "SELECT count(id) as count FROM tbl_member where member_status=0 and sponser_id=".$member_id;
      $this->data['directmeminactive']= $this->db->query($query)->row()->count;
      $query = "SELECT package_id FROM  tbl_member where id=".$member_id;
      $this->data['package_id']= $this->db->query($query)->row()->package_id;

      $query = "SELECT package_price FROM  tbl_member where id=".$member_id;
      $this->data['package_price']= $this->db->query($query)->row()->package_price;

      $month=date('m', strtotime(date('Y-m')." -1 month"));
      $sql="select sum(amount) as pmonthly from tbl_bonus where member_id=$member_id and month(created_on)=$month";
      $this->data['pmonthly'] = $this->m_default->get_single_row($sql);

      $month=date('m');
      $sql="select sum(amount) as monthly from tbl_bonus where member_id=$member_id and month(created_on)=$month";
      $this->data['monthly'] = $this->m_default->get_single_row($sql);

      $query = "SELECT * FROM tbl_member order by id desc limit 10 ";
      $this->data['recentmem']= $this->db->query($query)->result_array();

      $this->data['content']='home';
      $this->data['title']='Home | FIREXCOIN';
      $this->load->view('common/template',$this->data);
    }
  }


  public function total_members_down_active($userid,$arraycount){
    $sql="SELECT id,member_status FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    foreach($records as $row){
      if($row['member_status']==1){
          $arraycount['activecount']=$arraycount['activecount']+1;
      }
      if($row['member_status']==0){
        $arraycount['inactivecount']=$arraycount['inactivecount']+1;
      }
      $arraycount= $this->total_members_down_active($row['id'],$arraycount);
    }
    return $arraycount;
  }

   public function total_members_down_inactive($userid,$count)
  {
       $sql="SELECT id FROM tbl_member where parent_id=$userid and member_status=0";
    $records1=$this->db->query($sql)->result_array();
    $count+=count($records1);


    $sql="SELECT id FROM tbl_member where parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
     $count77=count($records);

    if($count77>0)
    {
    foreach($records as $row)
      {
        $count= $this->total_members_down_inactive($row['id'],$count);
      }
    }
      return $count;
  }
  public function referal()
  {
    $this->data['content']='referal';
    $this->data['title']='Home | FIREXCOIN';
    $this->load->view('common/template',$this->data);
  }

  public function graph_view(){
  $member_id=$this->session->userdata('member_id');
  $year = date('Y');
  $sql = "SELECT SUM(`amount`) AS grand_total, MoNTH(`created_on`) AS month FROM tbl_bonus WHERE year(`created_on`) = '$year' and member_id=$member_id GROUP BY MoNTH(`created_on`)";
  return $this->m_default->get_single_row($sql);

}

 public function admingraph_view(){

  $year = date('Y');
  $sql = "SELECT SUM(`amount`) AS grand_total, MoNTH(`created_on`) AS month FROM tbl_bonus WHERE year(`created_on`) = '$year' GROUP BY MoNTH(`created_on`)";
  return $this->m_default->get_single_row($sql);

}

public function wallet()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select sum(amount) as totalbonus from tbl_wallet where member_id=$member_id";
  $this->data['totalbonus'] = $this->m_default->get_single_row($sql);

  $sql="select amount from tbl_repurchase where member_id=$member_id";
  $this->data['repurchase'] = $this->m_default->get_single_row($sql);

  $this->data['content']='wallet';
  $this->data['title']='Wallet | FIREXCOIN';
  $this->load->view('common/template',$this->data);
}

public function withdraw_request()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select amount as totalbonus from tbl_wallet where member_id=$member_id";
  $this->data['totalbonus'] = $this->m_default->get_single_row($sql);
  $sql2="select price from tbl_dollarprice";
  $this->data['dollar_price'] = $this->m_default->get_single_row($sql2);

  $this->data['content']='withdraw_request';
  $this->data['title']='Wallet | FIREXCOIN';
  $this->load->view('common/template',$this->data);
}

public function w_activate()
{

    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('withdraw_status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_withdraw');
    if($flag == true) {

      //$contact_no=  $this->db->query("select contact_number from tbl_member where id in (select member_id from tbl_withdraw where id=$id)")->row()->contact_number;

          /**msg**/

//          $to = $contact_no;
// $message = 'Your withdraw request has been approved successfully! Please check your balance after 1-2 business days.';
//      if($this->msg91->send($to, $message) == TRUE)  {


//      }
      /**msg**/
      $this->session->set_flashdata('success', 'Activated Successfully..!');
      redirect('home/withdraw_list');
    }


}

public function w_decline()
{

    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('withdraw_status' => 2);
    $flag = $this->m_default->update($where, $data, 'tbl_withdraw');
    if($flag == true) {

      $member_id=  $this->db->query('select member_id from tbl_withdraw where id='.$id)->row()->member_id;

      $withdraw_amount=  $this->db->query('select withdraw_amount from tbl_withdraw where id='.$id)->row()->withdraw_amount;


       $query="update tbl_wallet set amount=amount+$withdraw_amount where member_id=$member_id";
    $this->m_default->execute_query($query);

    $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;
    $array=array('member_id'=>$member_id,'desc'=>"Declined Withdraw",'credited'=>$withdraw_amount,'balance'=>$debitedamount,'status'=>1);
    $this->m_default->data_insert('tbl_wallet_report',$array);



      $this->session->set_flashdata('success', 'Declined Successfully..!');
      redirect('home/withdraw_list');
    }

}

public function wc_activate()
{
    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('withdraw_status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_withdrawcoin');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Activated Successfully..!');
      redirect('home/list_withdraw_coin');
    }

}


public function wc_decline()
{
    $id = $this->input->post('id');
    $where = array('id' => $id);
    $data = array('withdraw_status' => 2);
    $flag = $this->m_default->update($where, $data, 'tbl_withdrawcoin');
    if($flag == true) {

      $member_id=  $this->db->query('select member_id from tbl_withdrawcoin where id='.$id)->row()->member_id;
      $withdraw_amount=  $this->db->query('select withdraw_amount from tbl_withdrawcoin where id='.$id)->row()->withdraw_amount;

      $query="update tbl_release_wallet set coins=coins+$withdraw_amount where member_id=$member_id";
      $this->m_default->execute_query($query);

      $this->session->set_flashdata('success', 'Declined Successfully..!');
      redirect('home/list_withdraw_coin');
    }

}


public function save()
{
  $member_id=$this->session->userdata('member_id');


//   $matrixcount=  $this->db->query("select count(id) as count from tbl_matrix where member_id=$member_id")->row()->count;

//     if($matrixcount==0 || $matrixcount=="")
//     {
//         $this->session->set_flashdata("error_login", "You are not eligible to withdraw");
//      redirect('home/member_withdraw_list');

//     }


  $withdraw_amount=$this->input->post('withdraw_amount');

  $query="select amount from tbl_wallet where member_id=$member_id";
  $amount = $this->db->query($query)->row()->amount;

  if($withdraw_amount>=20 && $amount>=$withdraw_amount)
{
   $query="update tbl_wallet set amount=amount-$withdraw_amount where member_id=$member_id";
    $this->m_default->execute_query($query);

    $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;
    $array=array('member_id'=>$member_id,'desc'=>"Withdraw Amount",'debited'=>$withdraw_amount,'balance'=>$debitedamount,'status'=>1);
    $this->m_default->data_insert('tbl_wallet_report',$array);

    $sqlx="select price from tbl_dollarprice where id=1";
    $price = $this->db->query($sqlx)->row()->price;

  $data = array('member_id' => $member_id,'withdraw_amount'=>$withdraw_amount,'coin_price'=>$price);
  $this->m_default->data_insert('tbl_withdraw',$data);
}
  redirect('home/member_withdraw_list');
}

public function withdraw_list()
{
  $member_id=$this->session->userdata('member_id');
//   $sql="select *  from tbl_withdraw where member_id=$member_id";
$sql=" select a.*,b.username, c.account_number,c.wec_wallet, c.ifsc_code,c.name, c.contact_number, c.bank_name,c.account_holder_name,c.branch, c.pan_no from tbl_withdraw a, tbl_login b, tbl_member c where b.member_id=a.member_id and c.id=a.member_id order by a.id desc";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='withdraw_list';
  $this->data['title']='Wallet | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function member_withdraw_list()
{

  $member_id=$this->session->userdata('member_id');
  $sql=" select a.*,b.username, c.account_number,c.wec_wallet, c.ifsc_code,c.name, c.contact_number, c.bank_name,c.account_holder_name,c.branch, c.pan_no from tbl_withdraw a, tbl_login b, tbl_member c where b.member_id=a.member_id and c.id=a.member_id and a.member_id=$member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='withdraw_list';
  $this->data['title']='List Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function member_withdraw_plist()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select *  from tbl_withdraw where member_id=$member_id and withdraw_status=0";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_pendingwithdrawal';
  $this->data['title']='Pending Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function member_withdraw_clist()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select *  from tbl_withdraw where withdraw_status=1 and member_id=$member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_successwithdrawal';
  $this->data['title']='Confirm Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function wallet_report()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select *  from tbl_wallet_report where member_id=$member_id order by id desc";
  $this->data['wallet_report'] = $this->m_default->get_single_row($sql);

  $this->data['content']='wallet_report';
  $this->data['title']='Wallet Report | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function payout_history()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select sum(amount) as amount, created_on from tbl_bonus  group by date(created_on) order by date(created_on) desc";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);
  
//   print_r($this->data['withdraw_list']);
//   die;
  
  foreach($this->data['withdraw_list'] as $key=>$value){
      $this->data['withdraw_list'][$key]['direct']=$this->db->query('select sum(amount) as amount from tbl_bonus where bonus_type="Direct" and date(created_on)=date("'.$this->data['withdraw_list'][$key]['created_on'].'")')->row()->amount;
      $this->data['withdraw_list'][$key]['matching']=$this->db->query('select sum(amount) as amount from tbl_bonus where bonus_type="MBV" and date(created_on)=date("'.$this->data['withdraw_list'][$key]['created_on'].'")')->row()->amount;
    // echo  'select sum(amount) as amount from tbl_bonus where bonus_type="MBV" and date(created_on)=date("'.$this->data['withdraw_list'][$key]['created_on'].'")';
  }

  $this->data['content']='payout_history';
  $this->data['title']='Payout History | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}



public function fundwallet_report()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select *  from tbl_fundwallet_report where member_id=$member_id order by id desc";
  $this->data['wallet_report'] = $this->m_default->get_single_row($sql);

  $this->data['content']='wallet_report';
  $this->data['title']='Wallet Report | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function wallet_report_admin()
{
  $member_id=$this->input->post('id');
  $sql="select *  from tbl_wallet_report where member_id=$member_id order by id desc";
  $this->data['wallet_report'] = $this->m_default->get_single_row($sql);

  $this->data['content']='wallet_report';
  $this->data['title']='Wallet Report | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function withdraw_plist()
{
  $member_id=$this->session->userdata('member_id');

  $sql=" select a.*,b.username, c.account_number, c.ifsc_code,c.name, c.wec_wallet,c.contact_number, c.bank_name,c.account_holder_name,c.branch, c.pan_no from tbl_withdraw a, tbl_login b, tbl_member c where b.member_id=a.member_id and c.id=a.member_id and a.withdraw_status=0";


  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_pendingwithdrawal';
  $this->data['title']='Pending Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function withdraw_clist()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select *  from tbl_withdraw where withdraw_status=1";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_successwithdrawal';
  $this->data['title']='Confirm Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

public function coin_withdraw()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select coins  from tbl_release_wallet where member_id=$member_id";
  $this->data['coins'] = $this->m_default->get_single_row($sql);
  
  $sql="select bitcoin_address  from tbl_member where id=$member_id";
  $this->data['bitcoin_address'] = $this->db->query($sql)->row()->bitcoin_address;
  
  $sql="select wec_wallet  from tbl_member where id=$member_id";
  $this->data['wec_wallet'] = $this->db->query($sql)->row()->wec_wallet;

  $this->data['content']='coin_withdraw';
  $this->data['title']='Coin Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}


// public function get(){
//   $listmember= $this->db->query('select a.id,a.package_price,a.package_id,a.package_add_date,b.coins from tbl_member a, tbl_coin b where a.id=b.member_id and a.id!=1 and a.package_id!=0')->result_array();
   
//   foreach($listmember as $val){
//       $member_id = $val['id'];
     
//       $package_id = $val['package_id'];
      
//       $date1=date_create(date("Y-m-d",strtotime($val['package_add_date'])));
// $date2=date_create(date("Y-m-d"));
// $diff=date_diff($date1,$date2);
// $diffdate= $diff->format("%a");
      
//       if($diffdate>30 ){
          
//           if($package_id==2  || $package_id==4 || $package_id==6 || $package_id==8 || $package_id==9 || $package_id==10 || $package_id==11 || $package_id==12 || $package_id==13){ 
             
//             $rWallet= ($val['coins']*30)/100;
            
//             $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
            
//             $array=array('member_id'=>$member_id,'coins'=>$rWallet);
//       $this->m_default->data_insert('tbl_get_rcoin',$array);
            
//         } 
        
//       }
         
      
//     //   $array=array('member_id'=>$member_id,'coins'=>$rWallet);
//     //   $this->m_default->data_insert('tbl_release_wallet',$array);
      
//     //   $array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
//     //     $this->m_default->data_insert('tbl_member_package',$array);
      
//   }
// }


// public function get_rcoin(){
//   $listmember= $this->db->query('select * from tbl_member_package ')->result_array();
   
//   foreach($listmember as $val){
//       $member_id = $val['member_id'];
     
//       $package_id = $val['package_id'];
      
//       $array=array('percent'=>10,'member_id'=>$member_id,'coins'=>$val['r_coin'],'created_on'=>date('Y-m-d',strtotime($val['created_on'])));
//       $this->m_default->data_insert('tbl_get_rcoin',$array);
      
//     //$array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
//     //$this->m_default->data_insert('tbl_member_package',$array);
      
//   }
// }


public function get_rcoin(){
  $listmember= $this->db->query('select * from tbl_member where member_status=0 and package_id=0 ')->result_array();
   
  foreach($listmember as $val){
      $member_id = $val['id'];
     
     $this->db->query("update tbl_release_wallet set coins=0 where member_id=$member_id");
     $this->db->query("update tbl_coin set coins=0 where member_id=$member_id");
      
  }
}
                                            


public function save_coin(){
    
    $withdraw_amount=$this->input->post('withdraw_amount');

    $member_id=$this->session->userdata('member_id');
    $sql="select coins  from tbl_release_wallet where member_id=$member_id";
    $available_coin = $this->m_default->get_single_row($sql);

    print_r($this->input->post());
    
    if (!($withdraw_amount <= $available_coin[0]['coins'])){
      $this->session->set_flashdata('error_login', "Coins not available");
      redirect("home/coin_withdraw");
    }

    $wec_wallet=$this->input->post('wec_wallet');

    if(empty(trim($wec_wallet))){

      $this->session->set_flashdata('error_login', "Please add wec wallet address");
      redirect("home/coin_withdraw");

    }

    
    $member_id=$this->session->userdata('member_id');
    $count =$this->db->query("select count(id) as count from tbl_withdrawcoin where  withdraw_status =0 and  member_id=$member_id")->row()->count;
            
    if($count==0){
    
    $otp=$this->input->post('otp');
      
    
            $count =$this->db->query("select count(id) as count from tbl_profile_otp where  otp_type =1 and otp='$otp' and status=0 and member_id=$member_id")->row()->count;
            
            if($count==0){
                 $this->session->set_flashdata('error_login', 'Please Enter Valid OTP');
                 redirect("home/coin_withdraw");
            }

            else{
                $this->db->query("update tbl_profile_otp set status=1 where otp_type =1 and otp='$otp' and status=0 and member_id=$member_id");
            
            }
    
    
    
    $wallet=$this->input->post('wallet');
    $bitcoin_address=$this->input->post('bitcoin_address');
    
    $array=array('member_id'=>$member_id,'withdraw_amount'=>$withdraw_amount,'bitcoin_address'=>$bitcoin_address,'wec_wallet'=>$wec_wallet);
    $this->m_default->data_insert('tbl_withdrawcoin',$array);
    
    $this->db->query('update tbl_release_wallet set coins=coins-'.$withdraw_amount.' where member_id='.$member_id);
    }
    redirect('home/coin_withdraw');
  }
  
  public function list_withdraw_coin()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.*,b.name, b.contact_number, c.username from tbl_withdrawcoin a, tbl_member b, tbl_login c where a.member_id=b.id and a.member_id=$member_id and c.member_id=$member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_coinwithdrawal';
  $this->data['title']='Coin Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

  public function list_withdraw_admincoin()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.*,b.name, b.contact_number, c.username from tbl_withdrawcoin a, tbl_member b, tbl_login c where a.member_id=b.id  and c.member_id=a.member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_coinwithdrawal';
  $this->data['title']='Coin Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}


  public function list_pwithdraw_admincoin()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.*,b.name, b.package_price, b.contact_number, c.username, d.coins as total_coin, d.coin_price, e.coins as release_coin from tbl_withdrawcoin a, tbl_member b, tbl_login c, tbl_coin d, tbl_release_wallet e where a.member_id=b.id and c.member_id=a.member_id and withdraw_status=0 and d.member_id = a.member_id and e.member_id = a.member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);
  $this->data['content']='list_coinwithdrawal';
  $this->data['title']='Coin Withdrawal | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

  public function coin_history()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.*  from tbl_member_package a  where a.member_id=$member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='coin_history';
  $this->data['title']='Coin History | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}

  public function rcoin_history()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.*  from tbl_get_rcoin a  where a.member_id=$member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='rcoin_history';
  $this->data['title']='Coin History | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}
  
   public function list_release_wallet()
{
  $member_id=$this->session->userdata('member_id');
  $sql="select a.* , b.username,c.name, d.coins as total_coin from tbl_release_wallet a , tbl_login b, tbl_member c, tbl_coin d where b.member_id=a.member_id and b.member_id=c.id and d.member_id = a.member_id";
  $this->data['withdraw_list'] = $this->m_default->get_single_row($sql);

  $this->data['content']='list_release_wallet';
  $this->data['title']='Coin History | FIREXCOIN';
  $this->load->view('common/template',$this->data);

}
	public function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

 public function coin_otp()  //to load home page
  {
      $email= $this->session->userdata("email");
      $member_id= $this->session->userdata("member_id");
      
       $otp = $this->generateRandomString();
       
        $subject = 'Coin Withdrawal';
     
       $message = "
       <!DOCTYPE html>
       <html>
        <head>
          <meta charset='utf-8'>
          <title></title>
        </head>
        <body>
        
        <p>To withdraw coins use below provided OTP.</p>
        <table>
        
        <tr>
          <td>OTP:</td><td>$otp</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        
        <tr>
          <td></td><td></td>
        </tr>
        
       </table>
       
       <p>Team WaveEduCoin</p>
</body>
       </html>

       ";


// <tr>
//           <td>Valid Upto</td><td>$tomorrow</td>
//         </tr>

      $this->mail->sendEmail($email,$subject,$message);
			
			$logindata = array('otp' => $otp,'member_id'=>$member_id,'otp_type'=>1);
            $this->m_default->data_insert('tbl_profile_otp',$logindata);
			
  }
  
  

}
