<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	/**
	 * profile Controller: For Loading member profile page
	 * Author: Cydigo
	 */
	public function __construct(){
    parent::__construct();
    $this->load->model('m_default');
	}

  public function index(){

    $member_id = $this->session->userdata('member_id');
    $sql = "select * from tbl_member where id = '$member_id'";
    $this->data['row'] = $this->m_default->get_single_row($sql);
    //end of get member data
    $this->data['title']='Member | Member Profile';
    $this->data['content'] = 'profile';
    $this->load->view('common/template',$this->data);
    foreach($this->input->post() as $items){
      if ($items != ''){
          if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
            $this->session->set_flashdata('error_login','Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
            break;
          }
      }
    }
  }
  //end of index function

  public function update_member_data(){

    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('contact_no', 'Contact Number', 'trim|required|integer|exact_length[10]');
    $this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
    $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->index();
    }else {
      $first_name = $this->input->post('first_name');
      $last_name = $this->input->post('last_name');
      $mobile = $this->input->post('contact_no');

      $get_dob = $this->input->post('dob');
      $final_date = date("Y-m-d", strtotime($get_dob));

      $gender = $this->input->post('gender');
      $address = $this->input->post('address');
      $where = array('id' => $this->session->userdata('user_id'));
      $data = array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'mobile' => $mobile,
        'dob' => $final_date,
        'gender' => $gender,
        'address' => $address
      );
      $update = $this->m_default->update('tbl_members', $data, $where);
      if ($update) {
        $this->session->set_flashdata('update_member', 'Your profile updated successfully');
        redirect('Profile');
      }
    }

  }
  //end of update_member_data function

  public function update_member_password(){

    $member_id = $this->session->userdata('user_id');

    $this->form_validation->set_rules('user_password', 'Recent password', 'trim|required');
    $this->form_validation->set_rules('new_password', 'New password', 'trim|required');
    $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->index();
    }else {
      $user_password = $this->input->post('user_password');
      $new_password = $this->input->post('new_password');
      $confirm_password = $this->input->post('confirm_password');
      $num_rows_query = "select * from tbl_members where id = '$member_id' and password = '$user_password'";
      $prev_pass = $this->m_default->get_num_rows($num_rows_query);
      if ($prev_pass < 1) {
        $this->data['prev_pass_error'] = "Your recent password does not matched";
        $this->index();
      }//prev password checking here
      else {
        if ($new_password != $confirm_password) {
          $this->data['conf_pass_error'] = "New password and confirm password does not matched";
          $this->index();
        }//checking new password and confirm password match
        else {
          $where = array('id' => $member_id);
          $data = array(
            'password' => $confirm_password
          );
          $update_password = $this->m_default->update('tbl_members', $data, $where);
          if ($update_password) {
            redirect(base_img_url);
          }
        }
      }
    }

  }
  // end of update_member_password function

  public function member_image_upload(){
    $member_id = $this->session->userdata('user_id');

    $config['upload_path'] = './../uploads/memberimage/';
    $config['allowed_types'] = 'jpg|png';
    $config['file_name'] = 'member_image' . date('YmdHis');
    $this->load->library('upload', $config);
    $upload = $this->upload->do_upload('profile_pic');
    if (!$upload) {
      $this->data['member_img_error'] = 'There is some problems to upload image';
      $this->index();
    }else {
      $upload_data = $this->upload->data();
      $image_path = 'uploads/memberimage/' . $config['file_name'] . $upload_data['file_ext'];
      $data = array(
        'image_path' => $image_path
      );
      $where = array('id' => $member_id);
      $img_upload = $this->m_default->update('tbl_members', $data, $where);
      if ($img_upload) {
        $this->session->set_flashdata('image_upload_msg', 'Your ppicture successfully uploaded');
        redirect('Profile');
      }
    }
  }
  //end of member_image_upload function




}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
