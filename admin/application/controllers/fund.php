<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fund extends CI_Controller {

	/**
	 * Package Controller
	 * Author: Dipanwita Chanda
	**/

  public function __construct(){
    parent::__construct();
    $username= $this->session->userdata("username");
    if (empty($username)) {
      $this->session->set_flashdata("error_login", "Invalid Request");
      redirect("login", "refresh");
    }
    $this->load->model('m_default');
    // foreach($this->input->post() as $items){
    //   if ($items != ''){
    //       if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
    //         $this->session->set_flashdata('error_login','Something went wrong');
    //         redirect($_SERVER['HTTP_REFERER']);
    //         break;
    //       }
    //   }
    // }
    $sql="select coin_price from tbl_coinprice where id=1";
    $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
  }
  
   public function iwallet_balance()
  {
      
  $member_id=  $this->input->post('member_id');
  $wallet_type=  $this->input->post('wallet_type');
  
  if($wallet_type==1){
      echo  $this->db->query("Select amount from tbl_fundwallet where member_id=$member_id")->row()->amount;

  }
  else{
      echo  $this->db->query("Select amount from tbl_wallet where member_id=$member_id")->row()->amount;

  }
  
  }
  public function getmembername()
  {
  $member_id=  $this->input->post('member_id');
  echo  $this->db->query("Select name from tbl_member where id=$member_id")->row()->name;
  }
public function add_deductfund(){
     $member_id=$this->session->userdata('member_id');
    $sql="SELECT  member_id,username FROM tbl_login WHERE member_id!=$member_id and usertype=2";
    $this->data['members']=$this->db->query($sql)->result_array();
    $this->data['content']='iwallet/iwallet';
    $this->data['title']='Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }


public function action(){
    $member_id=$this->input->post('member_id');
    $action=$this->input->post('mode');
    $amount=$this->input->post('amount');
    $bonustype=$this->input->post('remarks');
    $wallet_type=$this->input->post('wallet_type');
    

if($wallet_type==1){
    if($action==1){
      // $bonustype='Fund Added';
      $query="update tbl_fundwallet set amount=amount+$amount where member_id=$member_id";
    	$this->m_default->execute_query($query);

    	$debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$member_id ")->row()->amount;

    	$array=array('member_id'=>$member_id,'desc'=>$bonustype,'credited'=>$amount,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>1);
    	$this->m_default->data_insert('tbl_fundwallet_report',$array);
      $this->session->set_flashdata("success", "Transfered Successfully..!");
    }
    else{
        // $bonustype='Fund Deducted';
        $query="update tbl_fundwallet set amount=amount-$amount where member_id=$member_id ";
      	$this->m_default->execute_query($query);

      	$debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$member_id ")->row()->amount;

      	$array=array('member_id'=>$member_id,'desc'=>$bonustype,'debited'=>$amount,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>2);
      	$this->m_default->data_insert('tbl_fundwallet_report',$array);
          $this->session->set_flashdata("success", "Deducted Successfully..!");
    }
}

else{
    if($action==1){
      // $bonustype='Fund Added';
      $query="update tbl_wallet set amount=amount+$amount where member_id=$member_id";
    	$this->m_default->execute_query($query);

    	$debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;

    	$array=array('member_id'=>$member_id,'desc'=>$bonustype,'credited'=>$amount,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>1);
    	$this->m_default->data_insert('tbl_wallet_report',$array);
      $this->session->set_flashdata("success", "Transfered Successfully..!");
    }
    else{
        // $bonustype='Fund Deducted';
        $query="update tbl_wallet set amount=amount-$amount where member_id=$member_id ";
      	$this->m_default->execute_query($query);

      	$debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;

      	$array=array('member_id'=>$member_id,'desc'=>$bonustype,'debited'=>$amount,'wallet_type'=>$wallet_type,'balance'=>$debitedamount,'status'=>2);
      	$this->m_default->data_insert('tbl_wallet_report',$array);
          $this->session->set_flashdata("success", "Deducted Successfully..!");
    }
}


    redirect('fund/add_deductfund');
  }

  public function index(){
    $this->data['content']='fund/fund_request';
    $this->data['title']='Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }
  
   public function fund_transfer(){
        $member_id=$this->session->userdata('member_id');
    $sql="select amount as wallet from tbl_wallet where member_id=$member_id";
    $this->data['wallet'] = $this->m_default->get_single_row($sql);
    
    $sql="select amount as fundwallet from tbl_fundwallet where member_id=$member_id";
    $this->data['fundwallet'] = $this->m_default->get_single_row($sql);
    $this->data['content']='fund/fund_transfer';
    $this->data['title']='Fund Transfer | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }
  
   public function transfered_fund(){
      $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,c.payment_type,b.username,b.email, c.created_on AS bonusdate FROM tbl_transfered_fund c, tbl_member a,tbl_login b WHERE c.member_id=$member_id AND a.id=c.`transfered_to` AND  b.member_id=c.transfered_to order by c.created_on desc";
  $this->data['spillincome']=$this->m_default->get_user_list($sql);
    $this->data['content']='fund/transfered_fund';
    $this->data['title']='Transfered Fund | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }
  
  
   public function recieved_fund(){
       $member_id=$this->session->userdata('member_id');
  $sql="SELECT  a.* ,c.amount ,c.payment_type,b.username,b.email, c.created_on AS bonusdate FROM tbl_transfered_fund c, tbl_member a,tbl_login b WHERE c.transfered_to=$member_id AND a.id=c.`member_id` AND  b.member_id=c.member_id order by c.created_on desc";
  $this->data['spillincome']=$this->m_default->get_user_list($sql);
  
    $this->data['content']='fund/recieved_fund';
    $this->data['title']='Received Fund | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }
  
  
  public function transfer()
 {
     $walletmember_id=$this->session->userdata('member_id');
     
     $price=$this->input->post('amount');

     if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $price)){
          $this->session->set_flashdata('danger','Something went wrong');
          redirect($_SERVER['HTTP_REFERER']);
      }

      
     if($price<0){
         $this->session->set_flashdata('danger','Please Enter Proper Amount..!');
         redirect('fund/fund_transfer');
     }

    
     $payment=$this->input->post('payment');
     
     if($payment=="e_wallet"){
      $amountepin=$this->db->query("select amount from tbl_wallet where member_id=$walletmember_id")->row()->amount;   
     }
     
     else if($payment=="fund_wallet"){
      $amountepin=$this->db->query("select amount from tbl_fundwallet where member_id=$walletmember_id")->row()->amount;   
     }
     
     
     if($amountepin<$price){
         $this->session->set_flashdata('danger','Insufficient Balance..!');
         redirect('fund/fund_transfer');
     }
     else{
         
     $member_idusername=$this->input->post('member_id');

     if (! preg_match("/^[a-z0-9A-Z]+$/i", $member_idusername)){
      $this->session->set_flashdata('danger','Something went wrong');
      redirect($_SERVER['HTTP_REFERER']);

     }
     
     $member_id=$this->db->query('select member_id from tbl_login where username="'.$member_idusername.'"')->row()->member_id;
     if(!isset($member_id)){
           $this->session->set_flashdata('danger','Invalid Member Id..!');
         redirect('fund/fund_transfer');
     }
     else{
         
         
   
   
   
   $array=array('member_id'=>$walletmember_id,'transfered_to'=>$member_id,'amount'=>$price,'payment_type'=>$payment);
   $this->m_default->data_insert('tbl_transfered_fund',$array);
         
    
    if($payment=="e_wallet"){
      
      $this->db->query("update tbl_wallet set amount=amount+$price where member_id=$member_id");     
      $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$member_id ")->row()->amount;
      $array=array('member_id'=>$member_id,'desc'=>'Fund Received','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_wallet_report',$array);

      $this->db->query("update tbl_wallet set amount=amount-$price where member_id=$walletmember_id");
      
      $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
      $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_wallet_report',$array);
      
     }
     
    else if($payment=="fund_wallet"){

      $this->db->query("update tbl_fundwallet set amount=amount+$price where member_id=$member_id");     
      $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$member_id ")->row()->amount;
      $array=array('member_id'=>$member_id,'desc'=>'Fund Received','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_fundwallet_report',$array);


      $this->db->query("update tbl_fundwallet set amount=amount-$price where member_id=$walletmember_id");     
      
      $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
      $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_fundwallet_report',$array);
     }
     
     }
     $this->session->set_flashdata('success','Transfered Successfully');
    
     
      }
       redirect('fund/fund_transfer');
 }

  public function list_fund(){
    $member_id=$this->session->userdata('member_id');
    $this->data['listfund']=$this->db->query('select a.* , b.username  from tbl_fundtransfer a, tbl_login b where a.member_id=b.member_id and a.member_id='.$member_id)->result_array();
    $this->data['content']='fund/list_fund';
    $this->data['title']='List Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function list_funds(){
    $member_id=$this->session->userdata('member_id');
    $this->data['listfund']=$this->db->query('select a.* , b.username  from tbl_fundtransfer a, tbl_login b where  a.member_id=b.member_id order by a.id desc')->result_array();
    $this->data['content']='fund/list_fund';
    $this->data['title']='List Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function decline(){
    $id= $this->input->post('id');
    $this->db->query('update tbl_fundtransfer set status=2 where id='.$id);
    $this->data['listfund']=$this->db->query('select a.* , b.username  from tbl_fundtransfer a, tbl_login b where  a.member_id=b.member_id')->result_array();
    $this->data['content']='fund/list_fund';
    $this->data['title']='List Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function accept(){
    $id= $this->input->post('id');
    $this->db->query('update tbl_fundtransfer set status=1 where id='.$id);
    echo  $amount=$this->db->query('select amount from tbl_fundtransfer where id='.$id)->row()->amount;
    echo $member_id=$this->db->query('select member_id from tbl_fundtransfer where id='.$id)->row()->member_id;
    $this->db->query("update tbl_fundwallet set amount=amount+$amount where member_id=$member_id");
    $this->data['listfund']=$this->db->query('select a.* , b.username  from tbl_fundtransfer a, tbl_login b where  a.member_id=b.member_id')->result_array();
    $this->data['content']='fund/list_fund';
    $this->data['title']='List Fund Request | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function save_slip(){
    $this->form_validation->set_rules('transaction_id', 'Transaction Id', 'is_unique[tbl_fundtransfer.transaction_id]');
    $amount=$this->input->post('amount');
    $member_id=$this->session->userdata('member_id');
    $contact_number=$this->input->post('contact_number');
    $transaction_id=$this->input->post('transaction_id');
    $message=$this->input->post('message');
    $bank_id=$this->input->post('bank_id');
    $date=$this->input->post('date');
    $time=$this->input->post('time');
    $config['upload_path'] = './uploads/bankimage/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['file_name'] = 'bank_image' . date('YmdHis');
    $this->load->library('upload', $config);
    $upload = $this->upload->do_upload('userfile');
    if (!$upload) {
      echo $error = $this->upload->display_errors();
      $this->session->set_flashdata('upload_user_image', $error);
      $this->data['member_img_error'] = 'There is some problems to upload image';die;
      redirect('package/upload_bank');
    }else {
      $upload_data = $this->upload->data();
      $image_path =  $config['file_name'] . $upload_data['file_ext'];
      $array=array('date'=>date('Y-m-d',strtotime($date)),'time'=>$time,'bank_id'=>$bank_id,'member_id'=>$member_id,'message'=>$message,'upload_image'=>$image_path,'transaction_id'=>$transaction_id,'amount'=>$amount,'contact_number'=>$contact_number);
      $this->m_default->data_insert('tbl_fundtransfer',$array);
      $this->session->set_flashdata('success', 'Uploaded Successfully');
      redirect('fund/list_fund');
    }
  }
}
