<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	/**
	 * profile Controller: For Loading member profile page
	 * Author: Cydigo
	 */
	public function __construct(){
    parent::__construct();
		$username= $this->session->userdata("username");
		if (empty($username)) {
		$this->session->set_flashdata("error_login", "Invalid Request");
		redirect("login", "refresh");
		}
    $this->load->model('m_default');
    $this->load->model('includes/mail');
    $sql="select coin_price from tbl_coinprice where id=1";
      $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
    // foreach($this->input->post() as $items){
    //   if ($items != ''){
    //       if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
    //         $this->session->set_flashdata('error_login','Something went wrong');
    //         redirect($_SERVER['HTTP_REFERER']);
    //         break;
    //       }
    //   }
    // }  
	}
	
	public function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
	 public function profile_otp()  //to load home page
  {
      $email= $this->session->userdata("email");
      $member_id= $this->session->userdata("member_id");
      
       $otp = $this->generateRandomString();
       
        $subject = 'Profile Update';
     
       $message = "
       <!DOCTYPE html>
       <html>
        <head>
          <meta charset='utf-8'>
          <title></title>
        </head>
        <body>
        
        <p>To Update your Profile use below provided OTP.</p>
        <table>
        
        <tr>
          <td>OTP:</td><td>$otp</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        
        <tr>
          <td></td><td></td>
        </tr>
        
       </table>
       
       <p>Team WaveEduCoin</p>
</body>
       </html>

       ";


// <tr>
//           <td>Valid Upto</td><td>$tomorrow</td>
//         </tr>

      $this->mail->sendEmail($email,$subject,$message);
			
			$logindata = array('otp' => $otp,'member_id'=>$member_id);
            $this->m_default->data_insert('tbl_profile_otp',$logindata);
			
  }

  public function index(){

    $member_id = $this->session->userdata('member_id');
    $sql = "select * from tbl_member where id = '$member_id'";
    $this->data['row'] = $this->m_default->get_single_row($sql);
    //end of get member data
    $this->data['title']='Member | FIREXCOIN';
    $this->data['content'] = 'profile';
    $this->load->view('common/template',$this->data);
  }

	public function payment(){
    $sql = "select * from tbl_payment_address where id = '1'";
    $this->data['row'] = $this->m_default->get_single_row($sql);

    $this->data['title']='Payment Address | FIREXCOIN';
    $this->data['content'] = 'payment_address';
    $this->load->view('common/template',$this->data);
  }

	public function update_payment(){
		$payment_hash=$this->input->post('payment_hash');
		$query="update tbl_payment_address set payment_hash='".$payment_hash."' where id=1";
		$this->m_default->execute_query($query);
    redirect('profile/payment');
  }

  //end of index function

  public function update_member_data(){
      
      
      $otp=$this->input->post('otp');
      
      $member_id=$this->session->userdata('member_id');
       
      
            $count =$this->db->query("select count(id) as count from tbl_profile_otp where  otp_type =0 and otp='$otp' and status=0 and member_id=$member_id")->row()->count;
            
            if($count==0){
                 $this->session->set_flashdata('error_login', 'Please Enter Valid OTP');
                 redirect("profile");
            }
            else{
                $this->db->query("update tbl_profile_otp set status=1 where otp_type =0 and otp='$otp' and status=0 and member_id=$member_id");
            
            }
       

    // $this->form_validation->set_rules('name', 'Name', 'trim|required');
    // $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
    // $this->form_validation->set_rules('contact_no', 'Contact Number', 'trim|required|integer|exact_length[10]');
    // $this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
    // $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
    
  
    
    // if ($this->form_validation->run() == FALSE) {
    //   $this->index();
    // }else {
    //   $first_name = $this->input->post('first_name');
    //   $last_name = $this->input->post('last_name');
    //   $mobile = $this->input->post('contact_no');

    //   $get_dob = $this->input->post('dob');
    //   $final_date = date("Y-m-d", strtotime($get_dob));

    //   $gender = $this->input->post('gender');
    //   $address = $this->input->post('address');
    //   $where = array('id' => $this->session->userdata('user_id'));
    //   $data = array(
    //     'first_name' => $first_name,
    //     'last_name' => $last_name,
    //     'mobile' => $mobile,
    //     'dob' => $final_date,
    //     'gender' => $gender,
    //     'address' => $address
    //   );
    //   $update = $this->m_default->update('tbl_members', $data, $where);
    //   if ($update) {
    //     $this->session->set_flashdata('update_member', 'Your profile updated successfully');
    //     redirect('Profile');
    //   }
    // }
   
    if(!empty(trim($_POST['name']))) {
        $where = array('id' => $this->session->userdata('member_id'));
        $data = $_POST;
        // print_r($this->session->all_userdata());
         //print_r($where);
        $update = $this->m_default->update( $where, $data,'tbl_member');
      if ($update) {
        $this->session->set_flashdata('update_member_massage', 'Your profile updated successfully');
        redirect('profile');
      }
    }else{
        $this->session->set_flashdata('update_member_massage', 'Name and contact number required.. .');
        redirect('profile');
    }

  }
  //end of update_member_data function

  public function update_member_password(){

    $member_id = $this->session->userdata('member_id');
$this->form_validation->set_rules('user_password', 'User password', 'trim|required');
    $this->form_validation->set_rules('new_password', 'New password', 'trim|required');
    $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->index();
    }else {
      $user_password = $this->input->post('user_password');
      $new_password = $this->input->post('new_password');
      $confirm_password = $this->input->post('confirm_password');
      $num_rows_query = "select * from tbl_login where member_id = '$member_id' and password = '".md5($user_password)."'";
      $prev_pass = $this->db->query($num_rows_query)->result_array();
      if (count($prev_pass) < 1) {
        
        $this->session->set_flashdata('error','Your recent password does not matched..!');
             redirect('profile');
      }//prev password checking here
      else {
        if ($new_password != $confirm_password) {
         $this->session->set_flashdata('error','New password and confirm password does not matched..!');
             redirect('profile');
        }//checking new password and confirm password match
        else {
          $where = array('member_id' => $member_id);
          $data = array(
            'password' => md5($confirm_password)
          );
          $update_password = $this->m_default->update($where,$data,'tbl_login');
          if ($update_password) {
              $this->session->set_flashdata('success','Password Changed Successfully..!');
             redirect('profile');
          }
        }
      }
    }

  }

  public function member_image_upload(){
    $member_id = $this->session->userdata('member_id');

    $config['upload_path'] = './uploads/userimage/';
    $config['allowed_types'] = 'jpg|png';
    $config['file_name'] = 'member_image' . date('YmdHis');
    $this->load->library('upload', $config);
    $upload = $this->upload->do_upload('profile_pic');
    if (!$upload) {
			echo $error = $this->upload->display_errors();
                   $this->session->set_flashdata('upload_user_image', $error);
      $this->data['member_img_error'] = 'There is some problems to upload image';die;
      $this->index();
    }else {
      $upload_data = $this->upload->data();
      $image_path =  $config['file_name'] . $upload_data['file_ext'];
      $data = array(
        'user_image' => $image_path
      );

			$this->session->set_userdata('user_image',$image_path);
      $where = array('id' => $member_id);
      $img_upload = $this->m_default->update($where, $data, 'tbl_member');
      if ($img_upload) {
        $this->session->set_flashdata('image_upload_msg', 'Your picture successfully uploaded');
        redirect('profile');
      }
    }
  }
  //end of member_image_upload function

 public function transactional_password(){

    $member_id = $this->session->userdata('member_id');

    $this->form_validation->set_rules('new_password', 'New password', 'trim|required');
    $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->index();
    }else {
     
      $new_password = $this->input->post('new_password');
      $confirm_password = $this->input->post('confirm_password');
      
        if ($new_password != $confirm_password) {
          $this->data['conf_pass_error'] = "New password and confirm password does not matched";
          $this->index();
        }//checking new password and confirm password match
        else {
          $where = array('id' => $member_id);
          $data = array(
            'transactional_password' => $confirm_password
          );
          $update_password = $this->m_default->update($where,$data,'tbl_member');
          if ($update_password) {
            redirect('profile');
          }
        }
      
    }

  }
  


}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
			