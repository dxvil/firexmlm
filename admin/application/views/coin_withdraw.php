<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Withdraw Coins</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Withdraw Coins</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
         <?php
                  $error=$this->session->flashdata('error_login');
                  echo (!empty($error))?
                  "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                            <h5>Withdraw Request</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                            <div class="card-body">
                                <!-- <h4 class="card-title">Withdraw Request </h4> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/home/save_coin" method="post">
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Wallet Balance</label>
                                      
                                            <input type="text" class="form-control" id="exampleInputuname" placeholder="" value="<?php echo $coins[0]['coins'];?>" readonly>
                                    
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Withdraw Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                            <input type="number" class="form-control" name="withdraw_amount" id="withdraw_amount" placeholder="" min=0 max="<?php echo $coins[0]['coins']?>" required>
                                        </div>
                                    </div>
                                     </div>
                                     <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Choose Wallet</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                               <select id="wallet" name="wallet" class="form-control">
                                   <option value="1">Bitcoin Wallet</option>
                                   <option value="2">FRX Wallet</option>
                                
                               </select>
                                        </div>
                                    </div>
                                    
                                    <div id="divbitcoinwallet" class="form-group">
                                        <label for="exampleInputuname">Bitcoin Wallet</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                            <input type="text" class="form-control" name="bitcoin_address" id="bitcoin_address" placeholder="" value="<?php echo $bitcoin_address?>" required readonly>
                                        </div>
                                    </div>
                                    
                                    <div id="divwecwallet" style="display:none;"  class="form-group">
                                        <label for="exampleInputuname">FRX Wallet</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                            <input type="text" class="form-control" name="wec_wallet" id="wec_wallet" placeholder="" value="<?php echo $wec_wallet?>" required readonly>
                                        </div>
                                    </div>
                                    </div></div>
                                    
                                     <div class="row">
                                    <div class="col-md-6">
                                        
                                        <div class="form-group">
                                        <label for="exampleInputuname">OTP</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="otp" placeholder="Enter OTP" required value="" name="otp" >
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                         <div class="form-group">
                                        <label for="exampleInputuname">&nbsp;</label>
                                       
                                           <button id="btnotp" type="button" class="btn btn-warning waves-effect waves-light m-r-10" style="padding: 7px;
    margin-top: 32px;">Send OTP</button> <p style="color:green;display:none;" id="potp" >Please Check Your Mail.!</p>
                                        
                                    </div>
                                         
                                     </div>
                                </div>
                                    
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      
       <script type="text/javascript">
      $( "#btnotp" ).click(function() {
         
              var url = '<?php echo base_url(); ?>index.php/home/coin_otp';
          var data = {
            sponser_id : '',
          }
          $.post(url, data).done(function(msg){
            //   alert(msg);
               $( "#potp" ).show();
          });
          
          
        
      });

    
    </script>

      <script type="text/javascript">
          $(document.body).on('change',"#wallet",function (e) {
           if($("#wallet").val()==1){
               $("#divwecwallet").hide();
               $("#divbitcoinwallet").show();
           }
           else if($("#wallet").val()==2){
               $("#divwecwallet").show();
               $("#divbitcoinwallet").hide();
           }
          });
      </script>
      
      

      