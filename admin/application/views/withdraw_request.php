<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Withdraw Request</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Withdraw Request</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                            <h5>Withdraw Request</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                            <div class="card-body">
                                <!-- <h4 class="card-title">Withdraw Request </h4> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/home/save" method="post">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Wallet Balance</label>
                                      
                                            <input type="text" class="form-control" id="exampleInputuname" placeholder="" value="$<?php echo $totalbonus[0]['totalbonus'];?>" readonly>
                                    
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="exampleInputuname">Equivalent Coin</label>
                                      
                                            <input type="text" class="form-control"  placeholder="Equivalent Coin" value="<?php echo round(($totalbonus[0]['totalbonus']*70)/$dollar_price[0]['price']);?>" readonly>
                                    
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="exampleInputuname">Withdraw Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                            <input type="number" class="form-control" name="withdraw_amount" id="withdraw_amount" placeholder="" min=20 max="<?php echo $totalbonus[0]['totalbonus']<=10000?$totalbonus[0]['totalbonus']:10000;?>" required>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label for="exampleInputuname">Equivalent Coin of Withdraw Amount</label>
                                      
                                            <input type="text" class="form-control"  name="equivalent_coins" id="equivalent_coins" placeholder="Equivalent Coin" value="0" readonly>
                                    
                                    </div>
                                    
                                    
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        
        
    <script src="<?php echo base_url() ?>assets/js/vendor-all.min.js"></script>
    <!--<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
    <!--<script src="<?php echo base_url() ?>assets/js/pcoded.min.js"></script>-->
        
        <script type="text/javascript">
      $( "#withdraw_amount" ).change(function() {
          
          var equivalent_coins= ($( "#withdraw_amount" ).val()*70)/<?php echo  $dollar_price[0]['price'];?>;
        
        $('#equivalent_coins').val(Math.round(equivalent_coins));
        
      });

    </script>
