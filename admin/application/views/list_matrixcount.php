<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Matrix Count</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Matrix Count</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
          <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card">
                <div class="card-body">
                  <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                    <div class="table-responsive m-t-40">
                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Level</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>#</th>
                            <th>Level</th>
                            <th>Count</th>
                          </tr>
                        </tfoot>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Level - 1</td>
                          <td><?php echo  $level1?></td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Level - 2</td>
                          <td><?php echo  $level2?></td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Level - 3</td>
                          <td><?php echo  $level3?></td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>Level - 4</td>
                          <td><?php echo  $level4?></td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>Level - 5</td>
                          <td><?php echo  $level5?></td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>Level - 6</td>
                          <td><?php echo $level6?></td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>Level - 7</td>
                          <td><?php echo  $level7?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
