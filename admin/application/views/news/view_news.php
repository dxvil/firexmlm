<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">View News</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">View News</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-md-12">
                          <div class="card card-inverse card-danger">
                              <div class="card-header">
                                  <!-- <h4 class="m-b-0 text-white">Card Title</h4></div> -->
                              <div class="card-body">
                                  <h3 class="card-title"><?php echo $news[0]['subject']?></h3>
                                  <p class="card-text"><?php echo $news[0]['message']?></p>
                              </div>
                          </div>
                      </div>
                </div>
            </div>
        </div>
        <!-- Row -->
