<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">List News</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">List News</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->

                                <?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">
                                          <h4 class="card-title">List News</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Subject</th>
                                                      <th>Message</th>
                                                      <th>Date</th>
                                                      <th>Action</th>
                                                    </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>Subject</th>
                                                        <th>Message</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                      <?php
                                                        $edit_url=base_url().'index.php/news/edit_news';
                                                        $view_url=base_url().'index.php/news/view_news';
                                                        $delete=base_url().'index.php/news/delete';
                                                         $i=1; foreach ($listnews as $value) {?>
                                                      <tr>
                                                          <td><?php echo  $i;?></td>
                                                          <td><?php echo  $value['subject']?></td>
                                                          <td><?php echo  substr($value['message'],0,20)?>..</td>

                                                          <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>

                                                          <td><button  title="Edit News"  type="button"
                                                         onclick="action('<?php echo $edit_url;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </button>
                                                         <button  title="View News"  type="button"
                                                        onclick="action('<?php echo $view_url;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-file"></i> </button>
                                                         <button type="button"
                                                         onclick="confirmaction('<?php echo $delete;?>',<?php echo $value['id']; ?>,'Are you sure you want to Delete this News?')" class="btn btn-danger btn-circle"  title="Delete News"><i class="fa fa-times"></i> </button>
                                                       </td>

                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div></div></div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->

                                <script type='text/javascript'>

                                function action(url,id)
                                {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();

                                }

                                function confirmaction(url,id,msg)
                                {
                                 var strconfirm = confirm(msg);
                                 if (strconfirm == true)
                                            {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();
                                 }
                                }
                                </script>
