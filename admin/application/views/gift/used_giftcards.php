<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">My Used E-Pin</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">My Used E-Pin</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <?php
          $error=$this->session->flashdata('danger');
          echo (!empty($error))?
          "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>
          <?php
          $error=$this->session->flashdata('success');
          echo (!empty($error))?
          "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">List Used E-Pin</h4>
                <h6 class="card-subtitle">Export data to Copy,CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Created Date</th>
                        <th>E-Pin</th>
                        <th> Value</th>
                        <th>Used By</th>
                        <th>Date of Used</th>
                        <th>Remarks</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Created Date</th>
                        <th>E-Pin</th>
                        <th>E-Pin Value</th>
                        <th>Used By</th>
                        <th>Date of Used</th>
                        <th>Remarks</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i=1; foreach ($giftcards as $value) {?>
                      <tr>
                        <td><?php echo  $i; ?></td>
                        <td><?php echo date('Y-m-d',strtotime($value['created_on']))?></td>
                        <td><?php echo  $value['pin']?></td>
                        <td>Rs.<?php echo  $value['amount']?></td>
                        <td><?php echo  $value['used_by']?></td>
                        <td><?php echo  $value['used_date']?></td>
                        <td><?php echo  $value['remarks']?></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- ============================================================== -->
          <!-- End PAge Content -->
          <!-- ============================================================== -->
