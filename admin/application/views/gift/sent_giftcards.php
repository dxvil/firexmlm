

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">Sent E-Pin</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Sent E-Pin</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">
                                          <h4 class="card-title">List Sent E-Pin</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                      <tr>
                                                          <th>#</th>
                                                          <th>Transfer Date</th>
                                                          <th>E-Pin</th>
                                                          <th>E-Pin Value</th>
                                                          <th>Transfer to ID</th>
                                                          <th>Transfer to Name</th>
                                                          <th>PIN Status</th>
                                                          <th>Used By ID</th>
                                                          <th>Used By Name</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Transfer Date</th>
                                                        <th>E-Pin</th>
                                                        <th>E-Pin Value</th>
                                                        <th>Transfer to ID</th>
                                                        <th>Transfer to Name</th>
                                                        <th>PIN Status</th>
                                                        <th>Used By ID</th>
                                                        <th>Used By Name</th>
                                                    </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        <?php $i=1; foreach ($giftcards as $value) {?>
                                                      <tr>
                                                         <td><?php echo  $i; ?></td>
                                                          <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                                                          <td><?php echo  $value['pin']?></td>
                                                          <td>$<?php echo  $value['amount']?></td>
                                                          <td><?php echo  $value['sent_to']?></td>
                                                          <td><?php echo  $value['sent_name']?></td>
                                                          <td><?php echo  $value['remarks']?></td>
                                                          <td><?php echo  $value['used_date']?></td>
                                                          <td><?php echo  $value['remarks']?></td>

                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
