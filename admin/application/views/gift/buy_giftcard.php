<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Buy E-Pin</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Buy E-Pin</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Buy Gift Card</h4> -->
                                <!-- <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/gift/buy" method="post" id="form">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
        <label for="exampleInputuname">Select Package</label>

            <select name="package" id="package" class="form-control"  required onchange="getpackage()">
              <option selected="selected" value="">Select Package</option>
                <option value="1">Intas Bronze(1000/-)</option>
                                   <option value="2">Intas Silver(2000/-)</option>
                                   <option value="3">Intas Gold(5000/-)</option>
                                   <option value="4">Intas Platinum(8000/-)</option>
                                   <option value="5">Intas Diamond(12000/-)</option>
                                   <option value="6">Intas Kohinoor(15000/-)</option>
                                   <option value="7">Intas High 10PV(9999/-)</option>
                                   <option value="8">Intas High 20PV(19999/-)</option>
             
            </select>

        </div>
                                    </div>

                                    <div class="col-md-4">
                              <div class="form-group">
                              <label for="exampleInputuname">Gift Value</label>

                                  <input name="gift_value" id="gift_value" class="form-control" value="0.00" readonly>


                          </div>
                          </div>

                          <div class="col-md-4">
                    <div class="form-group">
                    <label for="exampleInputuname">
No. of EPIN</label>

                        <input name="pin" id="pin" class="form-control" value=""  placeholder="Enter no of PIN" required>


                </div>
                </div>
                                  </div>

                                  <div class="row">
                                   <!--  <div class="col-md-4">
                                      <div class="form-group">
                                      <label for="exampleInputuname">Select Payment Option(s)</label>
                                          <select name="wallet" id="wallet" class="form-control" onchange="getwallet()">
                                            <option selected="selected" value="0">Select Wallet</option>
                                            <option value="1">BitCoin Payment</option>
                                            <option value="2">Cash Balance</option>
                                            <option value="3">I-Wallet</option>
                                          </select> -->
                                      <!--<div class="demo-checkbox">
<input type="checkbox" id="bitcoin" name="bitcoin" value="1" class="filled-in chk-col-red" ><label for="bitcoin">Paytm </label>
<input type="checkbox" id="cash" name="cash" value="1" class="filled-in chk-col-pink" onchange="getwallet(2,this.id)" ><label for="cash">Cash Balance</label>
<input type="checkbox" id="iwallet" name="iwallet" value="1" class="filled-in chk-col-purple" onchange="getwallet(3,this.id)"><label for="iwallet">I-Wallet</label
  </div>
                                      </div>
                                                </div>-->

                        <div class="col-md-3">
                            <div class="form-group">
                            <label for="exampleInputuname">Payment Method</label>
                            <br>
      <input type="radio" name="gender" value="male" checked   style="position: unset;
    left: -9999px;
    opacity: unset;"> E Wallet<br>
  <input type="radio" name="gender" value="female" style="position: unset;
    left: -9999px;
    opacity: unset;"> Fund Wallet<br>
                            </div>
                        </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                            <label for="exampleInputuname">Cash Wallet Balance</label>
      <input name="wallet_balance" id="wallet_balance" class="form-control" value="<?php echo $wallet[0]['wallet']?>" readonly>
                                                    </div>
                                                  </div>
                                                  
                                                  <div class="col-md-3 " >
                                                    <div class="form-group">
                            <label for="exampleInputuname">Fund Wallet Balance</label>
      <input name="fundwallet_balance" id="fundwallet_balance" class="form-control" value="<?php echo $fundwallet[0]['fundwallet']?>" readonly>
                                                    </div>
                                                  </div>
                                                  
                                                  <div class="col-md-3">
                                                    <div class="form-group">
                            <label for="exampleInputuname">Remarks</label>
      <textarea name="remarks" id="remarks" class="form-control"  placeholder="Remarks"></textarea>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  
                                            </div>

                                    <button type="submit" onsubmit="check()" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<script>

$("#form").submit(function(){
    
    var pin=$('#pin').val();
    var wallet_balance=$('#wallet_balance').val();
    if((pin*200)>wallet_balance)
    {
        $('#paytm').show();
        return false;
    }
});

        function getpackage()
        {
      var package=  $('#package').val();

          var url = '<?php echo base_url(); ?>index.php/gift/getpackage';
          var data = {
          package : package,

          }
          $.post(url, data).done(function(msg){
      $('#gift_value').val(msg);
        });

        }

        function getwallet(wallet,id)
        {
          // var wallet=  $('#wallet').val();
          var isChecked = $('#'+id+':checked').val()?true:false;


if(isChecked){
  var url = '<?php echo base_url(); ?>index.php/gift/getwallet';
  var data = {
    wallet : wallet,
  }
  $.post(url, data).done(function(msg){
if(wallet==2)
{
    $('#wallet_balance').val(msg);
}
if(wallet==3)
{
  $('#iwallet_balance').val(msg);
}
});
}
else{
  if(wallet==2)
  {
      $('#wallet_balance').val('');
  }
  if(wallet==3)
  {
    $('#iwallet_balance').val('');
  }
}

        }


        </script>
