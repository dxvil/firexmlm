<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">View Member Details</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">View Member Details</li>
            </ol>
          </div>
          <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Member Id</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['username'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Name</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['name'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['email'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6"> <strong>Contact Number</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['contact_number'];?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Sponser Id</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['username'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Presenter Id</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['name'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Country</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['country'];?></p>
                  </div>
                  <div class="col-md-3 col-xs-6"> <strong>BitCoin Account</strong><br>
                    <p class="text-muted"><?php echo $memberdetails[0]['bitcoin_address'];?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
