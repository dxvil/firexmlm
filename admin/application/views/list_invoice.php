

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">List Invoice</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">List Invoice</li>
                                    </ol>
                                </div>
                                <div>
                                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->

                                <?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">

                                          <h4 class="card-title">List Product</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>User Name</th>
                                                      <th>Invoice Id</th>
                                                      <th>Billing Date</th>
                                                      <th>Sub Total</th>
                                                      <th>Tax</th>
                                                      <th>Grand Total</th>
                                                      <th>Payment Mode</th>
                                                      <th>Action</th>
                                                    </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>User Name</th>
                                                        <th>Invoice Id</th>
                                                        <th>Billing Date</th>
                                                        <th>Sub Total</th>
                                                        <th>Tax</th>
                                                        <th>Grand Total</th>
                                                        <th>Payment Mode</th>
                                                        <th>Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        <?php

                                                        $view_invoice=base_url().'index.php/product/view_invoice';

                                                         $i=1; foreach ($listinvoice as $value) {?>
                                                      <tr>
                                                          <td><?php echo  $i; ?></td>
                                                          <td><?php echo  $value['username']?></td>
                                                          <td>INV00<?php echo  $value['id']?></td>
                                                          <td><?php echo  $value['billing_date']?></td>
                                                          <td>&#8377;<?php echo  $value['sub_total']?></td>
                                                          <td>&#8377;<?php echo  $value['tax_amount']?></td>
                                                          <td>&#8377;<?php echo  $value['grand_total']?></td>
                                                          <td><?php echo  $value['payment_mode']?></td>

                                                          <td>

                                                        <button  title="View Invoice"  type="button"
                                                       onclick="action('<?php echo $view_invoice;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-file"></i> </button>
</td>
                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
                                <script type='text/javascript'>

                                function action(url,id)
                                {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();

                                }

                                function confirmaction(url,id,msg)
                                {
                                 var strconfirm = confirm(msg);
                                 if (strconfirm == true)
                                            {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();
                                 }
                                }
                                </script>
