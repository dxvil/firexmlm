<style type="text/css">
@media print
{

  @page { size: auto;  margin: 0mm; }
  body{
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
    .maincontainer{
       width:100%!important;
       /*margin:0 auto!important;*/
       /*background-color:#ccc;*/
       /*padding: 15px!important;*/
    }
    .invoicecontainer{
      margin-top:-100px!important;
      background-color:#fff!important;
      /*padding: 10px!important;*/
    }

    .newclass{
      /*margin-top:0px!important;*/
      background-color:#fff!important;
    }

    .header{
      /*display: flex!important;*/
      /*margin-top: 15px!important;*/
    }
    .header .logo{
      width: 20%!important;
      /*margin: 0!important;*/
      display: inline-block!important;
      margin-left: 15px!important;
    }
    .header .logo figure{
      /*margin: 0!important;*/
      display: block!important;
    }
    .header .address{
      width: 40%!important;
      display: inline-block!important;
      text-align: left!important;
    }
    .header .address address{
      font-size: 11px!important;
      text-align: left!important;
      margin-bottom: 6px!important;
    }
    .header .address h3{
      font-size: 15px!important;
      margin: 0!important;
      font-weight: 700!important;
      margin-bottom: 15px!important;
      color: #353535!important;
      text-transform: uppercase!important;
      text-align: left!important;
    }
    .header .billto{
      width: 40%!important;
      display: inline-block!important;
      text-align: right!important;
    }
    .header .billto h3{
      font-size: 15px!important;
      margin: 0!important;
      font-weight: 700!important;
      margin-bottom: 15px!important;
      color: #353535!important;
      text-transform: uppercase!important;
      text-align: right!important;
    }
    .header .billto address{
      font-size: 11px!important;
      text-align: right!important;
      width:90%!important;
    }

    .main-body{
      margin-top: -48px!important;
      margin-bottom: 30px!important;
    }

    .border-table{
      /*width: 80%;*/
      margin: 0 auto!important;
      display: block!important;
      text-align: center!important;
      margin-bottom: 10px!important;
    }
    .border-table table{
      text-align: center!important;
      /*border: 1px solid #a0c9ce;*/
      border-collapse: collapse!important;
      width: 100%!important;
    }
    .border-table table tr:nth-child(even){
      background: #e6e4e4!important;
    }
    .border-table table tr:nth-child(odd){
      background: #fff!important;
    }
    .border-table table thead tr th{
      font-size: 13px!important;
      font-weight: 600!important;
      text-transform: capitalize!important;
      color: #fff!important;
      padding: 6px 20px!important;
      border-left: 1px solid #fff!important;
      border-collapse: collapse!important;
      background:<?php echo $invoice_color;?>!important;
      text-align: center!important;
    }
    .border-table table tbody tr td{
      font-size: 12px!important;
      font-weight: 500!important;
      text-transform: capitalize!important;
      color: #000!important;
      padding: 6px 20px!important;
      /*border-bottom: 1px solid #000;*/
      border-collapse: collapse!important;
    }

    .footer-div .tarms{
      width: 65%!important;
      display: inline-block!important;
      vertical-align: top!important;
    }
    .footer-div .tarms h3{
      font-size: 18px!important;
      font-weight: 600!important;
      text-transform: capitalize!important;
      color: #000!important;
      margin: 0!important;
      margin-bottom: 15px!important;
    }
    .footer-div .tarms ul{
      padding: 0!important;
      margin: 0!important;
    }
    .footer-div .tarms ul li{
      list-style-type: none!important;
      font-weight: 500!important;
      color: #000!important;
      font-size: 12px!important;
    }
    .footer-div .tarms ul li span{
      font-size: 12px!important;
      font-weight: 400!important;
      color: #000!important;
    }
    .footer-div .total{
  width: 34%!important;
  display: inline-block!important;
  vertical-align: top!important;
  }
  .footer-div .total table{
  padding: 0!important;
  margin: 0!important;
  text-align: right!important;
  width: 100%!important;
  background: #fff!important;
  }
  .footer-div .total table tr{
  line-height: 2!important;
  }
  .footer-div .total table tr td{
  padding: 0!important;
  margin: 0!important;
  list-style-type: none!important;
  font-size: 15px!important;
  font-weight: 600!important;
  color:#353535!important;
  text-transform: capitalize!important;
  vertical-align: top!important;
  }
  .footer-div .total table tr:last-child td{
  /* border-bottom: 1px solid #d2d1d1!important;
  border-top: 1px solid #d2d1d1!important;
  padding: 7px 0!important; */
  }
  .footer-div .total table tr td:last-child{
  font-size: 14px!important;
  font-weight: 400!important;
  }


  .invoicedetails p{
  font-weight: 600!important;
  /*line-height: 9px;*/
  }

.tarms p{
  font-size: 10px!important;
  color:#696c74!important;
}

    .dotted{border-bottom:1px dotted!important;}
body * { visibility: hidden; }
#printble * { visibility: visible; }
}

</style>

<style>
  body{
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
    .maincontainer{
       width:80%;
       margin:0 auto;
       /*background-color:#ccc;*/
       padding: 15px;

    }
    .invoicecontainer{
      margin:15px;
      background-color:#fff;
      padding: 10px;
    }
    .newclass{
      margin:15px;
      background-color:#fff;
      padding: 10px;
    }
    .header{
      display: flex;
      margin-top: 15px;
    }
    .header .logo{
      width: 20%;
      margin: 0;
      display: inline-block;
      margin-left: 15px;
    }
    .header .logo figure{
      margin: 0;
      display: block;
    }
    .header .address{
      width: 40%;
      display: inline-block;
      text-align: left;
    }
    .header .address address{
      font-size: 11px;
      text-align: left;
      margin-bottom: 6px;
    }
    .header .address h3{
      font-size: 15px;
      margin: 0;
      font-weight: 700;
      margin-bottom: 15px;
      color: #353535;
      text-transform: uppercase;
      text-align: left;
    }
    .header .billto{
      width: 40%;
      display: inline-block;
      text-align: right;
    }
    .header .billto h3{
      font-size: 15px;
      margin: 0;
      font-weight: 700;
      margin-bottom: 15px;
      color: #353535;
      text-transform: uppercase;
      text-align: right;
    }
    .header .billto address{
      font-size: 11px;
      text-align: right;
      width:90%;
    }

    .main-body{
      margin-top: -48px;
      margin-bottom: 30px;
    }

    .border-table{
      /*width: 80%;*/
      margin: 0 auto;
      display: block;
      text-align: center;
      margin-bottom: 10px;
    }
    .border-table table{
      text-align: center;
      /*border: 1px solid #a0c9ce;*/
      border-collapse: collapse;
      width: 100%;
    }
    .border-table table tr:nth-child(even){
      background: #e6e4e4;
    }
    .border-table table tr:nth-child(odd){
      background: #fff;
    }
    .border-table table thead tr th{
      font-size: 13px;
      font-weight: 600;
      text-transform: capitalize;
      color: #fff;
      padding: 6px 20px;
      border-left: 1px solid #fff;
      border-collapse: collapse;
      background:<?php echo $invoice_color;?>;
      text-align: center;
    }
    .border-table table tbody tr td{
      font-size: 12px;
      font-weight: 500;
      text-transform: capitalize;
      color: #000;
      padding: 6px 20px;
      /*border-bottom: 1px solid #000;*/
      border-collapse: collapse;
    }

    .footer-div .tarms{
      width: 65%;
      display: inline-block;
      vertical-align: top;
    }
    .footer-div .tarms h3{
      font-size: 18px;
      font-weight: 600;
      text-transform: capitalize;
      color: #000;
      margin: 0;
      margin-bottom: 15px;
    }
    .footer-div .tarms ul{
      padding: 0;
      margin: 0;
    }
    .footer-div .tarms ul li{
      list-style-type: none;
      font-weight: 500;
      color: #000;
      font-size: 12px;
    }
    .footer-div .tarms ul li span{
      font-size: 12px;
      font-weight: 400;
      color: #000;
    }
    .footer-div .total{
width: 34%;
display: inline-block;
vertical-align: top;
}
.footer-div .total table{
padding: 0;
margin: 0;
text-align: right;
width: 100%;
background: #fff;
}
.footer-div .total table tr{
line-height: 2;
}
.footer-div .total table tr td{
padding: 0;
margin: 0;
list-style-type: none;
font-size: 15px;
font-weight: 600;
color:#353535;
text-transform: capitalize;
vertical-align: top;
}

.installment{
width: 59%;
display: inline-block;
vertical-align: top;
}
.installment table tr td{
padding-right: 20px;
padding-bottom:  8px;
}



.footer-div .total table tr:last-child td{
/* border-bottom: 1px solid #d2d1d1;
border-top: 1px solid #d2d1d1;
padding: 7px 0; */
}
.footer-div .total table tr td:last-child{
font-size: 14px;
font-weight: 400;
}


.invoicedetails p{
font-weight: 600;
  /*line-height: 9px;*/
}
.dotted{border-bottom:2px dotted;}
  </style>

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">Invoice</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Invoice</li>
                                    </ol>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
                                <div class="col-lg-12">
      <div class="row" id="printble">
        <div class="col-md-12">


<div class="maincontainer" style="background-color:#fff;" >
   <div class="invoicecontainer">
     <div class="header">

       <div class="address">
         <h3><?php echo $org_name?></h3>

         <address>
<?php echo $address?><br>
     Contact:<?php echo $contact?><br>
       <div><a href="mailto:<?php echo $email?>"><?php echo $email?></a></div>
   </address>
<div class="invoicedetails">
<p>Invoice no -
       <span>INV00<?php echo $invoice[0]['id']; ?></span>
     </p>
     <p>Invoice date -
       <span>
         <?php
         $old_date = $invoice[0]['billing_date'];
         $new_date = date("d-m-Y", strtotime($old_date));
         echo $new_date;
         ?>
       </span>
     </p>
</div>
       </div>
       <div class="logo">
         <figure>
           <img src="<?php echo $org_logo?>" alt="" style="width:85%">
         </figure>
       </div>
       <div class="billto" >
         <div class="bill" align="right">
           <h3>
             bill to: <?php echo $member_details[0]['name']; ?>
           </h3>
           <!-- <p style="color: #353535;" ><b>customer name</b></p> -->
           <address >
             <?php echo $member_details[0]['address'] ?><br>
             Contact:<?php echo $member_details[0]['contact_number'] ?><br>
             <div class="email"><a href="mailto:<?php echo $member_details[0]['email'] ?>"><?php echo $member_details[0]['email'] ?></a></div>
           </address>
         </div>
       </div>
     </div>
     <!-- <div class="main-body">
       <p class="subject" >invoice</p>
     </div> -->
     <div class="border-table">
       <table>
         <thead>
           <tr>
             <th>#</th>
             <th>packages</th>
             <th>price</th>
             <th>quantity</th>
             <th>Tax</th>
             <th>Tax Amount </th>
             <th>total</th>
           </tr>
         </thead>
         <tbody>
           <?php
           $id = 1;
            foreach ($invoice_details as $key => $val):
          ?>
           <tr>
             <td><?php echo $id; ?></td>
             <td><?php echo $val['product_name']; ?></td>
             <td>&#8377;<?php echo $val['price']; ?></td>
             <td><?php echo $val['quantity']; ?></td>
             <td><?php echo $val['tax']; ?></td>
             <td>&#8377;<?php echo (!empty($val['tax_amount'])) ?$val['tax_amount'] : 0 ; ?>/- </td>
             <td>&#8377;<?php echo (($val['price']*$val['quantity'])+$val['tax_amount']) ?></td>
           </tr>
         <?php
          $id++;
          endforeach;
         ?>
         </tbody>
       </table>
     </div>
     <div class="footer-div">
       <div class="installment">
         <div>

           <p><b>Terms & Condition:</b>
           <br>1. All payments are non-refundable and Prices include all applicable taxes.
           <br>2. Do not accept invoices with handwritten notes.
           <br>3. Do not accept invoices without Staff signature and Official stamp.
           </p>

         </div>
       </div>
       <div class="total">
           <table>
             <tr>
               <td>Sub Total</td>
               <td>
                 &#8377;<?php echo (!empty($invoice[0]['sub_total'])) ? $invoice[0]['sub_total'] : 0 ; ?>/-
               </td>
             </tr>
             <tr>
               <td>Tax</td>
               <td>&#8377;<?php echo (!empty($invoice[0]['tax_amount'])) ?$invoice[0]['tax_amount'] : 0 ; ?>/-</td>
             </tr>
               <tr>
                 <td>Grand Total</td>
                 <td>&#8377;<?php echo (!empty($invoice[0]['grand_total'])) ? $invoice[0]['grand_total'] : 0; ?>/-</td>
               </tr>
               <tr>
                 <td>Payment Mode</td>
                 <td><?php echo $invoice[0]['payment_mode']; ?></td>
               </tr>
           </table>
         </div>
     </div>
  </div>
</div>

</div>
</div>

</div>
<button class="btn btn-primary pull-right"  onclick="window.print();" style="margin-right: 158px;
    margin-bottom: 10px;" ><i class="fa fa-print"></i></button>
</div>
</div>
