<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Activate Your Membership</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">Buy Package</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Buy Package</h4>
                                <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6>
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/package/activate" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
    <div class="row">

        <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Member Id</label>
                              <!--<select id="member_id" name="member_id" class="form-control">-->

                              <!--    <option value="" selected >Select Member</option>-->
                              <!--    <?php foreach($memberlist as $row){?>-->
                              <!--     <option value="<?php echo $row['member_id']?>"><?php echo $row['username']?></option>-->
                              <!--     <?php }?>-->
                              <!--     </select>-->

                               <input type="text" class="form-control" id="member_id" name="member_id" placeholder=""  value="<?php echo $this->session->userdata('username')?>" >


                            </div>


                                    <div class="form-group">
                                        <label for="exampleInputuname">Member Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="member_name" name="member_name" placeholder=""  readonly value="<?php echo $this->session->userdata('name')?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Current Package</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="current_package" name="current_package" placeholder="" value="<?php echo $package_price;?>" readonly>
                                        </div>
                                    </div>

                            <div class="form-group">
                              <label for="exampleInputuname">Package</label>

                              <select id="package_id" name="package_id" class="form-control">
                                   <option value="17">BASIC PAKAGE ($25)</option>
                                   <option value="16">BASIC PAKAGE ($50)</option>
                                   <option value="1">BASIC PAKAGE ($100)</option>
                                   <option value="2">BASIC INVST PKG. ($100)</option>
                                   <option value="3">ADVANCE PKG  ($200)</option>
                                   <option value="4">ADVANCE INVST PKG ($200)</option>
                                   <option value="5">ADV ELITE EDU PKG ($300)</option>
                                   <option value="6">SMART INVST PKG ($300)</option>
                                   <option value="7">EXCELLENT EDU PKG ($500)</option>
                                   <option value="8">ELITE INVEST PKG ($500)</option>
                                   <option value="9">PREMIUN INVST PKG ($1000)</option>
                                   <option value="10">SUPER INVST PKG ($2000)</option>
                                   <option value="11">MEGA INVST PKG  ($3000)</option>
                                   <option value="12">ROYAL INVST PKG ($5000)</option>
                                   <option value="13">CROWN INVST PKG ($10000)</option>
                                   <option value="14">BASIC EXTRA PKG($100)</option>
                                   <option value="15">ADVANCED PKG($200)</option>
                                  
                                   
                                   
                               </select>
                            </div>
                                    <!--<div class="form-group">-->
                                        <!--<label for="exampleInputuname">E-Pin</label>-->

                                        <!--<select name="pin_id" id="pin_id" class="select2 form-control custom-select" style="width: 100%; height:36px;"  required>-->
                                          <!--<option selected="selected" value="">Select E-Pin</option>-->
                                          <?php //foreach ($epin as $key => $value) {
                                          ?>
                                          <!--<option value="<?php echo $value['id']?>"><?php echo $value['pin']?></option>-->
                                        <?php // }?>
                                        <!--</select>-->

                                                        <!--</div>-->

                                   </div>

                                   <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Payment Method</label>
                              <br>
      <input type="radio" name="payment" value="e_wallet" checked   style="position: unset;
    left: -9999px;
    opacity: unset;"> E Wallet<br>
  <input type="radio" name="payment" value="fund_wallet" style="position: unset;
    left: -9999px;
    opacity: unset;"> Fund Wallet<br>
                            </div>


                                    <div class="form-group">
                            <label for="exampleInputuname">Cash Wallet Balance</label>
      <input name="wallet_balance" id="wallet_balance" class="form-control" value="<?php echo $wallet[0]['wallet']?>" readonly>
                                                    </div>

                                    <div class="form-group">
      <label for="exampleInputuname">Fund Wallet Balance</label>
      <input name="fundwallet_balance" id="fundwallet_balance" class="form-control" value="<?php echo $fundwallet[0]['fundwallet']?>" readonly>
                                    </div>


                                   </div>
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

$( "#member_id" ).change(function() {
  var url = '<?php echo base_url(); ?>index.php/package/get_current_package';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
        $('#current_package').val(msg);
        });


        var url = '<?php echo base_url(); ?>index.php/package/get_member_name';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });
});

</script>
