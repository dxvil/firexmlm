

<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Coin History</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Coin History</li>
                </ol>
              </div>
            </div>
            <div class="container-fluid">
              <div class="col-sm-12">
                <?php
                $error=$this->session->flashdata('error_login');
                echo (!empty($error))?
                "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                <div class="card">
                  <div class="card-header">
                    <h5>List Coin History</h5>
                  </div>
                  <div class="card-block">
                    <div class="table-responsive">

                    <div class="row">
                      <div class="col-md-12">
                        <span class="text-danger" style="font-weight:600;">  Total Released Coins  :
                        <?php

                            $query = $this->db->query("SELECT SUM(a.coins) as t_coin  FROM tbl_release_wallet as a left join tbl_login as b on b.member_id = a.member_id WHERE b.status = 0");

                            if ($query->num_rows() > 0) {

                                $with_amount = $query->row()->t_coin;
                                echo '<span style="font-size:20px;">'. $with_amount.'</span>';
                                
                            }

                            ?>
                          
                        </span>

                        <br/>

                        <span class="text-danger" style="font-weight:600;">  Total Issued Coins  :
                        <?php

                                $query2 = $this->db->query("SELECT SUM(a.coins) as scoin  FROM tbl_coin as a left join tbl_login as b on b.member_id = a.member_id WHERE b.status = 0");

                                if ($query2->num_rows() > 0) {

                                    $s_amount = $query2->row()->scoin;
                                    echo '<span style="font-size:20px;">'. $s_amount.'</span>';
                                    
                                }

                                ?>

                        </span>

                        <br/>

                        <span class="text-danger" style="font-weight:600;">  Total Withdrawl Coins  :
                        <?php

                            $query3 = $this->db->query("SELECT SUM(withdraw_amount) as wcoin  FROM tbl_withdrawcoin where withdraw_status = 1");

                            if ($query3->num_rows() > 0) {

                                $w_amount = $query3->row()->wcoin;
                                echo '<span style="font-size:20px;">'. $w_amount.'</span>';
                                
                            }

                            ?>

                            </span>

                      </div>
                    </div>

                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Release Coins</th>
                            <th>Total Coin</th>
                            <th>Withdraled Coin</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;
                          foreach ($withdraw_list as $value) {?>

                          <tr>
                            <td><?php echo  $i; ?></td>
                            <td><?php echo $value['username']; ?></td>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo  round($value['coins'],0)?></td>
                            <td><?php echo  round($value['total_coin'],0)?></td>
                            <?php

                                $member_id = $value['member_id'];
                                $query = $this->db->query("SELECT SUM(withdraw_amount) as withdrwaled_coin FROM tbl_withdrawcoin WHERE member_id = '$member_id' and  withdraw_status = 1");
                                if ($query->num_rows() > 0) {

                                    $with_amount = $query->row()->withdrwaled_coin;
                                    if ($with_amount == '0.00'){
                                      echo '<td>0.00</td>';
                                    }
                                    else {
                                      echo '<td>'. $with_amount .'</td>';
                                    }

                                }
                                else {
                                  echo '<td>0.00</td>';
                                }

                            ?>
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                            <th>#</th>
                            <th>User Id</th>
                             <th>Name</th>
                            <th>Coins</th>
                            <th>Date</th>
                        </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
    </div>

    <script type='text/javascript'>
      function action(url,id){
        var form = document.createElement("form");
        element1 = document.createElement("input");
        form.action = url;
        form.method = "post";
        element1.name = "id";
        element1.value = id;
        form.appendChild(element1);
        document.body.appendChild(form);
        form.submit();
      }
      function confirmaction(url,id,msg){
        var strconfirm = confirm(msg);
        if (strconfirm == true){
          var form = document.createElement("form");
          element1 = document.createElement("input");
          form.action = url;
          form.method = "post";
          element1.name = "id";
          element1.value = id;
          form.appendChild(element1);
          document.body.appendChild(form);
          form.submit();
        }
      }
    </script>
