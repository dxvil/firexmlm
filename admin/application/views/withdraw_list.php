<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">List Withdraw</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">List Withdraw</li>
                </ol>
              </div>
            </div>
            <div class="container-fluid">
              <div class="col-sm-12">
                <?php
                $error=$this->session->flashdata('error_login');
                echo (!empty($error))?
                "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                <div class="card">
                  <div class="card-header">
                    <h5>List Wallet Report</h5>
                  </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Withdraw Amount</th>
                            <th>Withdraw Status</th>
                            <th>Request Date</th>
                            <th>User Id</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Bank Name</th>
                            <th>Branch</th>
                            <th>Account Holder Name</th>
                            <th>Account Number</th>
                            <th>IFSC </th>
                            <th>PayTm </th>
                            <th>WEC Address </th>
                            <th>Pan Card</th>
                            <th>Admin Charges</th>
                          
                            <th>Net Amount </th>
                            <th>Equivalent Coin </th>
                            <?php $userType=$this->session->userdata('usertype');?>
                            <?php if($userType==1){?><th>Action</th><?php } ?>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;
                          $approve_url=base_url()."index.php/home/w_activate";
                          $decline_url=base_url()."index.php/home/w_decline";
                          foreach ($withdraw_list as $value) {?>
                          <tr>
                            <td><?php echo  $i; ?></td>
                            <td><?php echo  $value['withdraw_amount']?></td>
                            <td><?php if($value['withdraw_status']==0)
                              echo 'Pending';
                            elseif($value['withdraw_status']==1)
                              echo 'Approved';
                            
                            elseif($value['withdraw_status']==2)
                                echo 'Declined';
                            
                            ?></td>
                            <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                            <td><?php echo  $value['username']?></td>
                            <td><?php echo  $value['name']?></td>
                            <td><?php echo  $value['contact_number']?></td>
                            <td><?php echo  $value['bank_name']?></td>
                            <td><?php echo  $value['branch']?></td>
                            <td><?php echo  $value['account_holder_name']?></td>
                            <td><?php echo  $value['account_number']?></td>
                            <td><?php echo  $value['ifsc_code']?></td>
                            <td><?php echo  $value['paytm']?></td>
                             <td><?php echo  $value['wec_wallet']?></td>
                            <td><?php echo  $value['pan_no']?></td>
                            <td><?php
                              echo  (($value['withdraw_amount'])*10)/100;
                            ?></td>
                           
                           
                            <td>$<?php
                            
                              echo  (($value['withdraw_amount'])*90)/100;
                            ?></td>
                            
                            <td><?php
                            
                              echo  (((($value['withdraw_amount'])*90)/100)*70)/$value['coin_price'];
                            ?></td>
                            
                            
                            <?php if($userType==1){?>  <td>
                            <?php if( $value['withdraw_status']==0){?>
                            <button  title="Approve" type="button"
                            onclick="confirmaction('<?php echo $approve_url;?>',<?php echo $value['id']; ?>,'Are you sure you want to Approve?')" class="btn btn-success btn-circle"><i class="fa fa-check"></i> </button>
                            <button  title="Approve" type="button"
                            onclick="confirmaction('<?php echo $decline_url;?>',<?php echo $value['id']; ?>,'Are you sure you want to Decline?')" class="btn btn-danger btn-circle"><i class="fa fa-check"></i> </button>
                          </td><?php } ?>
                        <?php } ?>
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Withdraw Amount</th>
                          <th>Withdraw Status</th>
                          <th>Request Date</th>
                          <th>User Id</th>
                          <th>Name</th>
                          <th>Contact</th>
                          <th>Bank Name</th>
                          <th>Branch</th>
                          <th>Account Holder Name</th>
                          <th>Account Number</th>
                          <th>IFSC </th>
                          <th>WEC Address </th>
                          <th>PayTm </th>
                          <th>Pan Card</th>
                          <th>Admin Charges</th>
                          <!--<th>TDS</th>-->
                          <!--<th>Club</th>-->
                          <th>Net Amount </th>
                          <th>Equivalent Coin </th>
                          
                          <?php if($userType==1){?>  <th>Action</th><?php } ?>
                        </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
    </div>

    <script type='text/javascript'>
      function action(url,id){
        var form = document.createElement("form");
        element1 = document.createElement("input");
        form.action = url;
        form.method = "post";
        element1.name = "id";
        element1.value = id;
        form.appendChild(element1);
        document.body.appendChild(form);
        form.submit();
      }
      function confirmaction(url,id,msg){
        var strconfirm = confirm(msg);
        if (strconfirm == true){
          var form = document.createElement("form");
          element1 = document.createElement("input");
          form.action = url;
          form.method = "post";
          element1.name = "id";
          element1.value = id;
          form.appendChild(element1);
          document.body.appendChild(form);
          form.submit();
        }
      }
    </script>
