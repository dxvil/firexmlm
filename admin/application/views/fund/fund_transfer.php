<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Transfer Fund to Member</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">Fund Transfer</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Fund Transfer</h4>
                                <h6 class="card-subtitle">Transfer Fund to Member</h6>
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/fund/transfer" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
    <div class="row">

        <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Member Id</label>
                              <!--<select id="member_id" name="member_id" class="form-control">-->

                              <!--    <option value="" selected >Select Member</option>-->
                              <!--    <?php foreach($memberlist as $row){?>-->
                              <!--     <option value="<?php echo $row['member_id']?>"><?php echo $row['username']?></option>-->
                              <!--     <?php }?>-->
                              <!--     </select>-->

                               <input type="text" class="form-control" id="member_id" name="member_id" placeholder="Enter Member Id"    required>


                            </div>


                                    <div class="form-group">
                                        <label for="exampleInputuname">Member Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="member_name" name="member_name" placeholder="Member Name"  readonly>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="exampleInputuname">Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Enter Amount to be send"  >
                                        </div>
                                    </div>

                                   </div>

                                   <div class="col-md-6">
    
<div class="form-group">
                                        <label for="exampleInputuname">Payment Method</label>
                              <br>
      <input type="radio" name="payment" value="e_wallet" checked   style="position: unset;
    left: -9999px;
    opacity: unset;"> E Wallet<br>
  <input type="radio" name="payment" value="fund_wallet" style="position: unset;
    left: -9999px;
    opacity: unset;"> Fund Wallet<br>
                            </div>

                                    <div class="form-group">
                            <label for="exampleInputuname">Cash Wallet Balance</label>
      <input name="wallet_balance" id="wallet_balance" class="form-control" value="<?php echo $wallet[0]['wallet']?>" readonly>
                                                    </div>

                                    <div class="form-group">
      <label for="exampleInputuname">Fund Wallet Balance</label>
      <input name="fundwallet_balance" id="fundwallet_balance" class="form-control" value="<?php echo $fundwallet[0]['fundwallet']?>" readonly>
                                    </div>


                                   </div>
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

$( "#member_id" ).change(function() {
  var url = '<?php echo base_url(); ?>index.php/package/get_current_package';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
        $('#current_package').val(msg);
        });


        var url = '<?php echo base_url(); ?>index.php/package/get_member_name';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });
});

</script>
