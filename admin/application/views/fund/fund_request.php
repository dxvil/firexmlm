<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Fund Request</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Fund Request</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Fund Request</h4>
                                <!--<h6 class="card-subtitle">Activate Your Membership by joining with this package</h6>-->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/fund/save_slip" method="post" accept-charset="utf-8" enctype="multipart/form-data">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

    <div class="row">
        <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="exampleInputuname">Amount</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-money"></i></div>-->
                                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Transaction Id</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-money"></i></div>-->
                                            <input type="text" class="form-control" id="transaction_id" name="transaction_id" placeholder="Transaction Id" value="" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Bank</label>

                                        <select name="bank_id" id="bank_id" class="select2 form-control custom-select" style="width: 100%; height:36px;"  required>
                                          <option selected="selected" value="">Select Bank</option>

                                          <option value="1">Axis Bank(918020100874558)</option>

                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Time</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-money"></i></div>-->
                                            <input type="time" class="form-control" id="time" name="time" placeholder="Time" value="" required>
                                        </div>
                                    </div>

        </div>

        <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="exampleInputuname">Contact Number</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-user"></i></div>-->
                                            <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Number" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Upload Slip</label>

                                       <input
                                            type="file"
                                            name="userfile"
                                            class="form-control form-control-line"
                                          required
                                            id="userfile" required
                                          >

                                                        </div>

                                                         <div class="form-group">
                                        <label for="exampleInputuname">Date</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-money"></i></div>-->
                                            <input type="date" class="form-control" id="date" name="date" placeholder="Date" value="" required>
                                        </div>
                                    </div>

                                                        <div class="form-group">
                                        <label for="exampleInputuname">Message</label>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon"><i class="ti-user"></i></div>-->
                                            <textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>

        </div>
    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
