<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">List Fund</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">List Fund</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <?php
          $error=$this->session->flashdata('success');
          echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
              <div class="card-body">
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Member Id</th>
                        <th>Amount</th>
                        <th>Contact Number</th>
                        <th>Transaction Id</th>
                        <th>Bank Slip</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Status</th>
                        <?php  $usertype=$this->session->userdata('usertype'); if($usertype==1){ ?>
                        <th>Action</th>
                        <?php  } ?>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Member Id</th>
                        <th>Amount</th>
                        <th>Contact Number</th>
                        <th>Transaction Id</th>
                        <th>Bank Slip</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Status</th>
                        <?php  $usertype=$this->session->userdata('usertype'); if($usertype==1){ ?>
                        <th>Action</th>
                        <?php  }  ?>
                      </tr>
                      </tfoot>
                      <tbody>
                        <?php
                        $activate=base_url().'index.php/fund/accept';
                        $deactivate=base_url().'index.php/fund/decline';
                        $i=1; foreach ($listfund as $value) {?>
                        <tr>
                          <td><?php echo  $i; ?></td>
                          <td><?php echo  $value['username']?></td>
                          <td><?php echo  $value['amount']?></td>
                          <td><?php echo  $value['contact_number']?></td>
                          <td><?php echo  $value['transaction_id']?></td>
                          <td><a href="<?php echo base_url();?>uploads/bankimage/<?php echo  $value['upload_image']?>" ><img style="width:50px;" src="<?php echo base_url();?>uploads/bankimage/<?php echo  $value['upload_image']?>"></a></td>
                          <td><?php echo  $value['date']?></td>
                          <td><?php echo  $value['time']?></td>
                          <td><?php if($value['status']==0){ echo 'Pending';}
                            if($value['status']==1){ echo 'Approved';}
                            if($value['status']==2){ echo 'Declined';}
                          ?></td>
                          <td>
                            <?php  $usertype=$this->session->userdata('usertype'); if($usertype==1){ ?>
                              <?php if($value['status']==0){?>
                              <button type="button"
                              onclick="confirmaction('<?php echo $activate;?>',<?php echo $value['id']; ?>,'Are you sure you want to Process this Request?')" class="btn btn-success btn-circle"  title="Accept Request"><i class="fa fa-check"></i> </button>
                              <button type="button"
                              onclick="confirmaction('<?php echo $deactivate;?>',<?php echo $value['id']; ?>,'Are you sure you want to Decline this Request?')" class="btn btn-danger btn-circle"  title="Decline Request"><i class="fa fa-times"></i> </button>
                              <?php } ?>
                              <?php if($value['status']==1){?>
                              <button type="button"
                              class="btn btn-success "  ><i class="fa fa-check"></i>Approved </button>
                            <?php } ?>
                            <?php if($value['status']==2){?>
                            <button type="button"
                            class="btn btn-danger "  ><i class="fa fa-check"></i>Declined </button>
                            <?php } ?>
                            <?php } ?>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <script type='text/javascript'>
                function action(url,id){
                  var form = document.createElement("form");
                  element1 = document.createElement("input");
                  form.action = url;
                  form.method = "post";
                  element1.name = "id";
                  element1.value = id;
                  form.appendChild(element1);
                  document.body.appendChild(form);
                  form.submit();
                }
                function confirmaction(url,id,msg){
                  var strconfirm = confirm(msg);
                  if (strconfirm == true){
                    var form = document.createElement("form");
                    element1 = document.createElement("input");
                    form.action = url;
                    form.method = "post";
                    element1.name = "id";
                    element1.value = id;
                    form.appendChild(element1);
                    document.body.appendChild(form);
                    form.submit();
                  }
                }
              </script>
