<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Profile</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>index.php/home">Home</a></li>
                <li class="breadcrumb-item">pages</li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <?php
                  $error=$this->session->flashdata('error_login');
                  echo (!empty($error))?
                  "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
        <div class="row">
            
            
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30"> <img src="<?php echo base_url()?>uploads/userimage/<?php echo $this->session->userdata('user_image')?>" class="img-circle" width="150" />
                            <h4 class="card-title m-t-10"><?php echo $this->session->userdata('name')?></h4>
                            <h6 class="card-subtitle"><?php $usertype= $this->session->userdata('usertype');
if($usertype==1){
  echo 'Admin';
}
else if($usertype==2){
  echo 'Member';
}
  ?></h6>
                <!-- <div class="row text-center justify-content-md-center">
                                <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                            </div> -->
                        </center>
                    </div>
                    <div>
                        <hr> </div>
                    <div class="card-body"> <small class="text-muted">Email address </small>
                        <h6><?php echo $this->session->userdata('email')?></h6> <small class="text-muted p-t-30 db">Phone</small>
                        <h6><?php echo $this->session->userdata('contact_number')?></h6> <!-- <small class="text-muted p-t-30 db">Address</small>
                        <h6><?php echo $this->session->userdata('contact_number')?></h6> -->

                        <!-- <button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button>
                        <button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button>
                        <button class="btn btn-circle btn-secondary"><i class="fa fa-youtube"></i></button> -->
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Personal</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Change Password</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#upload" role="tab">Upload Image</a> </li>

                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#transactional" role="tab">Transaction Password</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!--second tab-->
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $row[0]['name']?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $row[0]['contact_number']?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $this->session->userdata('email')?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Address</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $row[0]['bitcoin_address']?></p>
                                    </div>
                                </div>
                                <hr>
                                <form class="form p-t-20" method="post" action="<?php echo base_url()."index.php/profile/update_member_data"; ?>">
                                  
                                   
                                   <h4>Personal Details</h4>
                                   <div class="row">
                                    <div class="col-md-6">
                                        
                                        
                                        
                                      <div class="form-group">
                                          <div>
                                              <?php  echo $this->session->userdata('update_member_massage'); ?>
                                              </div>
                                          <label for="exampleInputuname">Name</label>
                                          <div class="input-group">

                                              <input type="text" class="form-control" id="txtreferallink" placeholder="Name" value="<?php echo $row[0]['name']?>" name="name" >
                                          </div>
                                      </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Contact Number</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Contact Number" value="<?php echo $row[0]['contact_number']?>" name="contact_number" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Pan Card Number</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Pan Card Number" value="<?php echo $row[0]['pan_no']?>" name="pan_no" >
                                        </div>
                                    </div>


                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputuname"> Address</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Address" value="<?php echo $row[0]['bitcoin_address']?>" name="bitcoin_address" >
                                        </div>
                                    </div>


                                                 <div class="form-group">
                                        <label for="exampleInputuname">Aadhar Card Number</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Aadhar Card Number" value="<?php echo $row[0]['adhar_number']?>" name="adhar_number" >
                                        </div>
                                    </div>

                                    </div>


                                  </div>
                                  
                                  
                                  <hr>
                                  <h4>Wallet Details</h4>
                                   <div class="row">
                                    <div class="col-md-6">
                                        
                                        
                                      <div class="form-group">
                                          <div>
                                              <?php  echo $this->session->userdata('update_member_massage'); ?>
                                              </div>
                                          <label for="exampleInputuname">Bitcoin Wallet</label>
                                          <div class="input-group">

                                              <input type="text" class="form-control" id="bitcoin_address" placeholder="Bitcoin Wallet" value="<?php echo $row[0]['bitcoin_address']?>" name="bitcoin_address" >
                                          </div>
                                      </div></div>
                                       <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputuname">FRX Wallet</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="wec_wallet" placeholder="FRX Wallet" value="<?php echo $row[0]['wec_wallet']?>" name="wec_wallet" >
                                        </div>
                                    </div>

                                   


                                    </div>
                                    


                                  </div>
                                  
                                    <hr>
                                   <h4>Bank Details</h4>

                                 <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Bank Name</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Bank Name" value="<?php echo $row[0]['bank_name']?>" name="bank_name" >
                                        </div>
                                    </div>


                                      <div class="form-group">
                                        <label for="exampleInputuname">Branch</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Branch" value="<?php echo $row[0]['branch']?>" name="branch" >
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="exampleInputuname">IFSC Code</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="IFSC Code" value="<?php echo $row[0]['ifsc_code']?>" name="ifsc_code" >
                                        </div>
                                    </div>
</div>

<div class="col-md-6">
                                     <div class="form-group">
                                        <label for="exampleInputuname">Account Number</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="account_number" placeholder="Account Number" value="<?php echo $row[0]['account_number']?>" name="account_number" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Account Holder Name</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="txtreferallink" placeholder="Account Holder Name" value="<?php echo $row[0]['account_holder_name']?>" name="account_holder_name" >
                                        </div>
                                    </div>

                                </div></div>
                                
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        
                                        <div class="form-group">
                                        <label for="exampleInputuname">OTP</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="otp" placeholder="Enter OTP" required value="" name="otp" >
                                        </div>
                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                         <div class="form-group">
                                        <label for="exampleInputuname">&nbsp;</label>
                                       
                                           <button id="btnotp" type="button" class="btn btn-warning waves-effect waves-light m-r-10" style="padding: 7px;
    margin-top: 32px;">Send OTP</button> <p style="color:green;display:none;" id="potp" >Please Check Your Mail.!</p>
                                        
                                    </div>
                                         
                                     </div>
                                </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-r-10" >Cancel</button>

                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" method="post" action="<?php echo base_url()?>index.php/profile/update_member_password">

                                    <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="exampleInputuname">Current Password</label>
                                            <div class="input-group">

                                                <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Enter User Password" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputuname">Password</label>
                                            <div class="input-group">

                                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter Password" >
                                            </div>
                                        </div>
                                      <div class="form-group">
                                          <label for="exampleInputuname">Confirm Password</label>
                                          <div class="input-group">

                                              <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" >
                                          </div>
                                      </div>

                                      </div>

                                    </div>


<hr>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                          <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                                          <button type="reset" class="btn btn-default waves-effect waves-light m-r-10" >Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="upload" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" accept-charset="utf-8" enctype="multipart/form-data" method="post" action="<?php echo base_url()?>index.php/profile/member_image_upload">

                                  <h2 class="profile-name">

                                  </h2>


                                      <div class="form-group">
                                        <label class="col-sm-12 control-label">
                                          Select Your Image <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-sm-12">
                                          <input
                                            type="file"
                                            name="profile_pic"
                                            class="form-control form-control-line"
                                          required
                                            id="profile_pic"
                                          >

                                          <label id="profile_pic_error" class="error" for="profile_pic">
                                            <?php if (!empty($member_img_error)): ?>
                                              <?php echo $member_img_error; ?>
                                            <?php endif; ?>
                                          </label>
                                        </div>
                                      </div>


                                    <div class="col-sm-9 col-sm-offset-3" >
                                      <button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>
                                      <br>  <br>
                                    </div>

                                  </form>

                            </div>
                        </div>
                        <div class="tab-pane" id="transactional" role="tabpanel">
                          <div class="card-body">
                              <form class="form-horizontal form-material" method="post" action="<?php echo base_url()?>index.php/profile/transactional_password">

                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="exampleInputuname">Password</label>
                                          <div class="input-group">

                                              <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter Password" >
                                          </div>
                                      </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Confirm Password</label>
                                        <div class="input-group">

                                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" >
                                        </div>
                                    </div>

                                    </div>

                                  </div>


<hr>
                                  <div class="form-group">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-r-10" >Cancel</button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->

        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

 <script type="text/javascript">
      $( "#btnotp" ).click(function() {
         
            
             
              
              var url = '<?php echo base_url(); ?>index.php/profile/profile_otp';
          var data = {
            sponser_id : '',
          }
          $.post(url, data).done(function(msg){
               $( "#potp" ).show();
          });
          
          
        
      });

    
    </script>
