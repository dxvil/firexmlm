<style>
.chosen-container-single .chosen-single{

height: 41px!important;
}

.chosen-container-single .chosen-single span{
        margin-top: 8px;
}

</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">Add/Deduct Fund</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Add/Deduct Fund</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Buy Gift Card</h4> -->
                                <!-- <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/fund/action" method="post" id="formwallet">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

    <div class="row">

      <div class="col-md-4">
      <div class="form-group">
      <label for="exampleInputuname">Select Member:</label>

      <select name="member_id" id="member_id" class="select2 form-control custom-select livesearch " style="width: 100%; height:46px;" onchange="getmembername()" required>
        <option selected="selected" value="">Select Member</option>
        <?php foreach ($members as $key => $value) {
        ?>
        <option value="<?php echo $value['member_id']?>"><?php echo $value['username']?></option>
      <?php  }?>
      </select>

      </div>
      </div>
      <div class="col-md-4">
<div class="form-group">
<label for="exampleInputuname">Member Name</label>

    <input name="member_name" id="member_name" class="form-control" value=""  placeholder="Member Name"  readonly required>

</div>
</div>
 <div class="col-md-4">
<div class="form-group">
<label for="exampleInputuname">Wallet Balance</label>

    <input name="balance" id="balance" class="form-control" value=""  placeholder="Wallet Balance"  readonly required>

</div>
</div>
                                   
                          </div>

                                  <div class="row">
                                                                  <div class="col-md-4">
                                                                      
                                                                      <div class="form-group">
                                                        <div class="switch switch-primary d-inline m-r-10">
                                                        <label> Deduct </label> &nbsp; <input type="radio" checked="checked" name="mode" value="0"/>
                                                        </div>
                                                        <label>Add</label> <input type="radio" name="mode" value="1"/>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="switch switch-warning d-inline m-r-10">
                                                        <label>  E-Wallet </label> &nbsp; <input type="radio" name="wallet_type" checked="checked" value="0"/> 
                                                        </div>
                                                        <label>Fund Wallet</label> <input type="radio" name="wallet_type" value="1"/> 
                                                    </div>
                                                                      
                                                                      

                                          
                                      </div>



                                              <div class="col-md-4">
                                        <div class="form-group">
<label for="exampleInputuname">Amount</label>

                                            <input  type="number" name="amount" id="amount" class="form-control" value=""  placeholder="Amount To be Added / Deducted"   required>

                                    </div>
                                    </div>

                                    <div class="col-md-4">
                              <div class="form-group">
<label for="exampleInputuname">Remarks</label>

                                  <textarea   type="text" name="remarks" id="remarks" class="form-control" value=""  placeholder="Enter Remarks" required ></textarea>

                          </div>
                          </div>
                                                                </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<script>



        function getmembername()
        {
          var member_id=  $('#member_id').val();
          var wallet_type=  $('input[name=wallet_type]:checked').val();
          var isChecked = $('input[name=mode]:checked').val();
          
          var url = '<?php echo base_url(); ?>index.php/fund/getmembername';
          var data = {
          member_id : member_id,
          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });

        var url = '<?php echo base_url(); ?>index.php/fund/iwallet_balance';

         
        var data = {
        member_id : member_id,
       wallet_type :wallet_type,
        }
        $.post(url, data).done(function(msg){
            
           
        $('#balance').val(msg);
      });

        }

// function calculate(){
//   if($('#balance').val(msg);)
// }

        </script>

        <script>
  

        $('input[name=wallet_type]').change(function() {

          getmembername(); 

        });




     $('#formwallet').on('submit', function() {
        var isChecked = $('input[name=mode]:checked').val();

        if(isChecked == 0){

        if(parseFloat($('#balance').val())<parseFloat($('#amount').val())) {
          alert('Deducted Amount Cant be more than balance');
          return false;
        }
        }
      });



       // For select 2
       $(".select2").select2();
     
     </script>
     
      

