

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">List All Transaction Ledger</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">List All Transaction Ledger</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">
                                        <div class="card-actions">
                                          <?php $total=0; foreach ($list as $value) {
                                            $total+=$value['debited'];
                                          }
                                            ?>
                                            <div class="row">
                                              <div class="col-md-12">
                                               
                                              </div>
                                              </div>
                      </div>
                                          <h4 class="card-title">List All Transaction Ledger</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                      <tr>
                                                          <th>#</th>
<th>Date</th>
<th>Particular</th><th>Credited</th>
                                                        <th>Debited</th>
                                                          <th>Member ID</th>
                                                          <th>Name</th>
                                                        
                                                          
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>
<th>Date</th>
<th>Particular</th>   <th>Credited</th>
                                                        <th>Debited</th>
                                                        <th>Member ID</th>
                                                        <th>Name</th>
                                                     
                                                        
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        <?php $i=1; foreach ($list as $value) {?>
                                                      <tr>
                                                         <td><?php echo  $i; ?></td>
<td><?php echo  date('Y-m-d',strtotime($value['bonusdate']))?></td>
<td><?php echo  $value['desc']?><?php echo  $value['username']?></td>
 <td><?php echo  $value['credited']?></td>
                                                          <td><?php echo  $value['debited']?></td>
                                                          <td><?php echo  $value['username']?></td>
                                                          <td><?php echo  $value['name']?></td>
                                                          
                                                          

                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
