
<!DOCTYPE html>
<html lang="en">

<head>
    <title>WAVE EDU COIN - Signin</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="CodedThemes">

    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/animation/css/animate.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/layouts/dark.css">

</head>

<body>
    <div class="auth-wrapper aut-bg-img" style="background-image: url('<?php echo base_url() ?>assets/images/bg-images/bg3.jpeg');">
        <div class="auth-content">
            <div class="text-white">
              <form class="" method="post" action="<?php echo base_url()?>index.php/login/forgot_password">
                <?php
                  $error=$this->session->flashdata('error_login');
                  echo (!empty($error))?
                  "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>

                <div class="card-body text-center">
                    <div class="mb-4">
                        <i class="feather icon-unlock auth-icon"></i>
                    </div>
                    <h3 class="mb-4 text-white">Forgot Password</h3>
                    <div class="input-group mb-3">
                         <input type="email" name="email" class="form-control forgot-email" placeholder="Enter your email">
                    </div>
                   
                   
                    <button class="btn btn-primary shadow-2 mb-4" type="submit">Submit</button>
                   
                    <p class="mb-0 text-muted"> <a class="text-white" href="<?php echo base_url()?>">Back to Login</a></p>
                </div>
              </form>
            </div>
        </div>
    </div>

    <!-- Required Js -->
    <script src="../assets/js/vendor-all.min.js"></script><script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/pcoded.min.js"></script>

</body>
<!-- Copied from http://html.codedthemes.com/datta-able/bootstrap/default/auth-signin-v3.html by Cyotek WebCopy 1.5.0.516, Friday, February 22, 2019, 3:40:33 PM -->
</html>

