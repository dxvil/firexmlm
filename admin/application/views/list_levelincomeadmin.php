<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Level Income</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Level Income</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-actions">
                  <?php $total=0; foreach ($spillincome as $value) {
                  $total+=$value['amount']; } ?>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="text-danger"  style="font-weight:600;" >  Total Level Income  :
                        <span style="font-size:20px;"> &#8377;<?php echo $total; ?></span>
                      </span>
                    </div>
                  </div>
                </div>
                <h4 class="card-title">List Level Income</h4>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Bonus</th>
                        <th>Upgrade Fee</th>
                        <th>Net Income</th>
                        <th>Level</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Bonus</th>
                        <th>Upgrade Fee</th>
                        <th>Net Income</th>
                        <th>Level</th>
                        <th>Date</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i=1; foreach($spillincome as $value) {?>
                      <tr>
                        <td><?php echo  $i; ?></td>
                        <td><?php echo  $value['username']?></td>
                        <td><?php echo  $value['amount']?></td>
                        <td>
                          <?php  if($value['amount']==600){echo 300;} if($value['amount']==1600){echo 1000;} if($value['amount']==5600){echo 2000;} if($value['amount']==35200){echo 10000;} if($value['amount']==416200){echo 160000;}  if($value['amount']==1152000){echo 6000000;}?>
                        </td>
                        <td>
                          <?php if($value['amount']==600){echo 300;} if($value['amount']==1600){echo 600;} if($value['amount']==5600){echo 3600;} if($value['amount']==35200){echo 25200;}  if($value['amount']==416200){echo 256000;} if($value['amount']==1152000){echo 5520000;}?>
                        </td>
                        <td><?php echo  $value['level']?></td>
                        <td><?php echo date('Y-m-d',strtotime($value['bonusdate']))?></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- ============================================================== -->
          <!-- End PAge Content -->
          <!-- ============================================================== -->
