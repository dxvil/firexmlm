<style media="screen">
  div.dataTables_wrapper div.dataTables_filter input{
        padding: 3px 10px;
  }

  div.dataTables_wrapper div.dataTables_filter label{
    margin-top: -36px;
  }

  .btn {
    padding: 9px 20px;
  font-size: 11px;}
</style>
<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
              <h3 class="text-themecolor">My Direct</h3>
            </div>
            <div class="col-md-7 align-self-center">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">My Direct</li>
              </ol>
            </div>
          </div>
          <!-- ============================================================== -->
          <!-- End Bread crumb and right sidebar toggle -->
          <!-- ============================================================== -->
          <!-- ============================================================== -->
          <!-- Container fluid  -->
          <!-- ============================================================== -->
          <div class="container-fluid">
             <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>Left Member</h5>
                  </div>
                  <div class="card-block">
                    <h6 class="card-title">Left Member <span style="color:red">Total Red : <?php echo $leftred?></span> <span style="color:green">Total Green : <?php echo $rightgreen?></span> </h6>
                    <div class="table-responsive">
                      <table id="key-act-button-left" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Member ID</th>
                            <th>Registration Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; foreach ($directmembers as $value) {?>
                          <tr>
                            <td><?php echo  $i;?></td>
                            <td><?php echo  $value['name']?><br>
                            <?php echo  $value['contact_number']?></td>
                            <td><?php  if($value['member_status']==0){?>
                              <img src="<?php echo base_url()?>images/red.png">
                            <?php   }
                              if($value['member_status']==1){?>
                                <img src="<?php echo base_url()?>images/green.png">
                            <?php   }
                            echo  $value['username'];?></td>
                            <td><?php echo  date('d-m-Y',strtotime($value['created_on']))?></td>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Member ID</th>
                            <th>Registration Date</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>Right Member</h5>
                  </div>
                  <div class="card-block">
                    <h6 class="card-title">Right Member  <span style="color:red">Total Red : <?php echo $rightred?></span> <span style="color:green">Total Green : <?php echo $rightgreen?></span></h6>
                    <div class="table-responsive">
                      <table id="key-act-button-right" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Member ID</th>
                            <th>Registration Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; foreach ($directmembersright as $value) {?>
                          <tr>
                            <td><?php echo  $i;?></td>
                            <td><?php echo  $value['name']?><br>
                            <?php echo  $value['contact_number']?></td>
                            <td><?php  if($value['member_status']==0){?>
                              <img src="<?php echo base_url()?>images/red.png">
                              <?php   }
                              if($value['member_status']==1){?>
                              <img src="<?php echo base_url()?>images/green.png">
                              <?php   } echo  $value['username']?>
                            </td>
                            <td><?php echo  date('d-m-Y',strtotime($value['created_on']))?></td>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Member ID</th>
                            <th>Registration Date</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
             </div>
