<style>

.tooltip-inner2{font-size: 12px;padding: 21px;}
/*Now the CSS*/
* {margin: 0; padding: 0;}
.tree td{
padding:2px;
border-right:1px dotted #ccc;
border-left:1px dotted #ccc;
}
.tree ul {
	padding-top: 20px; position: relative;

	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li {
	float: left; text-align: center;
	list-style-type: none;
	position: relative;
	padding: 20px 20px 0 20px;

	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
	content: '';
	position: absolute; top: 0; right: 50%;
	border-top: 1px solid #ccc;
	width: 50%; height: 20px;
}
.tree li::after{
	right: auto; left: 50%;
	border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
	display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px solid #ccc;
	width: 0; height: 20px;
}

.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;

	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;

	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after,
.tree li a:hover+ul li::before,
.tree li a:hover+ul::before,
.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
}

.btn{
	padding: 6px 16px;
}
/*Thats all. I hope you enjoyed it.
Thanks :)*/

</style>
<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
              	<h3 class="text-themecolor">My Network Tree</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                	<li class="breadcrumb-item active">My Network Tree</li>
                </ol>
              </div>
            </div>
            <!-- ============================================================== -->
        		<!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
          	<!-- ============================================================== -->
          	<!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
          	<!-- Start Page Content -->
            <!-- ============================================================== -->
    					<div class="card" style="width:1024px;">
                <div class="card-body">
                  <div class="col-md-12 col-xs-12">
										<h5 class="card-title pull-right" style="float: right;">Member Right :
											<?php echo $totalrightleg;?> ($<?php echo $totalrightpv?>)
										</h5>
									</div>
									<h5 class="card-title">Member Left :
										<?php echo $totalleftleg;?> ($<?php echo $totalleftpv?>)
									</h5>
									<?php $left_url=base_url().'index.php/team/left';
									$right_url=base_url().'index.php/team/right';
									$level_up=base_url().'index.php/team/level_up';
									$backtotop=base_url().'index.php/team/treeview';?>
									<div style="margin-left:297px;margin-top: -35px;" class="button-group"><button type="button" class="btn waves-effect waves-light btn-danger" onclick="action_without_msg('<?php echo $left_url?>',<?php echo $member[0]['id'] ?>)">Left</button>
										<button type="button" class="btn waves-effect waves-light btn-danger" onclick="action_without_msg('<?php echo $right_url?>',<?php echo $member[0]['id'];?>)">Right</button>
										<button type="button" class="btn waves-effect waves-light btn-danger" onclick="action_without_msg('<?php echo $level_up?>',<?php echo $member[0]['id'];?>)">1 Level Up</button><button type="button" class="btn waves-effect waves-light btn-danger" onclick="action_without_msg('<?php echo $backtotop?>')">Back to Top</button>
									</div>
									<br>
                	<div class="tree" >
                    <ul>
                			<li style="padding-left: 0px;">
												<a href="#" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-content=""  data-original-title="Rank:<?php echo $rank;?><br><?php echo $member[0]['username'];?><br> <?php echo $member[0]['email'];?><br><br>
													<?php echo '	<table >
													<tr><td></td><td>Left </td><td> Right</td><td> Total</td><tr>
													<tr><td>Member:</td><td>'.$totalleftleg.'</td><td>'.$totalrightleg.'</td><td>'.($totalrightleg+$totalleftleg).'</td><tr>
													<tr><td>BV:</td><td>'.($totalleftleg_bv*2000).'</td><td>'.($totalrightleg_bv*2000).'</td><td>'.(($totalleftleg_bv*2000)+($totalrightleg_bv*2000)).'</td><tr></table>';?>" class="mytooltip" style="text-transform:capitalize;font-size:12px; font-weight:600; "><?php echo	'<img src="'.base_url().'assets/images/user/avatar-1.jpeg" style="width:40px;">';?>
												<?php echo $member[0]['username'];?>
																								</a>
                      	<?php echo $str; ?>
                      </li>
                    </ul>
                  </div>
          			</div>
            	</div>
            </div>
          </div>
				</div>
  		</div>



			<script>
				$(".hover").hover(function(){
  				$('#RadToolTipWrapper_ctl00_ContentPlaceHolder1_RadToolTip1').show();
					$('.signpanel').show();
				},function(){
					$('.signpanel').hide();
      		$('#RadToolTipWrapper_ctl00_ContentPlaceHolder1_RadToolTip1').hide();
				});

				function action_without_msg(url,id){
    			var form = document.createElement("form");
    			element1 = document.createElement("input");
    			form.action = url;
    			form.method = "post";
    			element1.name = "id";
    			element1.value = id;
    			form.appendChild(element1);
    			document.body.appendChild(form);
    			form.submit();
  			}

  			function action_without_msg_member(url,id,direction){
    			var form = document.createElement("form");
    			element1 = document.createElement("input");
    			element2 = document.createElement("input");
    			form.action = url;
    			form.method = "post";
    			element1.name = "id";
    			element1.value = id;
    			form.appendChild(element1);
    			element2.name = "direction";
    			element2.value = direction;
    			form.appendChild(element2);
    			document.body.appendChild(form);
    			form.submit();
  			}
			</script>
