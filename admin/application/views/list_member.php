<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">List Member</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">List Member</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->

                                <?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">

                                          <h4 class="card-title">List Member</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                  <thead>
                                                      <tr>
                                                          <th>#</th>
                                                          <th>Member Id</th>
                                                          <th>Member Name</th>
                                                          <th>Email</th>
                                                          <th>Date</th>
                                                          <th>Action</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>

                                                        <th>Member ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        <?php
                                                        $activate=base_url().'index.php/member/activate';
                                                        $deactivate=base_url().'index.php/member/deactivate';
                                                        $edit_url=base_url().'index.php/member/edit_member';
                                                        $view_url=base_url().'index.php/member/view_member';
                                                        $password_url=base_url().'index.php/member/change_password';
                                                         $i=1; foreach ($listmember as $value) {?>
                                                      <tr>
                                                          <td><?php echo  $i; ?></td>
                                                          <td><?php echo  $value['username']?></td>
                                                          <td><?php echo  $value['name']?></td>
                                                          <td><?php echo  $value['email']?></td>
                                                          <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                                                          <td>

<?php if($value['status']==1){?>

                                                            <button type="button"
                                                         onclick="confirmaction('<?php echo $deactivate;?>',<?php echo $value['id']; ?>,'Are you sure you want to Deactivate this Member?')" class="btn btn-danger btn-circle"  title="Deactivate Member"><i class="fa fa-times"></i> </button>

<?php } else if($value['status']==0){
?>
                                                         <button title="Activate Member" type="button"
                                                        onclick="action('<?php echo $activate;?>',<?php echo $value['id']; ?>)" class="btn btn-success btn-circle"><i class="fa fa-check"></i> </button>
                                                      <?php } ?>
                                                        <button  title="Edit Member Profile"  type="button"
                                                       onclick="action('<?php echo $edit_url;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i> </button>
                                                       <button  title="View Member Profile" type="button"
                                                      onclick="action('<?php echo $view_url;?>',<?php echo $value['id']; ?>)" class="btn btn-warning btn-circle"><i class="fa fa-list"></i> </button>

                                                      <button  title="Change Password" type="button"
                                                     onclick="action('<?php echo $password_url;?>',<?php echo $value['id']; ?>)" class="btn btn-danger btn-circle"><i class="fa fa-briefcase"></i> </button>

                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
                                <script type='text/javascript'>

                                function action(url,id)
                                {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();

                                }

                                function confirmaction(url,id,msg)
                                {
                                 var strconfirm = confirm(msg);
                                 if (strconfirm == true)
                                            {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();
                                 }
                                }
                                </script>
