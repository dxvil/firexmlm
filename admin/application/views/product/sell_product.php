<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Sell Product</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Sell Product</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/product/sold_product" method="post">

                                  <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Member ID</label>
                                        <select name="member_id" id="member_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" onchange="getmembername()" required>
<option selected="selected" value="">Select Member</option>
<?php foreach ($members as $key => $value) {
?>
<option value="<?php echo $value['member_id']?>"><?php echo $value['username']?></option>
<?php  }?>
</select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Product ID</label>
                                        <select name="product_id[]" id="product_id" class="select2  form-control custom-select" multiple="multiple" style="width: 100%; height:36px;" onchange="getpackage()" required>
<option  value="">Select Product</option>
<?php foreach ($products as $key => $value) {
?>
<option value="<?php echo $value['id']?>"><?php echo $value['product_name']?></option>
<?php  }?>
</select>
                                    </div>
                                  </div>

                                  <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Member Name
  </label>
                                            <input name="member_name" id="member_name" class="form-control" value=""  placeholder="Transfer To Member Name"  readonly required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Billing Date
  </label>
                                            <input type="date" name="billing_date" id="billing_date" class="form-control" value=""  required>
                                    </div>


                                  </div>

                                  <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Payment </label>
                  <select name="payment" id="payment" class="select2 form-control custom-select" style="width: 100%; height:36px;" onchange="getpackage()" required>
                  <option selected="selected" value="Cash">Cash</option>
                  <option value="Due">Due</option>
                  </select>
                                    </div>

                                  </div>

                                  </div>

<div class="row" id="ajaxdiv">

</div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Add</button>
  <button type="reset"  class="btn btn-inverse" >Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
        <script>
                function getpackage()
                {
                  var package=  $('#product_id').val();
                  var url = '<?php echo base_url(); ?>index.php/product/getpackage';
                  var data = {
                  package : package,
                  }
                  $.post(url, data).done(function(msg){
                  $('#ajaxdiv').html(msg);

                });
                }

                function getmembername()
               {
                var member_id=  $('#member_id').val();
                var url = '<?php echo base_url(); ?>index.php/product/getmembername';
                var data = {
                member_id : member_id,
                }
                $.post(url, data).done(function(msg){
                $('#member_name').val(msg);
              });

              }
          </script>
          <script>
             jQuery(document).ready(function() {

                 // For select 2
                 $(".select2").select2();
               });
               </script>
