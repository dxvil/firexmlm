<!-- BEGIN HEADER -->
<?php
    /*	Load header	*/
    $this->load->view("common/header");
?>
<!-- END HEADER -->
<!-- BEGIN SIDEBAR -->

        <?php
			/*	Load Sidebar menu for particular User	*/
			$this->load->view("common/menu");
		?>
        <!-- END SIDEBAR -->
		 <!-- BEGIN CONTENT -->  
     
                    <?php
                    /*	Load the main content according to User type	*/
                    $this->load->view($content);
                ?>
         <!-- End CONTENT -->     
		


<!-- BEGIN FOOTER -->
<?php
			/*	Load Footer */
			$this->load->view("common/footer");
		?>
<!-- END FOOTER -->