<!DOCTYPE html>
<html lang="en">


<head>
    <title><?php echo $title; ?></title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="CodedThemes">

    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/animation/css/animate.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/layouts/dark.css">

</head>

<body>
    <div class="auth-wrapper aut-bg-img" style="background-image: url('<?php echo base_url() ?>assets/images/bg-images/bg3.jpeg');">
        <div class="auth-content" style="width: 600px;">
            <div class="text-white">
              <form class="" method="post" action="<?php echo base_url()?>index.php/login/save_member">
                <?php
                $error=$this->session->flashdata('success');
                echo (!empty($error))?
                "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

                <?php
                $error=$this->session->flashdata('error_login');
                echo (!empty($error))?
                "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                <div class="card-body text-center">
                  <div class="mb-4">
                    <i class="feather icon-user-plus auth-icon"></i>
                  </div>
                  <h3 class="mb-4">Sign up</h3>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <input class="form-control" type="text" required="" id="sponser_id" name="sponser_id"  placeholder="Sponser Id" required value=<?php echo $this->input->get('spid');?>>
                      </div>
                      <div class="col-sm-6">
                        <input class="form-control" type="text" required="" placeholder="Sponser Name" name="sponser_name" id="sponser_name" value="<?php echo $spname;?>" readonly>
                      </div>
                    </div>
                  </div>
                  <h3 class="box-title m-b-20">Applicant Information</h3>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <select name="direction" id="direction" class="form-control" required>
                          <option selected="selected" value="" >Select Direction</option>
                          <option value="0">Left</option>
                          <option  value="1">Right</option>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <input class="form-control" type="text" required="" placeholder="Name" name="name" id="name" value="<?php echo set_value('name'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <input class="form-control" type="number" maxlength="10" required="" id="contact_no" name="contact_no" placeholder="Contact No" value="<?php echo set_value('contact_no'); ?>">
                      </div>
                      <div class="col-sm-6">
                        <input class="form-control" type="email"  placeholder="Email" id="email" name="email" value="<?php echo set_value('email'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Your Password" required="" >
                      </div>
                      <div class="col-sm-6">
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required="" >
                      </div>
                    </div>
                  </div>
                  <button class="btn btn-primary shadow-2 mb-4" type="submit">Sign up</button>
                  <p class="mb-0 text-muted">Allready have an account? <a href="<?php echo base_url()?>"  style="color:#fff;"> Log in</a></p>
                </div>
              </form>
            </div>
        </div>
    </div>

    <!-- Required Js -->
    <script src="<?php echo base_url() ?>assets/js/vendor-all.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/pcoded.min.js"></script>

    <script type="text/javascript">
      $( "#sponser_id" ).change(function() {
        var url = '<?php echo base_url(); ?>index.php/login/getsponser_name';
          var data = {
            sponser_id : $( "#sponser_id" ).val(),
          }
          $.post(url, data).done(function(msg){
            $('#sponser_name').val(msg);
          });
      });

      $('#submit').click(function(){
        if ($('#sponser_id').val()=='') {
          $('#sponser_id').focus();
          return false;
        }
        if ($('#direction').val()=='') {
          $('#direction').focus();
          return false;
        }
        if ($('#name').val()=='') {
          $('#name').focus();
          return false;
        }
        if ($('#contact_no').val()=='') {
          $('#contact_no').focus();
          return false;
        }
        if ($('#user_id').val()=='') {
          $('#user_id').focus();
          return false;
        }
        if ($('#password').val()=='') {
          $('#password').focus();
          return false;
        }
        if ($('#confirm_password').val()=='') {
          $('#confirm_password').focus();
          return false;
        }
        if ($('#password').val().length <6) {
          alert('Please Enter a password more than 6 character.');
          return false;
        }
        if ($('#password').val()!== $('#confirm_password').val()) {
          alert('Password Mismatch.');
          return false;
        }
        if(!document.getElementById('checkbox-signup').checked){
          alert('Please Check Terms & Condition.');
          return false;
        }
      });
    </script>

  </body>
</html>
